#ifndef IN_MEMORY_DATABASE_H
#define IN_MEMORY_DATABASE_H

#include "FrameSequence.hpp"
#include "TrainedDatabase.hpp"

#include <vector>
#include <string>

class InMemoryDatabase : public TrainedDatabase
{

public:
    InMemoryDatabase();
    ~InMemoryDatabase();

    void add(FrameSequence &sequence);
    FrameSequence getNextVideo();
    int size();

private:
    std::vector<FrameSequence> classVideo;

    int index;
};

#endif
