#ifndef FLOW_VECTOR_CLASSIFIER_GPU_H
#define FLOW_VECTOR_CLASSIFIER_GPU_H

#include "FrameSequence.hpp"
#include "TrainedDatabase.hpp"
#include "ResultClass.hpp"
#include "cuda_definitions.cuh"

#include <vector>
#include <array>
#include <opencv2/opencv.hpp>
#include <cuda_runtime.h>

#ifndef FLOW_CHANNELS
#define FLOW_CHANNELS 4
#endif

class FlowVectorClassifier_GPU
{

public:
    FlowVectorClassifier_GPU();
    ~FlowVectorClassifier_GPU();

    __host__
    ResultClass classify(TrainedDatabase &trainedDatabase, FrameSequence &sequence);

private:
    typedef std::array<cv::Mat, FLOW_CHANNELS> FlowChannels;
    typedef std::vector<FlowChannels> FlowSequence;

#   define messageProgress(message) { fprintf(stderr, message); }
#   define monitorProgress(message, argv...) { fprintf(stderr, message, argv); }
#   define completeProgress(message) { fprintf(stderr, message); }

    __host__
    void createCUDAGaussianFilter();

    const int ROW_SCORES = 15;

    float *d_motionSimilarity;
    float *d_sequence;
    float *d_trained;
    float *d_gaussKernel;

    __host__
    void computeAndFindClass(FrameSequence &trained, FrameSequence &sequence,
                             FrameSequence &bestClass, float &bestScore, cv::Mat &similarity);
    __host__
    void preprocessSequence_GPU(FrameSequence &h_sequence, float **d_sequence);
    __host__
    void uploadSequence(FrameSequence &sequence, float **device);
    __host__
    void splitFlowToChannels_GPU(int width, int height, int length, float **channelMap, float *flowField);
    __host__
    void blurChannels_GPU(float **device, int height, int width, int length);
    __host__
    void downloadSequence(float *device, int rows, int cols, int length, FlowSequence &novel);
    __host__
    void normalizeFlowChannels_GPU(float *device, int height, int width, int length);
    __host__
    void computeFrameSimilarity_GPU(float *d_trained, float *d_sequence, int trainedLength, int sequenceLength, int width, int height);
    __host__
    void applyWeightKernel_GPU(int height, int width);
    __host__
    void scoreMotionSimilarity_GPU(int height, int width, float *h_rowScore);
    __host__
    void downloadMotionSimilarity(cv::Mat &mat, int height, int width);
    __host__
    void debugMotionSimilarity(cv::Mat motionSimilarity);
    __host__
    float maximumSimilarity(float *rowScore, int size);

    const cv::Size GAUSS_SIZE = Size(5, 5);
};

#endif
