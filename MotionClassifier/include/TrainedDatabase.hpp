#ifndef TRAINED_DATABASE_H
#define TRAINED_DATABASE_H

#include "FrameSequence.hpp"

#include <opencv2/opencv.hpp>


class TrainedDatabase
{
public:
    virtual FrameSequence getNextVideo() = 0;
};

#endif
