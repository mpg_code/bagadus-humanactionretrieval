#ifndef VIDEO_SEQUENCE_ANALYZER
#define VIDEO_SEQUENCE_ANALYZER

#include "StreamSynchronizer.hpp"
#include "FlowVectorClassifier.hpp"
#include "FlowVectorClassifier_GPU.cuh"
#include "FrameSequence.hpp"
#include "FrameSequenceElement.hpp"
#include "CameraFieldMapper.hpp"
#include "InMemoryDatabase.hpp"
#include "LinearDatabase.hpp"

#include <opencv2/opencv.hpp>
#include <string>

class VideoSequenceAnalyzer
{

public:
    VideoSequenceAnalyzer(CameraFieldMapper &mapper, int boundingSize, bool render, cv::Mat background);
    ~VideoSequenceAnalyzer();

    int extractToClassifier(StreamSynchronizer &stream, std::string classID, int trackID);
    std::string classifySequence(StreamSynchronizer &stream, int trackID, int &processedFrames);
    std::string classifySequence(LinearDatabase &linearDatabase, StreamSynchronizer &stream, int trackID, int &processedFrames);

    FrameSequence readVideo(StreamSynchronizer &stream, int trackID);

private:
    const int MAX_PLAYER_ID = 15;

#   ifdef USE_CUDA
    FlowVectorClassifier_GPU classifier;
#   else
    FlowVectorClassifier classifier;
#   endif

    CameraFieldMapper &mapper;
    int boundingSize;
    InMemoryDatabase memoryDatabase;
    bool render;
    cv::Mat background;

    struct PlayerPosition getNextPosition(StreamSynchronizer &stream, int trackID);
    bool insideFrame(struct PlayerPosition &player, int frameWidth, int frameHeight);
    void computeOpticalFlow(FrameSequence &sequence);
};

#endif
