#ifndef CUDA_DEFINITIONS_H
#define CUDA_DEFINITIONS_H

#ifdef __CUDACC__
#   define CUDA_CHECK(_cuda)                                            \
    {                                                                   \
     cudaError_t _cuda_err = _cuda;                                     \
     if (_cuda_err != cudaSuccess)                                      \
     {                                                                  \
      fprintf(stderr, "CUDA error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
      exit(EXIT_FAILURE);                                               \
      }                                                                 \
     }

#   define CUDA_CHECK_KERNEL()                                          \
    {                                                                   \
     CUDA_CHECK(cudaDeviceSynchronize());                               \
     cudaError_t _cuda_err = cudaGetLastError();                        \
     if (_cuda_err != cudaSuccess)                                      \
     {                                                                  \
      fprintf(stderr, "CUDA KERNEL error: %s @ %s:%d \n", cudaGetErrorString(_cuda_err), __FILE__, __LINE__); \
      exit(EXIT_FAILURE);                                               \
      }                                                                 \
     }

#endif

#endif
