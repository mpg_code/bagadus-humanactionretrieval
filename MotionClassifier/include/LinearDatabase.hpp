#ifndef LINEAR_DATABASE_H
#define LINEAR_DATABASE_H

#include "FrameSequence.hpp"
#include "TrainedSetSynchronizer.hpp"
#include "SQLEntity.hpp"
#include "TrainedDatabase.hpp"

#include <vector>
#include <opencv2/opencv.hpp>
#include <string>

class LinearDatabase : public TrainedDatabase
{
public:
    LinearDatabase(TrainedSetSynchronizer &stream, std::vector<int> skipSequences);
    ~LinearDatabase();

    FrameSequence getNextVideo();

private:
    TrainedSetSynchronizer &stream;
    std::vector<int> skipList;

    bool isClipBreak(SQLEntity* cur, SQLEntity *next);
    bool skipSequence(FrameSequence &sequence);
};

#endif
