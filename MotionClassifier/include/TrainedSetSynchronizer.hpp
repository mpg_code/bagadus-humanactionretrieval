#ifndef TRAINED_SET_SYNCHRONIZER_H
#define TRAINED_SET_SYNCHRONIZER_H

#include "MatSerializer.hpp"
#include "SQLDataReader.hpp"
#include "SQLEntity.hpp"

#include <opencv2/opencv.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/stream.hpp>

/**
 * The 'video' in this class is actually a series of cv::Mat,
 * put sequentially after one another to form a 'video'
 * consisting of optical flow vectors.
 */
class TrainedSetSynchronizer
{
public:
    TrainedSetSynchronizer(char *csvFilename, char *serFilename);
    ~TrainedSetSynchronizer();

    cv::Mat getVideo();
    struct PlayerPosition getPosition();
    SQLEntity* getPlayer();
    SQLEntity* peakNextPlayer();

    bool syncVideoToCsv(bool showProgress = false);
    void nextFrame();

    int getWidth();
    int getHeight();

private:
    SQLDataReader *sql;
    boost::archive::binary_iarchive *video;
    std::ifstream *ifs;
    boost::iostreams::filtering_streambuf<boost::iostreams::input> *dataFile;
    FrameMat frame;

    SQLEntity *currentPosition;
    SQLEntity *nextPosition;

    bool readVideoFrame();
    size_t findStartTime(char *serFilename);
    size_t getCsvFrame();
};

#endif
