#ifndef SKELETON_RENDERER_H
#define SKELETON_RENDERER_H

#include "FrameSequence.hpp"
#include "SQLEntity.hpp"

#include <vector>
#include <opencv2/opencv.hpp>
#include <iostream>

class SkeletonRender
{
public:
    SkeletonRender(FrameSequence &trained, FrameSequence &sequence, cv::Mat motionSimilarity);
    ~SkeletonRender();

    void exportToPng(std::string directory);
    void exportToCsv(std::string filename);

private:
    FrameSequence &sequence;
    std::vector<SQLEntity> timeline;
    std::vector<int> compression_params;

    void generateTimeline(FrameSequence &trained, cv::Mat &motionSimilarity);
    void drawSkeleton(SQLEntity &skeleton, cv::Mat &frame);
    void adjustSkeleton();

    void exportCsv(std::ofstream &csv, int frame);

    void exportPng(std::string prefix, cv::Mat frame);
    void drawPoint(cv::Point2f position, cv::Mat &frame);
    void drawLine(cv::Point2f a, cv::Point2f b, cv::Mat &frame);
};

#endif
