#ifndef FRAME_SEQUENCE_H
#define FRAME_SEQUENCE_H

#include "SimpleCSVReader.hpp"
#include "SQLEntity.hpp"
#include "FrameSequenceElement.hpp"

#include <vector>
#include <opencv2/opencv.hpp>
#include <string>

class FrameSequence
{
public:
    FrameSequence();
    ~FrameSequence();

    void add(cv::Mat frame, cv::Mat mask, cv::Rect roi, struct PlayerPosition position);
    void add(SQLEntity *entity, struct PlayerPosition position, cv::Mat flow);
    void add(FrameSequenceElement &elm);
    void setFlowField(cv::Mat &flow, int index);
    void setClass(std::string classID);
    void normalize();
    struct FrameSequenceElement get(int index);
    std::string getClass();
    int length();
    void removeFirstFrame();
    void removeLastFrame();
    void setMaxRoi(int maxWidth, int maxHeight);
    void shrinkRoi(int minWidth, int minHeight);
    std::vector<struct FrameSequenceElement>& getAll();

private:
    std::vector<struct FrameSequenceElement> element;
    std::string classID;

    int maxWidth;
    int maxHeight;
};

#endif
