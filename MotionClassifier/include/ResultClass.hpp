#ifndef RESULT_CLASS_H
#define RESULT_CLASS_H

#include <opencv2/opencv.hpp>

struct ResultClass
{
    FrameSequence sequence;
    cv::Mat motionSimilarity;
};

#endif
