#include "FrameSequence.hpp"

#include <algorithm>

using namespace cv;
using namespace std;

FrameSequence::FrameSequence() :
    maxWidth(0),
    maxHeight(0)
{

}

FrameSequence::~FrameSequence()
{

}

void FrameSequence::add(Mat frame, Mat mask, Rect roi, struct PlayerPosition position)
{
    struct FrameSequenceElement elm;
    elm.frame = frame;
    elm.mask = mask;
    elm.roi = roi;
    elm.position = position;
    elm.entity = NULL;
    element.push_back(elm);

    if (roi.width > maxWidth)
        maxWidth = roi.width;

    if (roi.height > maxHeight)
        maxHeight = roi.height;
}

void FrameSequence::add(SQLEntity *entity, struct PlayerPosition position, cv::Mat flow)
{
    struct FrameSequenceElement elm;
    // Normal assignment does not work, requires explicit deep copy here
    flow.copyTo(elm.flowField);
    elm.entity = entity;
    elm.position.heading = position.heading;
    elm.position.direction = position.direction;
    elm.position.speed = position.speed;
    element.push_back(elm);
}

void FrameSequence::add(FrameSequenceElement &elm)
{
    if (elm.roi.width > maxWidth)
        maxWidth = elm.roi.width;

    if (elm.roi.height > maxHeight)
        maxHeight = elm.roi.height;

    element.push_back(elm);
}

void FrameSequence::normalize()
{
    for (struct FrameSequenceElement &elm : element) {
        Rect &roi = elm.roi;

        while (roi.width + 1 < maxWidth) {
            roi.x -= 1;
            roi.width += 2;
        }

        while (roi.height + 1 < maxHeight) {
            roi.y -= 1;
            roi.height += 2;
        }

        if (roi.width < maxWidth)
            roi.width += 1;

        if (roi.height < maxHeight)
            roi.height += 1;
    }
}

struct FrameSequenceElement FrameSequence::get(int index)
{
    return element[index];
}

vector<struct FrameSequenceElement>& FrameSequence::getAll()
{
    return element;
}

string FrameSequence::getClass()
{
    return classID;
}

int FrameSequence::length()
{
    return element.size();
}

void FrameSequence::setFlowField(Mat &flow, int index)
{
    element[index].flowField = flow;
}

void FrameSequence::setClass(string classID)
{
    this->classID = classID;
}

void FrameSequence::removeFirstFrame()
{
    element.erase(element.begin());
}

void FrameSequence::removeLastFrame()
{
    element.erase(element.end());
}

void FrameSequence::setMaxRoi(int maxWidth, int maxHeight)
{
    this->maxWidth = maxWidth;
    this->maxHeight = maxHeight;
}

void FrameSequence::shrinkRoi(int minWidth, int minHeight)
{
    for (struct FrameSequenceElement &elm : element) {
        Rect &roi = elm.roi;

        while (roi.width - 1 > minWidth) {
            roi.x += 1;
            roi.width -= 2;
        }

        while (roi.height - 1 > minHeight) {
            roi.y += 1;
            roi.height -= 2;
        }

        if (roi.width > minWidth)
            roi.width -= 1;

        if (roi.height > minHeight)
            roi.height -= 1;
    }
}
