#include "FlowVectorClassifier_GPU.cuh"
#include "OpticalFlow.hpp"
#include "timetaker.h"

#include <cfloat>
#include <queue>
#include <stdio.h>
#include <math.h>
#include <omp.h>

#define DEBUG false

using namespace std;
using namespace cv;

extern __shared__ float resultSet[];

#define CUDA_KERNEL_SIZE 21

__device__ __constant__
float CUDA_weightKernel[441]=
{
    0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,   0.00000000,
    0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.00000000,   0.04761905,
};

FlowVectorClassifier_GPU::FlowVectorClassifier_GPU() :
    d_motionSimilarity(NULL),
    d_sequence(NULL),
    d_trained(NULL)
{
    assert(GAUSS_SIZE.width == GAUSS_SIZE.height);
    createCUDAGaussianFilter();
}

FlowVectorClassifier_GPU::~FlowVectorClassifier_GPU()
{
    CUDA_CHECK(cudaFree(d_gaussKernel));
}

void FlowVectorClassifier_GPU::createCUDAGaussianFilter()
{
    Mat kernelX = getGaussianKernel(GAUSS_SIZE.width, 0, CV_32F);
    Mat kernelY = getGaussianKernel(GAUSS_SIZE.height, 0, CV_32F);
    Mat kernelXY = kernelX * kernelY.t();

    assert(kernelXY.isContinuous());
    CUDA_CHECK(cudaMalloc((void**)&d_gaussKernel, kernelXY.rows * kernelXY.cols * sizeof(float)));
    CUDA_CHECK(cudaMemcpy(d_gaussKernel, kernelXY.ptr(), kernelXY.rows * kernelXY.cols * sizeof(float), cudaMemcpyHostToDevice));
}

__device__
float CUDA_kernelConvolution(float *matrix, int height, int width, float *kernel, int kSize, int i, int j)
{
    int kOffset = (kSize - 1) / 2;
    float partial = 0.0f;
    for (int y = 0; y < kSize; y++) {
        for (int x = 0; x < kSize; x++) {
            int idx_i = i + y - kOffset;
            int idx_j = j + x - kOffset;

            float matrixValue;
            bool outsideBounds = idx_i < 0 || idx_j < 0 || idx_i >= height || idx_j >= width;
            if (outsideBounds) {
                matrixValue = 0.0f;
            } else {
                matrixValue = matrix[idx_i * width + idx_j];
            }

            partial = fmaf(kernel[y * kSize + x], matrixValue, partial);
        }
    }

    return partial;
}

__device__
double atomicAdd(double *address, double val)
{
    unsigned long long int* address_as_ull = (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;

    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed)));
    } while (assumed != old);

    return __longlong_as_double(old);
}

__global__
void CUDA_applyWeightKernel(float *matrix, float *output, int height, int width)
{
    int y = blockIdx.x;
    int x = threadIdx.x;
    output[y * width + x] = CUDA_kernelConvolution(matrix, height, width, CUDA_weightKernel, CUDA_KERNEL_SIZE, y, x);
}


__global__
void CUDA_normalizeChannels(float *channels, int height, int width, int length)
{
    /**
     * This is one of the cases where float does not have ample precision to deal with
     * both large numbers and small decimals at the same time.
     * A double is therefore REQUIRED when summarizing, although it limits the GPU speed greatly.
     *
     * After the sum have been computed, a regular float will do as a normalization factor.
     */

    __shared__ double sum;
    sum = 0;
    __syncthreads();

    int i = blockIdx.x * width * height * FLOW_CHANNELS;
    int y = threadIdx.x * width;
    for (int c = 0; c < FLOW_CHANNELS; c++) {
        float *channelFrame = &channels[i + (width * height * c)];

        double partial = 0.0f;
        for (int x = 0; x < width; x++)
            partial += channelFrame[y + x] * channelFrame[y + x];

        atomicAdd(&sum, partial);
    }

    __syncthreads();
    if (sum == 0) {
        for (int c = 0; c < FLOW_CHANNELS; c++) {
            float *channelFrame = &channels[i + (width * height * c)];

            for (int x = 0; x < width; x++)
                channelFrame[y + x] = 0;
        }
    } else {
        float factor = sqrt(sum);

        for (int c = 0; c < FLOW_CHANNELS; c++) {
            float *channelFrame = &channels[i + (width * height * c)];

            for (int x = 0; x < width; x++)
                channelFrame[y + x] /= factor;
        }
    }
}

__global__
void CUDA_blurChannels(float *in, float *out, int height, int width, int length, float *kernel, int kSize)
{
    int i = blockIdx.x;
    int y = threadIdx.x;
    for (int c = 0; c < FLOW_CHANNELS; c++) {
        float *channelFrame = &in[i * width * height * FLOW_CHANNELS + (width * height * c)];
        float *output = &out[i * width * height * FLOW_CHANNELS + (width * height * c)];

        for (int x = 0; x < width; x++)
            output[y * width + x] = CUDA_kernelConvolution(channelFrame, height, width, kernel, kSize, y, x);
    }
}

__global__
void CUDA_splitFlowToChannels(float *flowField, float *flowChannels, int width, int height, int length)
{
    int whSize = width * height;
    int y = threadIdx.x * width;
    int i = blockIdx.x * whSize;
    float *channelFrame_nx = &flowChannels[i * FLOW_CHANNELS];
    float *channelFrame_px = &flowChannels[i * FLOW_CHANNELS + (whSize)];
    float *channelFrame_ny = &flowChannels[i * FLOW_CHANNELS + (whSize * 2)];
    float *channelFrame_py = &flowChannels[i * FLOW_CHANNELS + (whSize * 3)];

    for (int x = 0; x < width; x++) {
        int flowIndex = (i + y + x) * 2;

        float vx = flowField[flowIndex];
        float vy = flowField[flowIndex + 1];

        channelFrame_nx[y + x] = fmaxf(0, vx * -1);
        channelFrame_px[y + x] = fmaxf(0, vx);
        channelFrame_ny[y + x] = fmaxf(0, vy * -1);
        channelFrame_py[y + x] = fmaxf(0, vy);
    }
}

__global__
void CUDA_computeFrameSimilarity(float *trained, float *sequence, int tLen, int sLen, int width, int height, float *result)
{
    int t = blockIdx.x;
    int s = threadIdx.x;
    float value = 0;

    for (int c = 0; c < FLOW_CHANNELS; c++) {
        float *tFrame = &trained[t * width * height * FLOW_CHANNELS + (width * height * c)];
        float *sFrame = &sequence[s * width * height * FLOW_CHANNELS + (width * height * c)];

        float tempVal = 0;
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                tempVal += tFrame[y * width + x] * sFrame[y * width + x];

        value += tempVal;
    }

    result[t * sLen + s] = value;
}

__global__
void CUDA_scoreMotionSimilarity(float *matrix, float *rowScore, int height, int width)
{
    float columnMax = 0.0f;
    for (int x = 0; x < width; x++)
        if (matrix[threadIdx.x * width + x] > columnMax)
            columnMax = matrix[threadIdx.x * width + x];

    rowScore[threadIdx.x] = columnMax;
}

__host__
void FlowVectorClassifier_GPU::preprocessSequence_GPU(FrameSequence &host_sequence, float **device_sequence)
{
    float *d_opticalFlow;

    uploadSequence(host_sequence, &d_opticalFlow);

    splitFlowToChannels_GPU(host_sequence.get(0).flowField.rows,
                            host_sequence.get(0).flowField.cols,
                            host_sequence.length(),
                            device_sequence,
                            d_opticalFlow);

    CUDA_CHECK(cudaFree(d_opticalFlow));

    blurChannels_GPU(device_sequence,
                     host_sequence.get(0).flowField.rows,
                     host_sequence.get(0).flowField.cols,
                     host_sequence.length());

    normalizeFlowChannels_GPU(*device_sequence,
                              host_sequence.get(0).flowField.rows,
                              host_sequence.get(0).flowField.cols,
                              host_sequence.length());
}

__host__
void FlowVectorClassifier_GPU::uploadSequence(FrameSequence &sequence, float **device)
{
    size_t frameSize = sequence.get(0).flowField.rows * sequence.get(0).flowField.cols;
    CUDA_CHECK(cudaMalloc((void**)device, sequence.length() * frameSize * sizeof(Point2f)));
    for (int i = 0; i < sequence.length(); i++) {
        assert(sequence.get(i).flowField.isContinuous());

        int offset = i * frameSize * 2;
        CUDA_CHECK(cudaMemcpy((*device) + offset,
                              sequence.get(i).flowField.ptr(),
                              frameSize * sizeof(Point2f),
                              cudaMemcpyHostToDevice));
    }
}

__host__
void FlowVectorClassifier_GPU::downloadSequence(float *device, int rows, int cols, int length, FlowSequence &novel)
{
    size_t frameSize = rows * cols;

    for (int i = 0; i < length; i++) {
        FlowChannels channels;

        for (int j = 0; j < FLOW_CHANNELS; j++) {
            channels[j] = Mat(rows, cols, CV_32FC1, Scalar(0));
            int offset = i * frameSize * FLOW_CHANNELS + j * frameSize;
            CUDA_CHECK(cudaMemcpy(channels[j].ptr(),
                                  device + offset,
                                  frameSize * sizeof(float),
                                  cudaMemcpyDeviceToHost));
        }

        novel.push_back(channels);
    }
}

__host__
void FlowVectorClassifier_GPU::downloadMotionSimilarity(Mat &mat, int height, int width)
{
    mat.create(height, width, CV_32FC1);
    CUDA_CHECK(cudaMemcpy(mat.ptr(), d_motionSimilarity, width * height * sizeof(float), cudaMemcpyDeviceToHost));
}

__host__
ResultClass FlowVectorClassifier_GPU::classify(TrainedDatabase &trainedDatabase, FrameSequence &sequence)
{
    preprocessSequence_GPU(sequence, &d_sequence);

    float bestScore = 0.0;
    FrameSequence bestClass;
    bestClass.setClass("<unknown>");
    Mat motionSimilarity;

    TAKE_TIME_VARS;

    FrameSequence trained = trainedDatabase.getNextVideo();
    while (trained.length() > 0) {
        TAKE_TIME(computeAndFindClass(trained, sequence, bestClass, bestScore, motionSimilarity),
                  trained.length(),
                  "computeAndFindClass CUDA");

        trained = trainedDatabase.getNextVideo();
    }

    CUDA_CHECK(cudaFree(d_sequence));

    ResultClass result;
    result.sequence = bestClass;
    result.motionSimilarity = motionSimilarity;
    return result;
}

__host__
void FlowVectorClassifier_GPU::computeAndFindClass(FrameSequence &trained, FrameSequence &sequence,
                                                   FrameSequence &bestClass, float &bestScore, Mat &similarity)
{
    float *rowScore = new float[trained.length()];

    preprocessSequence_GPU(trained, &d_trained);
    computeFrameSimilarity_GPU(d_trained, d_sequence, trained.length(), sequence.length(), trained.get(0).flowField.cols, trained.get(0).flowField.rows);
    applyWeightKernel_GPU(trained.length(), sequence.length());
    scoreMotionSimilarity_GPU(trained.length(), sequence.length(), rowScore);

    if (DEBUG) {
        Mat motionSimilarity;
        downloadMotionSimilarity(motionSimilarity, trained.length(), sequence.length());
        debugMotionSimilarity(motionSimilarity);
    }

    float score = maximumSimilarity(rowScore, trained.length());
    if (score > bestScore) {
        bestScore = score;
        bestClass = trained;
        downloadMotionSimilarity(similarity, trained.length(), sequence.length());
    }

    delete[] rowScore;
    CUDA_CHECK(cudaFree(d_motionSimilarity));
    CUDA_CHECK(cudaFree(d_trained));
}

__host__
void FlowVectorClassifier_GPU::splitFlowToChannels_GPU(int width, int height, int length, float **channelMap, float *flowField)
{
    messageProgress("Splitting Into Channels...");
    CUDA_CHECK(cudaMalloc((void**)channelMap, width * height * length * FLOW_CHANNELS * sizeof(float)));
    CUDA_splitFlowToChannels<<<length, height>>>(flowField, *channelMap, width, height, length);
    CUDA_CHECK_KERNEL();
    completeProgress("      DONE!\n");
}

__host__
void FlowVectorClassifier_GPU::blurChannels_GPU(float **device, int height, int width, int length)
{
    messageProgress("Blurring channels...");
    float *d_resultBuffer;
    CUDA_CHECK(cudaMalloc((void**)&d_resultBuffer, width * height * length * FLOW_CHANNELS * sizeof(float)));
    CUDA_blurChannels<<<length, height, (height/4) * width * sizeof(float)>>>(*device, d_resultBuffer, height, width, length, d_gaussKernel, GAUSS_SIZE.width);
    CUDA_CHECK_KERNEL();

    CUDA_CHECK(cudaFree(*device));
    *device = d_resultBuffer;

    completeProgress("            DONE!\n");
}

__host__
void FlowVectorClassifier_GPU::normalizeFlowChannels_GPU(float *device, int height, int width, int length)
{
    messageProgress("Normalizing Channels...");
    CUDA_normalizeChannels<<<length, height>>>(device, height, width, length);
    CUDA_CHECK_KERNEL();
    completeProgress("         DONE!\n");
}

__host__
void FlowVectorClassifier_GPU::computeFrameSimilarity_GPU(float *d_trained, float *d_sequence,
                                                          int trainedLength, int sequenceLength,
                                                          int width, int height)
{
    messageProgress("Computing Frame Similarity...");
    CUDA_CHECK(cudaMalloc((void**)&d_motionSimilarity, trainedLength * sequenceLength * sizeof(float)));
    CUDA_computeFrameSimilarity<<<trainedLength, sequenceLength>>>(d_trained, d_sequence, trainedLength, sequenceLength, width, height, d_motionSimilarity);
    CUDA_CHECK_KERNEL();
    completeProgress("   DONE!\n");
}


__host__
void FlowVectorClassifier_GPU::applyWeightKernel_GPU(int height, int width)
{
    messageProgress("Applying Weight Kernel...");
    float *d_tempBuffer;

    CUDA_CHECK(cudaMalloc((void**)&d_tempBuffer, width * height * sizeof(float)));

    CUDA_applyWeightKernel<<<height, width>>>(d_motionSimilarity, d_tempBuffer, height, width);
    CUDA_CHECK_KERNEL();

    CUDA_CHECK(cudaFree(d_motionSimilarity));
    d_motionSimilarity = d_tempBuffer;

    completeProgress("       DONE!\n");
}

__host__
void FlowVectorClassifier_GPU::scoreMotionSimilarity_GPU(int height, int width, float *h_rowScore)
{
    messageProgress("Scoring Motion Similarity...");
    float *d_rowScore;
    CUDA_CHECK(cudaMalloc((void**)&d_rowScore, height * sizeof(float)));

    // This code runs faster with 1 thread and 1 SM than using several with atomic operations
    CUDA_scoreMotionSimilarity<<<1, height>>>(d_motionSimilarity, d_rowScore, height, width);
    CUDA_CHECK_KERNEL();
    completeProgress("    DONE!\n");

    CUDA_CHECK(cudaMemcpy(h_rowScore, d_rowScore, height * sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CHECK(cudaFree(d_rowScore));
}

__host__
void FlowVectorClassifier_GPU::debugMotionSimilarity(Mat motionSimilarity)
{
    float max = 0.0;
    for (int j = 0; j < motionSimilarity.rows; j++)
        for (int k = 0; k < motionSimilarity.cols; k++)
            if (motionSimilarity.at<float>(j, k) > max)
                max = motionSimilarity.at<float>(j, k);

    fprintf(stderr, "MAX = %f\n", max);

    for (int j = 0; j < motionSimilarity.rows; j++)
        for (int k = 0; k < motionSimilarity.cols; k++)
            motionSimilarity.at<float>(j, k) /= max;

    namedWindow("Motion Similarity", 0);
    imshow("Motion Similarity", motionSimilarity);
    waitKey(0);
}

float FlowVectorClassifier_GPU::maximumSimilarity(float *rowScore, int size)
{
    priority_queue<float> que;
    for (int i = 0; i < size; i++)
        que.push(rowScore[i]);

    // Extract the sum of the top T scores
    float total = 0.0;
    for (int i = 0; i < size; i++) {
        total += que.top();
        que.pop();
    }

    return total / size;
}
