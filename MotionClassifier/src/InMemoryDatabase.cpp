#include "InMemoryDatabase.hpp"

InMemoryDatabase::InMemoryDatabase() :
    index(-1)
{

}

InMemoryDatabase::~InMemoryDatabase()
{

}

void InMemoryDatabase::add(FrameSequence &sequence)
{
    classVideo.push_back(sequence);
}

FrameSequence InMemoryDatabase::getNextVideo()
{
    index++;
    if (index >= (int)classVideo.size())
        return FrameSequence();

    return classVideo[index];
}

int InMemoryDatabase::size()
{
    return classVideo.size();
}
