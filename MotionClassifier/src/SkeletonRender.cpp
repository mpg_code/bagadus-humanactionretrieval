#include "SkeletonRender.hpp"

#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;

SkeletonRender::SkeletonRender(FrameSequence &trained, FrameSequence &sequence, Mat motionSimilarity) :
    sequence(sequence)
{
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);

    sequence.setClass(trained.getClass());

    generateTimeline(trained, motionSimilarity);
    adjustSkeleton();
}

SkeletonRender::~SkeletonRender()
{

}

void SkeletonRender::exportToPng(string directory)
{
    for (int i = 0; i < (int)timeline.size() - 1; i++) {
        fprintf(stderr, "\rExporting frames: %3.0f%%", (100.0f * (i + 1)) / timeline.size());
        Mat image = sequence.get(i).frame;
        char namebuf[128];

        sprintf(namebuf, "%s%04d.png", directory.c_str(), i);
        drawSkeleton(timeline[i], image);
        exportPng(string(namebuf), image);
    }
    fprintf(stderr, "\n");
}

void SkeletonRender::exportToCsv(string filename)
{
    fprintf(stderr, "\rExporting CSV...");

    ofstream csv;
    csv.open(filename);

    csv << "sessionID,timecode,frameID,frameX,frameY,width,height,players,control,processed,motion,sequenceNo,"
        "bb_x1,bb_y1,bb_x2,bb_y2,h_x,h_y,s_lx,s_ly,s_rx,s_ry,e_lx,e_ly,e_rx,e_ry,n_lx,n_ly,n_rx,n_ry,"
        "l_lx,l_ly,l_rx,l_ry,k_lx,k_ly,k_rx,k_ry,f_lx,f_ly,f_rx,f_ry"
        << endl;

    for (int i = 0; i < (int)timeline.size() - 1; i++)
        exportCsv(csv, i);

    fprintf(stderr, "   DONE!\n");

    csv.close();
}

void SkeletonRender::generateTimeline(FrameSequence &trained, Mat &motionSimilarity)
{
    // For every sequence frame
    for (int col = 0; col < motionSimilarity.cols; col++) {
        float max = 0;
        SQLEntity ent;

        // Find best matching trained frame
        for (int i = 0; i < motionSimilarity.rows; i++) {
            if (motionSimilarity.at<float>(i, col) > max) {
                max = motionSimilarity.at<float>(i, col);
                ent = *(trained.get(i).entity);
            }
        }

        // If found, use the best-matching frame, this is done by overlaying the skeleton on top of one sequence.
        if (max > 0) {
            // First obtain the size bounding box of elm
            float elmBBSizeX;
            float elmBBSizeY;

            if (ent.get(SQLEntity::BBEnd).x == -1 && ent.get(SQLEntity::BBStart).x == -1
                && ent.get(SQLEntity::BBEnd).y == -1 && ent.get(SQLEntity::BBStart).y == -1) {
                elmBBSizeX = ent.get(SQLEntity::Size).x;
                elmBBSizeY = ent.get(SQLEntity::Size).y;
            } else {
                elmBBSizeX = ent.get(SQLEntity::BBEnd).x - ent.get(SQLEntity::BBStart).x;
                elmBBSizeY = ent.get(SQLEntity::BBEnd).y - ent.get(SQLEntity::BBStart).y;
            }

            // Centering is done by taking the size difference (which becomes the total offset offset from one another)
            // and divide that by 2 to get how much to move (centering contains half of the size difference)
            float centerDiffX = (sequence.get(col).roi.width - elmBBSizeX) / 2;
            float centerDiffY = (sequence.get(col).roi.height - elmBBSizeY) / 2;

            for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
                // If joint is visible (aka. not obscured)
                if (ent[i].x != -1 && ent[i].y != -1) {
                    // Add the center adjustemnt and sequence position offset to the existing elm joint positions
                    // to get final absolute full-frame joint positions
                    ent[i].x += sequence.get(col).roi.x + centerDiffX;
                    ent[i].y += sequence.get(col).roi.y + centerDiffY;
                }
            }

            timeline.push_back(ent);
        }
    }
}

void SkeletonRender::adjustSkeleton()
{
    // Frame positions are made to match excact boundingBoxSize*2,
    // but at times that will result in negative values for the image position.
    // DataGenerator will produce negative-indexed images, but will instead hold it to the closest edge.
    // The final result is an offset that must be re-adjusted at render-time.
    for (SQLEntity &skeleton : timeline) {
        if (skeleton.get(SQLEntity::FramePos).x < 0) {
            for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
                if (skeleton[i].x != -1)
                    skeleton[i].x -= skeleton.get(SQLEntity::FramePos).x * 2;
        }

        if (skeleton.get(SQLEntity::FramePos).y < 0) {
            for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
                if (skeleton[i].y != -1)
                    skeleton[i].y -= skeleton.get(SQLEntity::FramePos).y * 2;
        }
    }
}

void SkeletonRender::exportCsv(ofstream &csv, int frame)
{
    FrameSequenceElement elm = sequence.get(frame);
    SQLEntity sql = timeline.at(frame);
    char framename[64];
    sprintf(framename, "\"%04d.png\"", frame);

    csv << "0,"
        << "\"" << elm.position.timecode << "\","
        << framename << ","
        << "0,0,"
        << elm.frame.cols << ","
        << elm.frame.rows << ","
        << "1,0,1,"
        << "\"" << sequence.getClass()  << "\","
        << "0,-1,-1,-1,-1,";

    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        csv << sql[i].x << "," << sql[i].y << ",";

    csv << endl;
}

void SkeletonRender::exportPng(string filename, Mat frame)
{
    assert(imwrite(filename, frame, compression_params));
}

void SkeletonRender::drawSkeleton(SQLEntity &skeleton, Mat &frame)
{
    drawLine(skeleton.get(SQLEntity::ShoulderLeft), skeleton.get(SQLEntity::ElbowLeft), frame);
    drawLine(skeleton.get(SQLEntity::HandLeft), skeleton.get(SQLEntity::ElbowLeft), frame);
    drawLine(skeleton.get(SQLEntity::LegLeft), skeleton.get(SQLEntity::KneeLeft), frame);
    drawLine(skeleton.get(SQLEntity::FootLeft), skeleton.get(SQLEntity::KneeLeft), frame);

    drawLine(skeleton.get(SQLEntity::ShoulderRight), skeleton.get(SQLEntity::ElbowRight), frame);
    drawLine(skeleton.get(SQLEntity::HandRight), skeleton.get(SQLEntity::ElbowRight), frame);
    drawLine(skeleton.get(SQLEntity::LegRight), skeleton.get(SQLEntity::KneeRight), frame);
    drawLine(skeleton.get(SQLEntity::FootRight), skeleton.get(SQLEntity::KneeRight), frame);

    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        drawPoint(skeleton[i], frame);
}

void SkeletonRender::drawPoint(Point2f position, Mat &frame)
{
    // If obscured, do not draw
    if (position.x == -1 && position.y == -1)
        return;

    circle(frame, position, 0, Scalar(255, 0, 0));
    circle(frame, position, 1, Scalar(255, 0, 0));
}

void SkeletonRender::drawLine(Point2f a, Point2f b, Mat &frame)
{
    if ((a.x == -1 && a.y == -1) || (b.x == -1 && b.y == -1))
        return;

    line(frame, a, b, Scalar(0, 0, 255));
}
