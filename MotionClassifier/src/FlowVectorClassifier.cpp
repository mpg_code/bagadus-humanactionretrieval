#include "FlowVectorClassifier.hpp"
#include "OpticalFlow.hpp"
#include "timetaker.h"

#include <cfloat>
#include <queue>
#include <stdio.h>
#include <math.h>
#include <omp.h>

#define DEBUG false

using namespace std;
using namespace cv;

FlowVectorClassifier::FlowVectorClassifier()
{

}

FlowVectorClassifier::~FlowVectorClassifier()
{

}

ResultClass FlowVectorClassifier::classify(TrainedDatabase &trainedDatabase, FrameSequence &sequence)
{
    FlowSequence novel = preprocessSequence(sequence);

    float bestScore = 0.0;
    FrameSequence bestClass;
    Mat motionSimilarity;
    bestClass.setClass("<unknown>");

    TAKE_TIME_VARS;

    FrameSequence trained = trainedDatabase.getNextVideo();
    while (trained.length() > 0) {
        TAKE_TIME(computeAndFindClass(trained, novel, bestClass, bestScore, motionSimilarity),
                  trained.length(),
                  "computeAndFindClass CPU");

        trained = trainedDatabase.getNextVideo();
    }

    ResultClass result;
    result.sequence = bestClass;
    result.motionSimilarity = motionSimilarity;
    return result;
}

void FlowVectorClassifier::computeAndFindClass(FrameSequence &trained, FlowSequence &sequence,
                                               FrameSequence &bestClass, float &bestScore, Mat &similarity)
{
    FlowSequence trainedClass = preprocessSequence(trained);
    Mat motionSimilarity = computeFrameSimilarity(trainedClass, sequence);
    applyWeightKernel(motionSimilarity);
    float score = scoreMotionSimilarity(motionSimilarity);

    if (DEBUG)
        debugMotionSimilarity(motionSimilarity, trained.getClass());

    if (score > bestScore) {
        bestScore = score;
        bestClass = trained;
        similarity = motionSimilarity;
    }
}


FlowVectorClassifier::FlowSequence FlowVectorClassifier::preprocessSequence(FrameSequence &sequence)
{
    FlowSequence flowSequence = splitFlowToChannels(sequence);
    blurFlowChannels(flowSequence);
    normalizeFlowChannels(flowSequence);
    return flowSequence;
}

FlowVectorClassifier::FlowSequence FlowVectorClassifier::splitFlowToChannels(FrameSequence &sequence)
{
    FlowSequence flowSequence;
    OpticalFlow opticalFlow;

    for (int i = 0; i < sequence.length(); i++) {
        monitorProgress("\rExpanding channels: %3.0f%%", (100.0f * i) / sequence.length());

        Mat flowField = sequence.get(i).flowField;
        Mat *matChannels = opticalFlow.expandChannels(flowField);

        FlowChannels channels;
        channels[0] = matChannels[0];
        channels[1] = matChannels[1];
        channels[2] = matChannels[2];
        channels[3] = matChannels[3];

        flowSequence.push_back(channels);
    }
    completeProgress("\rExpanding channels: 100%%\n");

    return flowSequence;
}

void FlowVectorClassifier::blurFlowChannels(FlowSequence &sequence)
{
    for (uint frame = 0; frame < sequence.size(); frame++) {
        monitorProgress("\rBlurring channels: %3.0f%%", (100.0f * frame) / sequence.size());

        for (uint channel = 0; channel < FLOW_CHANNELS; channel++) {
            Mat blurredChannel;
            GaussianBlur(sequence[frame][channel], blurredChannel, GAUSS_SIZE, 0);
            sequence[frame][channel] = blurredChannel;
        }
    }
    completeProgress("\rBlurring channels: 100%%\n");
}

void FlowVectorClassifier::normalizeFlowChannels(FlowSequence &sequence)
{
    for (uint frame = 0; frame < sequence.size(); frame++) {
        monitorProgress("\rNormalizing channels: %3.0f%%", (100.0f * frame) / sequence.size());

        double sum = channelsSum(sequence[frame]);
        sum = sqrt(sum);
        normalizeSingleFrameChannels(sequence[frame], sum);
    }
    completeProgress("\rNormalizing channels: 100%%\n");
}

double FlowVectorClassifier::channelsSum(FlowChannels &channels)
{
    double sum = 0;
    for (int channel = 0; channel < FLOW_CHANNELS; channel++)
        for (int y = 0; y < channels[channel].rows; y++)
            for (int x = 0; x < channels[channel].cols; x++)
                sum += channels[channel].at<float>(y, x) * channels[channel].at<float>(y, x);

    return sum;
}

void FlowVectorClassifier::normalizeSingleFrameChannels(FlowChannels &channels, float factor)
{
    if (factor == 0) {
        fprintf(stderr, "%s:%d > Factor is %f\n", __FILE__, __LINE__, factor);

        for (int channel = 0; channel < FLOW_CHANNELS; channel++)
            channels[channel] = 0.0f;

        return;
    }

    for (int channel = 0; channel < FLOW_CHANNELS; channel++)
        for (int y = 0; y < channels[channel].rows; y++)
            for (int x = 0; x < channels[channel].cols; x++)
                channels[channel].at<float>(y, x) /= factor;
}

Mat FlowVectorClassifier::computeFrameSimilarity(FlowSequence &trained, FlowSequence &sequence)
{
    Mat frameSimilarity(trained.size(), sequence.size(), CV_32FC1, Scalar(0.0f));

#   pragma omp parallel for
    for (uint f_train = 0; f_train < trained.size(); f_train++) {
        for (uint f_seq = 0; f_seq < sequence.size(); f_seq++) {
            if (omp_get_thread_num() == 0) {
                monitorProgress("\rComputing frame similarity: %3.0f%%",
                                (100.0f * (f_train * sequence.size() + f_seq))
                                / (trained.size() * sequence.size()));
            }

            frameSimilarity.at<float>(f_train, f_seq) = frameProduct(trained[f_train], sequence[f_seq]);
        }
    }
    completeProgress("\rComputing frame similarity: 100%%\n");

    return frameSimilarity;
}


float FlowVectorClassifier::frameProduct(FlowChannels &trained, FlowChannels &sequence)
{

    float sum = 0.0;
    for (int i = 0; i < FLOW_CHANNELS; i++) {
        sum += channelProduct(trained[i], sequence[i]);
    }

    return sum;
}

float FlowVectorClassifier::channelProduct(Mat &trained, Mat &sequence)
{
    assert(trained.cols == sequence.cols && trained.rows == sequence.rows);

    float product = 0.0;
    for (int y = 0; y < trained.rows; y++)
        for (int x = 0; x < trained.cols; x++)
            product += trained.at<float>(y, x) * sequence.at<float>(y, x);

    return product;
}

void FlowVectorClassifier::applyWeightKernel(Mat &motionSimilarity)
{
    Mat originalSimilarity;
    motionSimilarity.copyTo(originalSimilarity);

    for (int train = 0; train < motionSimilarity.rows; train++) {
        for (int seq = 0; seq < motionSimilarity.cols; seq++) {
            monitorProgress("\rApplying weight kernel: %3.0f%%",
                            (100.0f * (train * motionSimilarity.rows + seq)) / (motionSimilarity.rows * motionSimilarity.cols));

            motionSimilarity.at<float>(train, seq) = kernelConvolution(originalSimilarity, train, seq);
        }
    }
    completeProgress("\rApplying weight kernel: 100%%\n");
}

float FlowVectorClassifier::kernelConvolution(Mat &frameSimilarity, int i, int j)
{
    float partial = 0.0f;

    for (int y = 0; y < kernelSize; y++) {
        for (int x = 0; x < kernelSize; x++) {
            int idx_i = i + y - kernelOffset;
            int idx_j = j + x - kernelOffset;

            if (idx_i < 0 || idx_j < 0 || idx_i >= frameSimilarity.rows || idx_j >= frameSimilarity.cols)
                continue;

            partial += weightKernel[y][x] * frameSimilarity.at<float>(idx_i, idx_j);
        }
    }

    return partial;
}

float FlowVectorClassifier::scoreMotionSimilarity(Mat &motionSimilarity)
{
    priority_queue<float> que;
    for (int y = 0; y < motionSimilarity.rows; y++) {
        float columnMax = 0.0f;

        for (int x = 0; x < motionSimilarity.cols; x++)
            if (motionSimilarity.at<float>(y, x) > columnMax)
                columnMax = motionSimilarity.at<float>(y, x);

        que.push(columnMax);
    }

    float maxScore = 0.0;
    for (int i = 0; i <motionSimilarity.rows; i++) {
        maxScore += que.top();
        que.pop();
    }

    return maxScore / motionSimilarity.rows;
}

void FlowVectorClassifier::debugMotionSimilarity(Mat motionSimilarity, string classID)
{
    // Log output
    fprintf(stderr, "ME_MATRIX ||\n");
    for (int j = 0; j < motionSimilarity.rows; j++) {
        fprintf(stderr, "ME_MATRIX ||  ");
        for (int k = 0; k < motionSimilarity.cols; k++) {
            fprintf(stderr, "  %8.4f", motionSimilarity.at<float>(j, k));
        }
        fprintf(stderr, "\nME_MATRIX ||\n");
    }

    // Find MAX
    float max = 0.0;
    for (int j = 0; j < motionSimilarity.rows; j++)
        for (int k = 0; k < motionSimilarity.cols; k++)
            if (motionSimilarity.at<float>(j, k) > max)
                max = motionSimilarity.at<float>(j, k);

    fprintf(stderr, "MAX = %f\n", max);

    // Normalize for dispplay
    for (int j = 0; j < motionSimilarity.rows; j++)
        for (int k = 0; k < motionSimilarity.cols; k++)
            motionSimilarity.at<float>(j, k) /= max;

    // Display
    namedWindow(classID, 0);
    imshow(classID, motionSimilarity);
    waitKey(0);
}
