#include "LinearDatabase.hpp"
#include "SimpleCSVReader.hpp"

using namespace std;
using namespace cv;

#define ONE_SECOND 1000

LinearDatabase::LinearDatabase(TrainedSetSynchronizer &stream, vector<int> skipSequences) :
    stream(stream),
    skipList(skipSequences)
{
    if (skipSequences.size() > 0) {
        printf("Skipping trained sequences:");
        for (int s : skipSequences)
            printf(" %d", s);
        printf("\n");
    }

    stream.syncVideoToCsv(true);
}

LinearDatabase::~LinearDatabase()
{

}

FrameSequence LinearDatabase::getNextVideo()
{
    FrameSequence sequence;

    fprintf(stderr, "Creating new sequence...");
    while (true) {
        SQLEntity *cur = stream.getPlayer();
        if (cur == NULL)
            break;

        Mat flow = stream.getVideo();
        if (flow.empty())
            break;

        sequence.add(cur, stream.getPosition(), flow);
        sequence.setClass(cur->get(SQLEntity::Motion));

        stream.nextFrame();
        if (isClipBreak(cur, stream.getPlayer()))
            break;
    }

    if (skipSequence(sequence)) {
        fprintf(stderr, "  SKIP (length = %d, seqNo = %d)!\n",
                sequence.length(),
                sequence.length() > 0 ? sequence.get(0).entity->get(SQLEntity::SequenceNo) : -1);

        return getNextVideo();
    }

    if (sequence.length() == 0)
        fprintf(stderr, "  EoF!\n");
    else
        fprintf(stderr, "  DONE (length = %d, seqNo = %d)!\n",
                sequence.length(),
                sequence.length() > 0 ? sequence.get(0).entity->get(SQLEntity::SequenceNo) : -1);

    return sequence;
}

bool LinearDatabase::isClipBreak(SQLEntity* cur, SQLEntity *next)
{
    if (cur == NULL || next == NULL)
        return true;

    return cur->get(SQLEntity::Motion).compare(next->get(SQLEntity::Motion)) != 0
        || SimpleCSVReader::stringToTime(next->get(SQLEntity::Timecode))
        - SimpleCSVReader::stringToTime(cur->get(SQLEntity::Timecode)) > ONE_SECOND;
}

bool LinearDatabase::skipSequence(FrameSequence &sequence)
{
    if (sequence.length() == 0)
        return false;

    for (int s : skipList)
        if (s == sequence.get(0).entity->get(SQLEntity::SequenceNo))
            return true;

    return false;
}
