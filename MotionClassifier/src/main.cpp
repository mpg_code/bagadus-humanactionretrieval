#include "StreamSynchronizer.hpp"
#include "CameraFieldMapper.hpp"
#include "FrameSequence.hpp"
#include "SigSevHandler.hpp"
#include "timetaker.h"
#include "VideoSequenceAnalyzer.hpp"
#include "LinearDatabase.hpp"
#include "TrainedSetSynchronizer.hpp"

#include <stdio.h>
#include <vector>
#include <string>

#include <opencv2/opencv.hpp>
#include <opencv2/gpu/gpu.hpp>

//#define DEBUG

using namespace cv;
using namespace std;
using namespace gpu;

static int processedFrames;
static VideoSequenceAnalyzer *analyzer;
static CameraFieldMapper *mapper;

struct TrainingVideo {
    string classID;
    string csvFile;
};

inline StreamSynchronizer* createStream(char *csvData, char *clipDirectory, char *clipIndexFile)
{
    StreamSynchronizer *stream = new StreamSynchronizer(csvData, clipDirectory, clipIndexFile);

    stream->syncCsvToVideo(true);
    stream->syncVideoToCsv(true);

    return stream;
}

// Takes all the video sequences and puts them into the classifier
void constructClassifier(vector<struct TrainingVideo> list, char *clipDirectory, char *clipIndexFile, int trackID)
{
    processedFrames = 0;

    for (struct TrainingVideo video : list) {
        StreamSynchronizer *stream = createStream((char*)video.csvFile.c_str(), clipDirectory, clipIndexFile);
        processedFrames += analyzer->extractToClassifier(*stream, video.classID, trackID);
        delete stream;
    }
}


vector<int> obtainSkipList(char *skipFile) {
    vector<int> sequences;

    if (!strncmp(skipFile, "NULL", strlen("NULL")))
        return sequences;

    ifstream clipList;
    clipList.open(skipFile, ifstream::in);
    if (!clipList.is_open()) {
        fprintf(stderr, "ERROR: Cannot open video clips skip file: %s\n", skipFile);
        exit(EXIT_FAILURE);
    }

    string line;
    while(getline(clipList, line)) {
        int seq;
        sscanf(line.c_str(), "%d", &seq);
        sequences.push_back(seq);
    }

    clipList.close();

    return sequences;
}

void analyzeVideo(char *sqlFile, char *serFile, char *csvData, char *clipDirectory, char *clipIndexFile, char *skipFile, int trackID)
{
    StreamSynchronizer *stream = createStream(csvData, clipDirectory, clipIndexFile);
    TrainedSetSynchronizer tsStream(sqlFile, serFile);
    LinearDatabase db(tsStream, obtainSkipList(skipFile));

    string classID = analyzer->classifySequence(db, *stream, trackID, processedFrames);
    printf("Class is: %s\n", classID.c_str());

    delete stream;
}

void analyzeVideo(char *csvData, char *clipDirectory, char *clipIndexFile, int trackID)
{
    StreamSynchronizer *stream = createStream(csvData, clipDirectory, clipIndexFile);
    string classID = analyzer->classifySequence(*stream, trackID, processedFrames);
    printf("Class is: %s\n", classID.c_str());

    delete stream;
}

vector<struct TrainingVideo> readTrainingVideo(char *trainingFile)
{
    ifstream clipList;
    clipList.open(trainingFile, ifstream::in);
    if (!clipList.is_open()) {
        fprintf(stderr, "ERROR: Cannot open video clips listings file: %s\n", trainingFile);
        exit(EXIT_FAILURE);
    }

    string line;
    char tmp_name[128];
    char tmp_class[16];
    vector<struct TrainingVideo> list;
    while(getline(clipList, line)) {
        struct TrainingVideo clip;
        sscanf(line.c_str(), "%s %s", tmp_class, tmp_name);

        clip.csvFile = tmp_name;
        clip.classID = tmp_class;
        list.push_back(clip);
    }

    clipList.close();

    return list;
}

void printClassList(vector<struct TrainingVideo> &list)
{
    printf("\n===============================\n=====   List of Classes   =====\n===============================\n");
    for (struct TrainingVideo v : list)
        printf("CLASS: %-15s\t\tFILE: %s\n", v.classID.c_str(), v.csvFile.c_str());
    printf("===============================\n\n");
}

bool stringEndsWith(char* base, const char* str)
{
    int blen = strlen(base);
    int slen = strlen(str);
    return (blen >= slen) && (0 == strcmp(base + blen - slen, str));
}

int main(int argc, char** argv)
{
    printf("\nProvided Arguments:\n");
    for (int i = 0; i < argc; i++)
        printf("\t[%d] = %s\n", i, argv[i]);
    printf("\n\n");

    if (argc < 11) {
        printf("ERROR: Requires 11 parameters, %d passed.\n"
               "Usage:\n\t%s (<sqlFile.csv> <opticalFlow.ser.zlib> | <trainingSequences.txt> <trainingDir/>)\n"
               "\t<zxyFile.csv> <clipsDir/> <clipsIndexFile.txt> <boundingSize> <camera.config> <field.config> <background.png>\n"
               "\t<skipList.txt> [norender (classify, no image export)]\n", argc, argv[0]);

        return EXIT_FAILURE;
    }

    bool renderSequence = true;
    if (argc == 12)
        renderSequence = strncmp(argv[11], "norender", strlen("norender") == 0 ? false : true);

#ifdef USE_CUDA
    if (getCudaEnabledDeviceCount() == 0) {
        printf("ERROR: Requires CUDA compatible device, none found.\n");
        return EXIT_FAILURE;
    }

    printf("CUDA devices: %d\n", getCudaEnabledDeviceCount());
#endif

    printf("Rendering final output: %s\n", renderSequence ? "yes" : "no");

    // Set up the signal handler to get stacktrace on OpenCV assertion failure
    SigSevHandler::enableSignalHandler(argv[0]);

    TAKE_TIME_VARS;
    mapper = new CameraFieldMapper(argv[7], argv[8]);
    int boundingSize = atoi(argv[6]);
    analyzer = new VideoSequenceAnalyzer(*mapper, boundingSize, renderSequence, imread(argv[9], CV_LOAD_IMAGE_COLOR));

    if (stringEndsWith(argv[1], ".csv")) {
        TAKE_TIME(analyzeVideo(argv[1], argv[2], argv[3], argv[4], argv[5], argv[10], 9), processedFrames, "analyzeVideo (SQL)");

    } else if (stringEndsWith(argv[1], ".txt")) {
        vector<struct TrainingVideo> list = readTrainingVideo(argv[1]);
        printClassList(list);
        TAKE_TIME(constructClassifier(list, argv[4], argv[5], 9), processedFrames, "constructClassifier (Video)");
        TAKE_TIME(analyzeVideo(argv[3], argv[4], argv[5], 9), processedFrames, "analyzeVideo (Video)");

    } else {
        printf("ERROR: Unknown file extension for argument 1: '%s'\n", argv[1]);
        return EXIT_FAILURE;
    }

    delete analyzer;
    delete mapper;
    return EXIT_SUCCESS;
}
