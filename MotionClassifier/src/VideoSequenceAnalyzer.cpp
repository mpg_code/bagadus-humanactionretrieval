#include "VideoSequenceAnalyzer.hpp"
#include "BoundingBoxExtractor.hpp"
#include "OpticalFlow.hpp"
#include "SkeletonRender.hpp"
#include "ResultClass.hpp"

using namespace std;
using namespace cv;

VideoSequenceAnalyzer::VideoSequenceAnalyzer(CameraFieldMapper &mapper, int boundingSize, bool render, Mat background) :
    mapper(mapper),
    boundingSize(boundingSize),
    render(render),
    background(background)
{

}

VideoSequenceAnalyzer::~VideoSequenceAnalyzer()
{

}

void VideoSequenceAnalyzer::computeOpticalFlow(FrameSequence &sequence)
{
    OpticalFlow opticalFlow;
    for (int i = 1; i < sequence.length(); i++) {
        fprintf(stderr, "\rComputing Optical Flow: %3.0f%%", (100.0f * i) / sequence.length());

        struct FrameSequenceElement elm_A = sequence.get(i-1);
        struct FrameSequenceElement elm_B = sequence.get(i);
        Mat flowField(elm_A.normalized.size(), CV_32FC2);
        opticalFlow.computeDense(elm_A.normalized, elm_B.normalized, flowField);

        sequence.setFlowField(flowField, i);
    }
    fprintf(stderr, "\rComputing Optical Flow: 100%%\n");
}


int VideoSequenceAnalyzer::extractToClassifier(StreamSynchronizer &stream, string classID, int trackID)
{
    FrameSequence sequence = readVideo(stream, trackID);
    computeOpticalFlow(sequence);
    sequence.removeFirstFrame(); // First frame never has any optical flow
    sequence.setClass(classID);
    memoryDatabase.add(sequence);
    return sequence.length();
}

string VideoSequenceAnalyzer::classifySequence(LinearDatabase &linearDatabase,
                                               StreamSynchronizer &stream,
                                               int trackID, int &processedFrames)
{
    FrameSequence sequence = readVideo(stream, trackID);
    computeOpticalFlow(sequence);
    sequence.removeFirstFrame(); // First frame never has any optical flow

    ResultClass result = classifier.classify(linearDatabase, sequence);

    if (render) {
        SkeletonRender renderer(result.sequence, sequence, result.motionSimilarity);
        renderer.exportToPng("render/");
        renderer.exportToCsv("render/render.csv");
    }

    processedFrames = sequence.length();
    return result.sequence.getClass();
}

string VideoSequenceAnalyzer::classifySequence(StreamSynchronizer &stream, int trackID, int &processedFrames)
{
    FrameSequence sequence = readVideo(stream, trackID);
    computeOpticalFlow(sequence);
    sequence.removeFirstFrame(); // First frame has never any optical flow
    ResultClass result = classifier.classify(memoryDatabase, sequence);

    // Cannot render/export, since no joint locations are available

    processedFrames = sequence.length();
    return result.sequence.getClass();
}

FrameSequence VideoSequenceAnalyzer::readVideo(StreamSynchronizer &stream, int trackID)
{
    unsigned char *framedata = (unsigned char*)malloc((stream.getHeight() + stream.getHeight()/2) * (stream.getWidth()));

    if (!stream.getVideo(framedata)) {
        fprintf(stderr, "No video frames @ %s line %d.\n", __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }

    int progressCounter = 0;
    bool firstFrame = true;
    FrameSequence sequence;
    BoundingBoxExtractor BBExtractor(boundingSize, stream.getWidth(), stream.getHeight(), background);
    while (true) {
        struct PlayerPosition player = getNextPosition(stream, trackID);
        if (player.eof)
            break;

        progressCounter++;
        fprintf(stderr, "\rExtracting video image: %d", progressCounter);

        mapper.transformPlayer(player);
        if (!insideFrame(player, stream.getWidth(), stream.getHeight()))
            continue;

        if (!stream.getVideo(framedata))
            break;

        struct FrameSequenceElement elm = BBExtractor.computeFrameSequenceElement(player, BBExtractor.YUVtoMat(framedata));
        sequence.add(elm);

        if (firstFrame) {
            firstFrame = false;
            BBExtractor.freeTracking();
        }
    }
    fprintf(stderr, "\n"); // Clear processing log

    free(framedata);

    BBExtractor.zoomScaleSequence(sequence.getAll());

    return sequence;
}

// The program tracks one player at a time to generate multi-frame samples.
// Therefore, CSV data is the main source of progress while the video stream
// is kept along with it.
inline struct PlayerPosition VideoSequenceAnalyzer::getNextPosition(StreamSynchronizer &stream, int trackID)
{
    struct PlayerPosition player;

    bool terminate = false;
    int limit = 0;
    player.id = -1;
    while (player.id != trackID) {
        limit++;

        if (!stream.getPlayer(player)) {
            terminate = true;
            break;
        }

        if (limit >= MAX_PLAYER_ID * 2) {
            printf("\nNo player or more data for player with ID %d.\n", trackID);
            terminate = true;
            break;
        }
    }

    // Cannot break out twice out of a loop, so this is needed
    if (terminate)
        player.eof = true;
    else
        player.eof = false;

    return player;
}

inline bool VideoSequenceAnalyzer::insideFrame(struct PlayerPosition &player, int frameWidth, int frameHeight)
{
    return !(player.x < 0 || player.y < 0 || player.x > frameWidth || player.y > frameHeight);
}
