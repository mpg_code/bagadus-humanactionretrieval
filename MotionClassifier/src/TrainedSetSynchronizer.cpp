#include "TrainedSetSynchronizer.hpp"
#include "SimpleCSVReader.hpp"

#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <fstream>
#include <iostream>

/**
 * CSV is here frame-exact placement, which means CVS defines time.
 * The video is then following frame-excact value obtained by the
 * CSV (SQLEntity) element.
 *
 * This class assumes csv file and serialized file starts on the excact same frame.
 */

#define DEBUG false

#if DEBUG
#    define dLog(argv...) { fprintf(stderr, argv); }
#    define dLogWait(argv...) {fprintf(stderr, argv); cin.ignore(); }
#else
#    define dLog(argv...)
#    define dLogWait(argv...)
#endif

using namespace cv;
using namespace std;
using namespace boost::archive;
using namespace boost::iostreams;

TrainedSetSynchronizer::TrainedSetSynchronizer(char *csvFilename, char *serFilename)
{
    sql = new SQLDataReader(csvFilename);

    currentPosition = sql->next();
    nextPosition = sql->next();

    ifs = new ifstream(serFilename, fstream::binary | fstream::in);
    dataFile = new filtering_streambuf<input>();
    dataFile->push(zlib_decompressor());
    dataFile->push(*ifs);
    video = new binary_iarchive(*dataFile);

    readVideoFrame();

    dLogWait("\n\nCSV File: %s\nSerialized File: %s\n\n", csvFilename, serFilename);
}

TrainedSetSynchronizer::~TrainedSetSynchronizer()
{
    delete currentPosition;
    delete nextPosition;
    delete sql;
    delete video;
    delete ifs;
    delete dataFile;
}

size_t TrainedSetSynchronizer::findStartTime(char *serFilename)
{
    int lastDir = 0;
    for (int i = 0; i < (int)strlen(serFilename); i++)
        if (serFilename[i] == '/')
            lastDir = i + 1;

    string extension(&(serFilename[lastDir]));
    return SimpleCSVReader::stringToTime(extension.erase(extension.length() - 9, extension.length()));
}

bool TrainedSetSynchronizer::readVideoFrame()
{
    try {
        (*video) >> frame;
    } catch (archive_exception e) {
        fprintf(stderr, "ARCHIVE ERROR %d: '%s' in %s:%d\n", e.code, e.what(), __FUNCTION__, __LINE__);
        exit(EXIT_FAILURE);
    }

    return true;
}

Mat TrainedSetSynchronizer::getVideo()
{
    dLog("%s CALL\n", __FUNCTION__);
    return frame.mat;
}

struct PlayerPosition TrainedSetSynchronizer::getPosition()
{
    dLog("%s CALL\n", __FUNCTION__);
    struct PlayerPosition position;
    position.heading = frame.heading;
    position.direction = frame.direction;
    position.speed = frame.speed;
    return position;
}

SQLEntity* TrainedSetSynchronizer::getPlayer()
{
    dLog("%s CALL\n", __FUNCTION__);
    return currentPosition;
}

SQLEntity* TrainedSetSynchronizer::peakNextPlayer()
{
    dLog("%s CALL\n", __FUNCTION__);
    return nextPosition;
}

void TrainedSetSynchronizer::nextFrame()
{
    dLog("%s CALL\n", __FUNCTION__);
    currentPosition = nextPosition;
    nextPosition = sql->next();
    syncVideoToCsv();
}

size_t TrainedSetSynchronizer::getCsvFrame()
{
    string identifier = currentPosition->get(SQLEntity::FrameID);
    identifier = identifier.erase(0, identifier.length() - 10); // Nothing but the last 10 chars are of interest
    identifier = identifier.erase(identifier.length() - 4, identifier.length()); // Remove file extension
    return (size_t)atoi(identifier.c_str());
}

bool TrainedSetSynchronizer::syncVideoToCsv(bool showProgress)
{
    if (currentPosition == NULL)
        return false;

    size_t currentFrame = getCsvFrame();

    dLog("ADVANCING VIDEO: video frame = %5d, SQL frame = %5ld (video from target = %5ld)\n",
         frame.frame, currentFrame, frame.frame - currentFrame);

    while (frame.frame < (int)currentFrame) {
        if (!readVideoFrame())
            return false;
    }

    dLogWait("    SUCCESS    : video frame = %5d, SQL frame = %5ld (video from target = %5ld)\n",
             frame.frame, currentFrame, frame.frame - currentFrame);


    if (showProgress && !DEBUG)
        fprintf(stderr, "\n");

    return true;
}
