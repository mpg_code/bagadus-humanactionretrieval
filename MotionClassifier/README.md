# MotionClassifier

Motion classifier consists of the project MotionClassifier.

## MotionClassifier
The _MotionClassifier_ program takes the output generated from the _DataGenerator_ program plus the merged training data from the _ResultChecker_ program and uses these to classify a query sequence to obtain a class label and perform skeleton reprojection based on trained data. The produced output is a video sequence with the skeleton projected onto every frame and a CSV file with the exact joint positions.

The program can be run with either a trained database for full classification and skeleton reprojection, or it can be used for classification only that uses an in-memory database (read in).

### Input
#### Classification and In-Memory Database:
1. List of video sequences (ZXY files) to read in
2. Directory where video sequence ZXY files reside
3. Query sequence tracker data
4. Directory for video sources
5. Index list of clips found in directory containing video sources
6. Size of the bounding box (actual size becomes 2x this value)
7. Camera configuration file for homography mapping
8. Field configuration file for homography mapping
9. Background image
10. List of trained sequence numbers to not include in the classification
11. Do not render output (optional)

#### Trained Data Classification and Skeleton Reprojection:
1. Annotated (trained) skeleton data
2. Serialized file with optical flow data (obtained from DataGenerator)
3. Query sequence tracker data
4. Directory for video sources
5. Index list of clips found in directory containing video sources
6. Size of the bounding box, actual size becomes 2x this value
7. Camera configuration file for homography mapping
8. Field configuration file for homography mapping
9.  Background image for background subtraction initialization
10. List of trained sequence numbers to not include in the classification
11. Do not render output (optional)

### Output
#### Classification and In-Memory Database:
- Classification Results

#### Trained Data Classification and Skeleton Reprojection:
- Classification Results
- CSV with skeleton data
- Images containing reprojected skeleton (optional)