#include "MatSerializer.hpp"

#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

using namespace std;
using namespace cv;

int main()
{
    Mat image_out = imread("c1_g1_p9-006433.png", CV_LOAD_IMAGE_COLOR);
    FrameMat frame_out;
    frame_out.mat = image_out;

    ofstream ofs("matTest.ser");
    boost::archive::binary_oarchive oa(ofs, ios_base::binary);
    oa << frame_out;

    ifstream ifs("matTest.ser");
    boost::archive::binary_iarchive ia(ifs, ios_base::binary);

    FrameMat frame_in;
    ia >> frame_in;
    try {
        ia >> frame_in;
    } catch (boost::archive::archive_exception e) {
        printf("EOF!\n");
    }

    Mat image_in = frame_in.mat;

    imshow("Out", image_out);
    imshow("In", image_in);
    waitKey(0);
}
