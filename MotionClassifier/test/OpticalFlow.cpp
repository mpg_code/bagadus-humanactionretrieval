#include "OpticalFlow.hpp"
#include "BoundingBoxExtractor.hpp"
#include "FrameSequenceElement.hpp"

#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>
#include <stdio.h>

using namespace cv;
using namespace std;

#define POINT_OK 1

int main(void)
{
    // Simple frame reading and cropping
    Mat image_0 = imread("green.png", CV_LOAD_IMAGE_COLOR);
    Mat image_1 = imread("c1_g1_p9-006433.png", CV_LOAD_IMAGE_COLOR);
    Mat image_2 = imread("c1_g1_p9-006466.png", CV_LOAD_IMAGE_COLOR);

    BoundingBoxExtractor bbe(35, image_0.cols, image_0.rows, image_0);

    struct PlayerPosition position_1;
    position_1.x = 80;
    position_1.y = 85;

    struct PlayerPosition position_2;
    position_2.x = 30;
    position_2.y = 108;

    FrameSequenceElement elm_A = bbe.computeFrameSequenceElement(position_1, image_1);
    FrameSequenceElement elm_B = bbe.computeFrameSequenceElement(position_2, image_2);


    Mat img_A = elm_A.normalized;
    Mat img_B = elm_B.normalized;
    Rect roi_A = elm_A.roi;
    Rect roi_B = elm_B.roi;

    // Sparse method implementation
    vector<Point2f> sparseFeatures;
    vector<uchar> status;
    vector<float> error;
    Mat frame_1 = imread("c1_g1_p9-006433.png", CV_LOAD_IMAGE_GRAYSCALE);
    Mat frame_2 = imread("c1_g1_p9-006466.png", CV_LOAD_IMAGE_GRAYSCALE);
    Mat frame_3 = imread("c1_g1_p9-006466.png", CV_LOAD_IMAGE_COLOR);
    Mat frame_4(frame_1.size(), 1);
    frame_4 = Scalar(25, 25, 25);

    OpticalFlow flow;
    flow.computeSparse(img_A, img_B, sparseFeatures, status, error);

    vector<Point2f> prevSparseFeatures = flow.getPrevSparseFeatures();

    printf("Feature size set: %ld\n", sparseFeatures.size());
    for (int i = 0; i < sparseFeatures.size(); i++) {
        printf("Index: %2d  Status: %d  Value: (%6.2f, %6.2f)  Error: %6.2f\n",
               i, status[i], sparseFeatures[i].x, sparseFeatures[i].y, error[i]);
    }

    // This will only an approximation, as the camera is moving
    for(int i = 0; i < sparseFeatures.size(); i++) {
        Point p0(ceil(prevSparseFeatures[i].x + roi_A.x), ceil(prevSparseFeatures[i].y + roi_A.y));
        Point p1(ceil(sparseFeatures[i].x + roi_B.x), ceil(sparseFeatures[i].y + roi_B.y));
        line(frame_4, p0, p1,
             CV_RGB(status[i] == POINT_OK ? 255 : 0,
                    status[i] == POINT_OK ? 255 : 0,
                    status[i] == POINT_OK ? 255 : 0)
            );

        if (status[i] == POINT_OK) {
            double size = error[i]/15.0;
            if (size == 0)
                size = 1;
            else if (size < 0)
                size *= -1;

            if (size > 0 && size < 1)
                size = 1;

            circle(frame_3, p0, 1, CV_RGB(0, 255, 0));
            circle(frame_3, p1, (int)size, CV_RGB(255, 0, 0));
            line(frame_3, p0, p1, CV_RGB(0, 0, 255));
        }
    }

    imshow("Sparse Frame 1", frame_1);
    imshow("Sparse Frame 2", frame_2);
    imshow("Sparse Selection A: ", img_A);
    imshow("Sparse Selection B: ", img_B);
    imshow("Sparse Optical Vectors", frame_4);
    imshow("Sparse Visual Flow", frame_3);

    // Dense method implementation
    Mat flowField(img_A.size(), CV_32FC2); // This image cannot be drawn directly!

    Mat visualField(img_A.size(), CV_8UC3);
    flow.computeDense(img_A, img_B, flowField);
    visualField = flow.drawOptFlowMap(flowField);


    imshow("Dense Visual Flow", visualField);

    waitKey(0);

    return 0;
}
