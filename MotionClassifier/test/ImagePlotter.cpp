#include "SQLDataReader.hpp"

#include <cstdlib>
#include <iostream>

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/**
 * This is a simple program to see/verify that joints and bounding box
 * taken from the SQL actually hits the correct trained spots on a frame.
 */
int main(void)
{
    SQLDataReader sql((char*)"image_plotting_data.csv");
    SQLEntity *data = sql.next();

    Point2f BBStart = data->get(SQLEntity::BBStart);
    Point2f BBEnd = data->get(SQLEntity::BBEnd);

    Mat frame = imread(data->get(SQLEntity::FrameID), CV_LOAD_IMAGE_COLOR);
    Rect roi(BBStart.x, BBStart.y, BBEnd.x - BBStart.x, BBEnd.y - BBStart.y);
    Mat image = frame(roi);

    rectangle(image, BBStart, BBEnd, Scalar(0, 0, 255));
    for (int i = 4; i < 17; i++)
        circle(image, Point((*data)[i].x, (*data)[i].y), 0, Scalar(255, 0, 0));

    namedWindow("Display window", WINDOW_AUTOSIZE);
    imshow("Display window", image);
    waitKey(0);
    
    return EXIT_SUCCESS;
}
