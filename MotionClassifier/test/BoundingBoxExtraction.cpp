#include "BoundingBoxExtractor.hpp"
#include "SimpleCSVReader.hpp"

#include <opencv2/opencv.hpp>

#include <stdio.h>
#include <string>

#include "FrameSequenceElement.hpp"

using namespace cv;

int main(void)
{
    Mat frame_0 = imread("green.png", CV_LOAD_IMAGE_COLOR);
    Mat frame_1 = imread("c1_g1_p9-006433.png", CV_LOAD_IMAGE_COLOR);
    Mat frame_2 = imread("c1_g1_p9-006466.png", CV_LOAD_IMAGE_COLOR);


    BoundingBoxExtractor bbe(35, frame_0.cols, frame_0.rows, frame_0);

    struct PlayerPosition position_1;
    position_1.x = 80;
    position_1.y = 85;

    struct PlayerPosition position_2;
    position_2.x = 30;
    position_2.y = 108;

    FrameSequenceElement elm_A = bbe.computeFrameSequenceElement(position_1, frame_1);
    Mat img_A = elm_A.frame;
    Mat mask_A = elm_A.mask;
    Rect roi_A = elm_A.roi;

    FrameSequenceElement elm_B = bbe.computeFrameSequenceElement(position_2, frame_2);
    Mat img_B = elm_B.frame;
    Mat mask_B = elm_B.mask;
    Rect roi_B = elm_B.roi;


    imshow("Background", frame_0);
    imshow("Frame 1", frame_1);
    imshow("Frame 2", frame_2);
    imshow("Mask A", mask_A);
    imshow("Mask B", mask_B);
    imshow("Image A", img_A(roi_A));
    imshow("Image B", img_B(roi_B));

    waitKey(0);

    return 0;
}
