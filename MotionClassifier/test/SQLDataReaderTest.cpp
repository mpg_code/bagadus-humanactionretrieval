#include "SQLDataReader.hpp"

#include <cassert>
#include <iostream>


/**
 * Simple TDD for the SQL classes using assertions.
 */
int main(void)
{
    SQLDataReader reader((char*)"training_data.csv");
    SQLEntity *ent = reader.next();
    assert(ent != NULL);

    assert(ent->get(SQLEntity::SessionID) == 2066203433);

    assert(ent->get(SQLEntity::Timecode).empty());

    assert(ent->get(SQLEntity::FrameID)
           .compare("image_export/c1_g1_p9-016250.png") == 0);

    assert(ent->get(SQLEntity::FramePos).x == -1.0f
           && ent->get(SQLEntity::FramePos).y == -1.0f);

    assert(ent->get(SQLEntity::Size).x == 200.0f
           && ent->get(SQLEntity::Size).y == 200.0f);

    assert(ent->get(SQLEntity::Players) == 1);

    assert(ent->get(SQLEntity::Control) == 0);

    assert(ent->get(SQLEntity::Processed) == 1);

    assert(ent->get(SQLEntity::Motion).compare("run") == 0);

    assert(ent->get(SQLEntity::BBStart).x == 10.0f
           && ent->get(SQLEntity::BBStart).y == 20.0f);

    assert(ent->get(SQLEntity::KneeLeft).x == 3.52f
           && ent->get(SQLEntity::KneeLeft).y == 13.12f);

    cout << "All good!" << endl;
}
