# Utility

Utility programs/projects are design to filter, view and perform quality control on data obtained from _www_ or to export data to programs outside this repo's programs.

## ResultChecker
The _ResultChecker_ program is an utility program that have several functions for validating and visualizing data. Among them there are two different ways of asserting and filtering data annotated with the _www_ and merging them into a single CSV file (as each image will have several annotations that must be fused to a single point). Additionally, it can be used to examine the intermediate CSV skeleton files or individual worker's click-point performance by rendering skeletons into images.

The _ResultChecker_ program is quite versatile, being able to take different types of input of skeleton data and either view, merge and/or perform quality assesment over the input data. All the data files (CSV inputs and outputs) uses the database format.

### Input
#### Option 1: Gold Standard Tests quality control and merging
1. Option type argument
2. Gold Standard Data
3. Annotated data
4. Location of images that are annotated and contains gold standard images
5. Display the skeletons (optional)

#### Option 2: Majority Vote Filtering and merging
1. Option type argument
2. Annotated data, original un-merged from crowdworkers
3. Location of annotated images
4. Filename for merged skeleton data
5. Display the joint location filtering process (optional)

#### Option 3: Compare two skeleton plottings
1. Option type argument
2. Annotated data, original un-merged from crowdworkers
3. Location of annotated images
4. Filename for merged skeleton data
5. Display the joint location filtering process (optional)

#### Option 4: Compute overlap between two sequences' bounding boxes
1. Option type argument
2. Annotated data by expert
3. Bounding boxes produces by tracker results, obtained from _MotionClassifier_
4: Filename for overlap results

#### Option 5: Compute difference between two plots
1. Option type argument
2. Annotated data by expert
3. Skeleton reprojections produces by classification results, obtained from _MotionClassifier_
4: Filename for difference results

#### Option 6: View a skeleton
1. Option type argument
2. Annotated skeleton
3. Directory containing images for annotated data
4. Specify the images are of full frame and not trakced data (optional)

### Output
#### Option 1: Gold Standard Tests quality control and merging
- List of worker scored in number of test fails (lower is better)

#### Option 2: Majority Vote Filtering and Merging
- List of approved workers scored in percentage accuracy (higher is better)

#### Option 3: Compare two skeleton plottings
- None

#### Option 4: Compute overlap between two sequences' bounding boxes
- File containing percentage of overlaps

#### Option 5: Compute difference between two plots
- File containing the distance between joint locations

#### Option 6: View a skeleton
- None

## CoordinateTranslator
The _CoordinateTranslator_ can be used on the exported online training data to obtain absolute pixel positions for use with external programs (or with any of the intermediate results produces by other programs). It is required to translate the trained joint locations (or any of the positions between programs) to absolute pixel coordinates as they are given as floating point numbers relative to the tracker region of interest (ROI) and worker-selected bounding box. The only cases where translation is not needed is with the final skeleton output, which already comes as absolute values.

## Input
1. Input set of relative position skeleton data
2. Output filename of absolute pixel positions

## Output
- Absolute skeleton position data