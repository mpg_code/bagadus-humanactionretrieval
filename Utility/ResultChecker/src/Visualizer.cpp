#include "Visualizer.hpp"

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

// The '?' is to mark array indexes lower than DoF_start
static string jointName[] = {"?", "?", "?", "?", "H", "SL", "SR", "EL", "ER", "NL", "NR", "LL", "LR", "KL", "KR", "FL", "FR"};

static void drawCircle(Point2f position, Point2f offset, Mat &frame, Scalar color, float scale, int radiusStart, int radiusEnd)
{
    // If obscured, do not draw
    if (position.x == -1 && position.y == -1)
        return;

    for (int r = radiusStart; r < radiusEnd; r++)
        circle(frame, (position + offset) * scale, r, color);
}

static void drawPoint(Point2f position, Point2f offset, Mat &frame, Scalar color, float scale, int radius)
{
    drawCircle(position, offset, frame, color, scale, 0, radius);
}

static void drawLine(Point2f a, Point2f b, Point2f offset, Mat &frame, Scalar color, float scale, float thickness)
{
    if ((a.x == -1 && a.y == -1) || (b.x == -1 && b.y == -1))
        return;

    line(frame, (a + offset) * scale, (b + offset) * scale, color, thickness);
}

static float deviationRadius(Point2f deviation)
{
    return sqrtf((deviation.x * deviation.x) + (deviation.y * deviation.y));
}

Scalar deviationColor(Point2f joint, Point2f offset, Point2f &center, float radius)
{
    float diffX = (joint.x + offset.x) - center.x;
    float diffY = (joint.y + offset.y) - center.y;
    float deviation = deviationRadius(Point2f(diffX, diffY));
    if (deviation >= radius)
        return Scalar(0, 0, 128);

    return Scalar(255, 0, 0);
}

static void drawSkeleton(SQLEntity &skeleton, Mat &frame, Scalar colorLine, Scalar colorPoint, float scale, float thickness, int radius)
{
    drawLine(skeleton.get(SQLEntity::ShoulderLeft),
             skeleton.get(SQLEntity::ElbowLeft),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    drawLine(skeleton.get(SQLEntity::HandLeft),
             skeleton.get(SQLEntity::ElbowLeft),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    drawLine(skeleton.get(SQLEntity::LegLeft),
             skeleton.get(SQLEntity::KneeLeft),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    drawLine(skeleton.get(SQLEntity::FootLeft),
             skeleton.get(SQLEntity::KneeLeft),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);


    drawLine(skeleton.get(SQLEntity::ShoulderRight),
             skeleton.get(SQLEntity::ElbowRight),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    drawLine(skeleton.get(SQLEntity::HandRight),
             skeleton.get(SQLEntity::ElbowRight),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    drawLine(skeleton.get(SQLEntity::LegRight),
             skeleton.get(SQLEntity::KneeRight),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    drawLine(skeleton.get(SQLEntity::FootRight),
             skeleton.get(SQLEntity::KneeRight),
             skeleton.get(SQLEntity::BBStart),
             frame, colorLine, scale, thickness);

    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        drawPoint(skeleton[i], skeleton.get(SQLEntity::BBStart), frame, colorPoint, scale, radius);
}

static void drawTextLabel(Point2f position, string label, Point2f offset, Mat &image)
{
    if (position.x == -1 && position.y == -1)
        return;

    putText(image, label, (position + offset) * 5, FONT_HERSHEY_PLAIN, 1, Scalar(0, 100, 200), 1.5);
}

static void drawLabels(SQLEntity &ent, Mat &image)
{
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        drawTextLabel(ent[i], jointName[i], ent.get(SQLEntity::BBStart), image);
}

void renderImage(SQLEntity &entity, Mat &image, Statistics &stats)
{
    namedWindow("Image Render", 0);
    namedWindow("Skeleton Render", 0);

    Mat overlayed;
    resize(image, overlayed, image.size() * 5, 0, 0);
    drawSkeleton(entity, overlayed, Scalar(0, 0, 255), Scalar(255, 0, 0), 5, 2, 5);
    imshow("Image Render", overlayed);

    Mat labeled(image.size() * 5, CV_8UC3, Scalar(0, 0, 0));
    drawSkeleton(entity, labeled, Scalar(200, 0, 200), Scalar(200, 200, 0), 5, 1, 2);
    drawLabels(entity, labeled);

    imshow("Skeleton Render", labeled);

    fprintf(stderr,
            "Stats:\n\tImage = %s\n\tCluser = %6.2f\n\tLine = %6.2f\n\tMaskError = %6.2f\nVerdict = %s\n",
            entity.get(SQLEntity::FrameID).c_str(),
            stats.cluster, stats.line, stats.maskError,
            stats.acceptedImage() ? "Accepted" : "Rejected");

    waitKey(0);
}

void renderImage(SQLEntity &control, SQLEntity &trained, Mat &image, Statistics &stats)
{
    namedWindow("Rendered Comparison", 0);
    namedWindow("Labeled Image", 0);
    namedWindow("Control Image", 0);

    Mat overlayed;
    resize(image, overlayed, image.size() * 5, 0, 0);
    drawSkeleton(control, overlayed, Scalar(0, 0, 255), Scalar(255, 0, 0), 5, 2, 5);
    drawSkeleton(trained, overlayed, Scalar(200, 0, 200), Scalar(200, 200, 0), 5, 2, 5);
    imshow("Rendered Comparison", overlayed);

    Mat labeled(image.size()*5, CV_8UC3, Scalar(0, 0, 0));
    drawSkeleton(trained, labeled, Scalar(200, 0, 200), Scalar(200, 200, 0), 5, 1, 2);
    drawLabels(trained, labeled);

    imshow("Labeled Image", labeled);

    Mat solution(image.size()*5, CV_8UC3, Scalar(0, 0, 0));
    drawSkeleton(control, solution, Scalar(0, 0, 255), Scalar(255, 0, 0), 5, 1, 2);
    drawLabels(control, solution);


    imshow("Control Image", solution);

    fprintf(stderr, "\n\n\n=== RENDERING ===\nControl = %s\ntrained = %s\n",
            control.get(SQLEntity::FrameID).c_str(), trained.get(SQLEntity::FrameID).c_str());

    fprintf(stderr,
            "Stats:\n\tError = %f\n\tOverPlot = %d\n\tUnderPlot = %d\n\tCluser = %6.2f\n\tLine = %6.2f\n\tMaskError = %6.2f\nVerdict = %s\n",
            stats.error, stats.overPlot, stats.underPlot, stats.cluster, stats.line, stats.maskError,
            stats.acceptedImage() ? "Accepted" : "Rejected");

    waitKey(0);
}

void renderImage(Mat &image, int joint, Point2f &prevCenter, Point2f &prevPrevCenter, Point2f &center, Point2f &deviation, vector<SQLEntity> &frames)
{
    namedWindow("Worker Joint", 0);

    string visibility = center.x != -1 && center.y != -1 ? "Visible" : "Obscured";
    float devRadius = deviationRadius(deviation);

    Mat labeled;
    resize(image, labeled, image.size() * 5, 0, 0);
    rectangle(labeled, Point(0, 0), Point(380, 25), Scalar(255, 255, 255), CV_FILLED);
    putText(labeled, jointName[joint] + "  " + visibility, Point(5, 20), FONT_HERSHEY_PLAIN, 1.5, Scalar(0, 100, 200), 2);

    // Center
    drawPoint(center, Point(0, 0), labeled, Scalar(0, 128, 0), 5, 5);
    drawCircle(center, Point(0, 0), labeled, Scalar(0, 0, 255), 5, devRadius * 5, (devRadius * 5) + 2);

    // History
    drawPoint(prevPrevCenter, Point(0, 0), labeled, Scalar(128, 128, 128), 5, 3);
    drawPoint(prevCenter, Point(0, 0), labeled, Scalar(0, 100, 255), 5, 3);

    // Points
    for (SQLEntity ent : frames) {
        Scalar color = deviationColor(ent[joint], ent.get(SQLEntity::BBStart), center, devRadius);
        drawPoint(ent[joint], ent.get(SQLEntity::BBStart), labeled, color, 5, 3);
    }

    imshow("Worker Joint", labeled);
    waitKey(0);
}
