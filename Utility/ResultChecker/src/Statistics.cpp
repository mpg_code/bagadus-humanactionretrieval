#include "Statistics.hpp"

#include <opencv2/opencv.hpp>
#include <algorithm>

Statistics::Statistics() :
    error(0),
    overPlot(0),
    underPlot(0),
    line(0),
    cluster(0),
    maskError(0)
{

}

Statistics::Statistics(SQLEntity &entity, string &imageDir) :
    error(0),
    overPlot(0),
    underPlot(0)
{
    cluster = clusterDiameter(entity);
    line = lineDistance(entity);
    maskError = 0;//maskDistance(entity, imageDir);
    imageDir = imageDir;// To remove compiler warning
}

bool Statistics::acceptedImage()
{
    return error <= 24
        && underPlot <= 3
        && overPlot <= 2
        && line > 4.0f
        && cluster > 4.0f
        && maskError < 20.0f;
}

Statistics::Statistics(SQLEntity &control, SQLEntity& trained, string &imageDir)
{
    error = distanceErrors(control, trained);
    overPlot = isOverPlot(control, trained);
    underPlot = isUnderPlot(control, trained);
    cluster = clusterDiameter(trained);
    line = lineDistance(trained);
    maskError = 0;//maskDistance(trained, imageDir);
    imageDir = imageDir;// To remove compiler warning
}

double Statistics::jointDistance(Point2f control, Point2f trained)
{
    double diffX = control.x - trained.x;
    double diffY = control.y - trained.y;
    return sqrt((diffX * diffX) + (diffY * diffY));
}

double Statistics::distanceErrors(SQLEntity &control, SQLEntity &trained)
{
    double error = 0;
    int joints = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        if (control[i].x != -1 && control[i].y != -1 && trained[i].x != -1 && trained[i].y != -1) {
            error += jointDistance(control[i] + control.get(SQLEntity::BBStart), trained[i] + trained.get(SQLEntity::BBStart)) * 5;
            joints++;
        }
    }

    return joints == 0 ? 0 : error / joints;
}

int Statistics::isOverPlot(SQLEntity &control, SQLEntity &trained)
{
    int errors = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        if (control[i].x == -1 && control[i].y == -1 && trained[i].x != -1 && trained[i].y != -1)
            errors++;

    return errors;
}

int Statistics::isUnderPlot(SQLEntity &control, SQLEntity &trained)
{
    int errors = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        if (control[i].x != -1 && control[i].y != -1 && trained[i].x == -1 && trained[i].y == -1)
            errors++;

    return errors;
}

float Statistics::clusterDiameter(SQLEntity &ent)
{
    double max = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        for (int j = SQLEntity::DoF_start; j < SQLEntity::DoF_end; j++) {
            double distance = jointDistance(ent[i], ent[j]);
            if (distance > max)
                max = distance;
        }
    }

    return max; // approx. 3 pixels in any direction
}

double Statistics::distanceFromLine(Point2f start, Point2f end, Point2f point)
{
    double enumerator = abs(((end.y - start.y) * point.x) - ((end.x - start.x) * point.y) + (end.x * start.y) - (end.y * start.x));
    double denominator = sqrt(((end.y * end.y) - (2 * end.y * start.y) + (start.y * start.y)) + ((end.x * end.x) - (2 * end.x * start.x) + (start.x * start.x)));
    return enumerator / denominator;
}

float Statistics::lineDistance(SQLEntity &ent)
{
    // Find most opposing points
    Point2f a(0, 0);
    Point2f b(500, 500);

    double max = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        for (int j = SQLEntity::DoF_start; j < SQLEntity::DoF_end; j++) {
            if ((ent[i].x == -1 && ent[i].y == -1) || (ent[j].x == -1 && ent[j].y == -1))
                continue;

            double distance = jointDistance(ent[i], ent[j]);
            if (distance > max) {
                max = distance;
                a = ent[i];
                b = ent[j];
            }
        }
    }

    // Compute line distance
    max = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        if (ent[i].x == -1 && ent[i].y == -1)
            continue;

        double dist = distanceFromLine(a, b, ent[i]);
        if (dist > max)
            max = dist;
    }

    return max;
}

float Statistics::maskDistance(SQLEntity &ent, string &imageDir)
{
    cv::Mat mask = imread(imageDir + ent.get(SQLEntity::FrameID) + "_mask", CV_LOAD_IMAGE_GRAYSCALE);

    double maxDist = 0;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        if (ent[i].x == -1 && ent[i].y == -1)
            continue;

        double minDist = mask.cols * mask.rows;
        for (int y = 0; y < mask.rows; y++) {
            for (int x = 0; x < mask.cols; x++) {
                if (mask.at<char>(y, x) != 0) {
                    double distance = jointDistance(Point2f(x, y), ent[i]);
                    if (distance < minDist)
                        minDist = distance;
                }
            }
        }

        if (minDist > maxDist)
            maxDist = minDist;
    }

    return maxDist;
}
