#include "WorkerSetValidator.hpp"

#include <float.h>

using namespace std;

WorkerSetValidator::WorkerSetValidator(SQLDataReader &trained, std::string &imageDir, string &csvExportFilename, bool display, ofstream &goldLogOut) :
    data(trained),
    imageDir(imageDir),
    display(display),
    goldLogOut(goldLogOut),
    prevEnt(NULL),
    nextEnt(NULL),
    totalWorkers(0),
    acceptedWorkers(0)
{
    printf("Created output CSV file '%s'\n", csvExportFilename.c_str());
    csvData.open(csvExportFilename);

    csvData << "sessionID,timecode,frameID,frameX,frameY,width,height,players,control,processed,motion,sequenceNo,"
        "bb_x1,bb_y1,bb_x2,bb_y2,h_x,h_y,s_lx,s_ly,s_rx,s_ry,e_lx,e_ly,e_rx,e_ry,n_lx,n_ly,n_rx,n_ry,"
        "l_lx,l_ly,l_rx,l_ry,k_lx,k_ly,k_rx,k_ry,f_lx,f_ly,f_rx,f_ry"
            << endl;
}

void WorkerSetValidator::process()
{
    int totalFrames = 0;
    vector<SQLEntity> frames;

    while (fillFrames(frames) > 0) {
        string frameID = frames[0].get(SQLEntity::FrameID);
        vector<SQLEntity> cleanFrames = rejectBadFramesMedian(frames);

        bool useGoldLogging = true;
        if (cleanFrames.size() == 0) {
            cleanFrames = frames;
            useGoldLogging = false;
        }

        totalFrames += cleanFrames.size();

        printf("'%s' training times = %2d\n", frameID.c_str(), (int)cleanFrames.size());

        if (cleanFrames.size() != 0)
            processSequence(cleanFrames, useGoldLogging);

        frames.clear();
    }

    csvData.close();

    exportGoldErrors();

    printf("\nAccepted workload = %5.2f%%\nA total of %d frames where trained.\n",
           (acceptedWorkers * 100.0f) / totalWorkers,
           totalFrames);
}

void WorkerSetValidator::rejectBadFrames(vector<SQLEntity> &frames) {
    auto iter = frames.begin();
    while (iter != frames.end()) {
        Statistics stat(*iter, imageDir);
        bool accepted = stat.acceptedImage();


        if (!accepted) {
            iter = frames.erase(iter);
            for (int i = SQLEntity::BBStart; i < SQLEntity::DoF_end; i++)
                logGoldError(*iter, false);
        } else {
            ++iter;
        }
    }
}

vector<SQLEntity> WorkerSetValidator::rejectBadFramesMedian(vector<SQLEntity> &frames)
{
    float minMedian = FLT_MAX;
    Statistics calculator;
    vector<float> distances;
    vector<SQLEntity> filteredPoints;

    for (int joint = SQLEntity::BBStart; joint < SQLEntity::DoF_end; joint++) {
        for (SQLEntity ent : frames) {
            distances.clear();

            for (SQLEntity other : frames)
                if (ent.get(SQLEntity::SessionID) != other.get(SQLEntity::SessionID)
                    && ent[joint].x != -1 && ent[joint].y != -1
                    && other[joint].x != -1 && other[joint].y != -1)
                    distances.push_back(calculator.jointDistance(ent[joint], other[joint]));

            float m = median(distances);
            if (m < minMedian)
                minMedian = m;
        }
    }

    int i = 0;
    for (SQLEntity ent : frames) {
        if (distances[i] < medianFilterThreshold * minMedian)
            filteredPoints.push_back(ent);
        else
            logGoldError(ent, false);

        i++;
    }

    return filteredPoints;
}

float WorkerSetValidator::median(std::vector<float> &points)
{
    if (points.size() <= 1)
        return FLT_MAX;

    sort(points.begin(), points.end());

    if (points.size() % 2 != 0 )
        return points[((points.size() + 1) / 2)];
    else
        return points[(points.size() / 2)];
}

void WorkerSetValidator::addRejected(int sessionID, GoldLogEntry &entry)
{
    goldLogOut << "UPDATE crowdworker SET goldErrors = "
               << ((int)((100.0f * entry.errors) / entry.totalJoints))
               << " WHERE sessionID = "
               << sessionID
               << ";"
               << endl;
}

void WorkerSetValidator::writeEntity(SQLEntity &ent)
{
    csvData << ent.get(SQLEntity::SessionID) << ","
            << "\"" << ent.get(SQLEntity::Timecode) << "\","
            << "\"" << ent.get(SQLEntity::FrameID) << "\","
            << ent.get(SQLEntity::FramePos).x - (ent.get(SQLEntity::Size).x / 2) << ","
            << ent.get(SQLEntity::FramePos).y - (ent.get(SQLEntity::Size).y / 2) << ","
            << ent.get(SQLEntity::Size).x << "," << ent.get(SQLEntity::Size).y << ","
            << ent.get(SQLEntity::Players) << ","
            << ent.get(SQLEntity::Control) << ","
            << ent.get(SQLEntity::Processed) << ","
            << "\"" << ent.get(SQLEntity::Motion) << "\","
            << ent.get(SQLEntity::SequenceNo) << ",";

    for (int i = SQLEntity::BBStart; i < SQLEntity::DoF_end; i++)
        csvData << ent[i].x << "," << ent[i].y << ",";

    csvData << endl;
}


void WorkerSetValidator::exportGoldErrors()
{
    int index = 0;
    for (pair<int, GoldLogEntry> entry : goldMap) {
        printf("[%3d] = %d: Trained = %5d, Errors = %5d (Deviation = %6.2f%%)  | <36%% %c | = [%3d]\n",
               index,
               entry.first, entry.second.totalJoints, entry.second.errors,
               (100.0f * entry.second.errors) / entry.second.totalJoints,
               (100.0f * entry.second.errors) / entry.second.totalJoints <= acceptanceThreshold ? 'Y' : ' ',
               index);

        if ((100.0f * entry.second.errors) / entry.second.totalJoints <= acceptanceThreshold) {
            acceptedWorkers++;
        }
        totalWorkers++;
        index++;
        addRejected(entry.first, entry.second);
    }
}

void WorkerSetValidator::logGoldError(SQLEntity &ent, bool accepted)
{
    if (ent.get(SQLEntity::SessionID) == 0)
        return;

    unordered_map<int, GoldLogEntry>::iterator search = goldMap.find(ent.get(SQLEntity::SessionID));
    if (search != goldMap.end()) {
        GoldLogEntry &log = search->second;
        log.totalJoints++;
        log.errors += !accepted;
    }
    else {
        GoldLogEntry log{1, accepted ? 0 : 1};
        goldMap.insert(pair<int, GoldLogEntry>(ent.get(SQLEntity::SessionID), log));
    }

}

vector<SQLEntity> WorkerSetValidator::createAcceptedVector(vector<SQLEntity> &frames, int joint,
                                                           Point2f &center, Point2f &deviation, bool logGold)
{
    vector<SQLEntity> vec;

    float tolerance = sqrtf(deviation.x * deviation.x + deviation.y * deviation.y);

    for (SQLEntity ent : frames) {
        bool accepted = true;
        if (ent[joint].x != -1 && ent[joint].y != -1) {
            float diffX = (ent[joint].x + ent[SQLEntity::BBStart].x) - center.x;
            float diffY = (ent[joint].y + ent[SQLEntity::BBStart].y) - center.y;
            float delta = sqrtf(diffX * diffX + diffY * diffY);

            if (delta <= tolerance)
                vec.push_back(ent);
            else
                accepted = false;
        }

        if (logGold)
            logGoldError(ent, accepted);
    }

    return vec;
}

void WorkerSetValidator::processSequence(vector<SQLEntity> &frames, bool useGoldLogging)
{
    // Entity used for mergin the result and export
    frames.push_back(previousPlot);
    SQLEntity exportEntity;
    exportEntity.copyFrom(frames[0]);

    for (int joint = SQLEntity::DoF_start; joint < SQLEntity::DoF_end; joint++) {
        Point2f center = findCenter(joint, frames);;
        Point2f prevCenter(0, 0);
        Point2f prevPrevCenter(0, 0);
        Point2f deviation;

        if (center.x != -1 && center.y != -1) {
            deviation = averageDeviation(joint, frames, center);
            vector<SQLEntity> accepted = createAcceptedVector(frames, joint, center, deviation, true && useGoldLogging);

            if (display) {
                Mat image = imread(imageDir + frames[0].get(SQLEntity::FrameID), CV_LOAD_IMAGE_COLOR);
                renderImage(image, joint, prevCenter, prevPrevCenter, center, deviation, frames);
            }

            Statistics calculator;
            while (calculator.jointDistance(center, prevCenter) > 2.8) {
                prevPrevCenter = prevCenter;
                prevCenter = center;

                center = findCenter(joint, accepted);
                deviation = averageDeviation(joint, accepted, center);
                accepted = createAcceptedVector(frames, joint, center, deviation);


                if (display) {
                    Mat image = imread(imageDir + frames[0].get(SQLEntity::FrameID), CV_LOAD_IMAGE_COLOR);
                    renderImage(image, joint, prevCenter, prevPrevCenter, center, deviation, frames);
                }

                // Too many iterations, lost track!
                // Only attempt while still having ambigius points
                if (center.x == -1 && center.y == -1) {
                    center = prevCenter;
                    break;
                }
            }

            exportEntity[joint] = center;
        }
    }

    previousPlot = exportEntity;
    writeEntity(exportEntity);
}

Point2f WorkerSetValidator::findCenter(int joint, vector<SQLEntity> &frames)
{
    Point2f center(0, 0);
    int plots = 0;
    int obscured = 0;

    for (SQLEntity ent : frames) {
        if (ent[joint].x == -1 && ent[joint].y == -1) {
            obscured++;
        } else {
            plots++;
            center.x += ent[joint].x + ent[SQLEntity::BBStart].x;
            center.y += ent[joint].y + ent[SQLEntity::BBStart].y;
        }
    }

    if (plots > 0 && plots >= obscured) {
        center.x /= plots;
        center.y /= plots;
        return center;
    }

    return Point2f(-1, -1);
}

Point2f WorkerSetValidator::averageDeviation(int joint, vector<SQLEntity> &frames, Point2f &center)
{
    Point2f deviation(0, 0);
    int plots = 0;

    for (SQLEntity ent : frames) {
        if (ent[joint].x != -1 && ent[joint].y != -1) {
            plots++;
            deviation.x += fabsf((ent[joint].x + ent[SQLEntity::BBStart].x) - center.x);
            deviation.y += fabsf((ent[joint].y + ent[SQLEntity::BBStart].y) - center.y);
        }
    }

    deviation.x /= plots;
    deviation.y /= plots;

    return deviation;
}

int WorkerSetValidator::fillFrames(vector<SQLEntity> &frames)
{
    if (nextEnt == NULL) {
        prevEnt = data.next();
        nextEnt = data.next();

        if (prevEnt != NULL)
            frames.push_back(*prevEnt);

    } else {
        frames.push_back(*nextEnt);

        delete prevEnt;
        prevEnt = nextEnt;
        nextEnt = data.next();
    }

    while (nextEnt != NULL && prevEnt != NULL && nextEnt->get(SQLEntity::FrameID).compare(prevEnt->get(SQLEntity::FrameID)) == 0) {
        frames.push_back(*nextEnt);

        delete prevEnt;
        prevEnt = nextEnt;
        nextEnt = data.next();
    }

    return frames.size();
}
