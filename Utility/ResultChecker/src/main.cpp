#include "SQLDataReader.hpp"
#include "SQLEntity.hpp"
#include "Statistics.hpp"
#include "Visualizer.hpp"
#include "SigSevHandler.hpp"
#include "WorkerSetValidator.hpp"
#include "CSVComparer.hpp"

#include <stdio.h>
#include <string>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

static bool display;
static ofstream out;

void addRejected(SQLEntity &ent)
{
    out << "UPDATE crowdworker SET goldErrors = goldErrors + 1 WHERE sessionID = "
        << ent.get(SQLEntity::SessionID)
        << ";" << endl;
}


int frameIndex(SQLEntity &entity)
{
    string identifier = entity.get(SQLEntity::FrameID);
    identifier = identifier.erase(0, identifier.length() - 10); // Nothing but the last 10 chars are of interest
    identifier = identifier.erase(identifier.length() - 4, identifier.length()); // Remove file extension
    return atoi(identifier.c_str());
}

void compareWorkers(SQLDataReader &trainedFile, string imageDir, string exportFile)
{
    WorkerSetValidator ws(trainedFile, imageDir, exportFile, display, out);
    ws.process();
}

void viewFile(SQLDataReader &file, string imageDir, bool fullFrame)
{
    SQLEntity *ent = file.next();
    while (ent != NULL) {
        Mat image = imread(imageDir + string(ent->get(SQLEntity::FrameID)), CV_LOAD_IMAGE_COLOR);
        Statistics stats(*ent, imageDir);

        if (fullFrame) {
            for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
                if ((*ent)[i].x != -1 && (*ent)[i].y != -1) {
                    (*ent)[i].x += ent->get(SQLEntity::FramePos).x - (ent->get(SQLEntity::Size).x / 2);
                    (*ent)[i].y += ent->get(SQLEntity::FramePos).y - (ent->get(SQLEntity::Size).y / 2);
                }
            }
        }

        renderImage(*ent, image, stats);

        delete ent;
        ent = file.next();
    }
}

void compareFiles(SQLDataReader &controlFile, SQLDataReader &trainedFile, string imageDir)
{
    SQLEntity *trained = trainedFile.next();
    SQLEntity *control = controlFile.next();

    Statistics totals;
    int trainedImages = 0;
    int accepted = 0;

    while (control != NULL && trained != NULL) {

        // Spool to start
        while (control != NULL && frameIndex(*control) < frameIndex(*trained)) {
            delete control;
            control = controlFile.next();
        }

        if (control == NULL)
            break;

        while (trained != NULL && frameIndex(*trained) < frameIndex(*control)) {
            delete trained;
            trained = trainedFile.next();
        }

        if (trained == NULL)
            break;

        // Look at all the trained with same id as control
        while (trained != NULL && frameIndex(*trained) == frameIndex(*control)) {
            Mat image = imread(imageDir + string(control->get(SQLEntity::FrameID)), CV_LOAD_IMAGE_COLOR);
            Statistics stats(*control, *trained, imageDir);
            bool ack = stats.acceptedImage();

            trainedImages++;
            totals += stats;
            if (ack)
                accepted++;
            else
                addRejected(*trained);

            if (display)
                renderImage(*control, *trained, image, stats);

            delete trained;
            trained = trainedFile.next();
        }

        if (trained == NULL)
            break;

        // Advance control to next trained
        while (control != NULL &&frameIndex(*control) < frameIndex(*trained)) {
            delete control;
            control = controlFile.next();
        }

        if (control == NULL)
            break;
    }

    printf("\n\n === Overall Statistics ===\n"
           "    Images = %d\n    Avg. Error = %f\n    OverPlot = %d (Avg. = %f)\n    UnderPlot = %d (Avg. = %f)\n"
           "    Clusters = %f\n    Lines = %f\n    MaskError = %f\n    Accepted = %d\n    Rejected = %d\n\n",
           trainedImages,
           totals.error / trainedImages,
           totals.overPlot,
           (double)totals.overPlot / trainedImages,
           totals.underPlot,
           (double)totals.underPlot / trainedImages,
           totals.cluster,
           totals.line,
           totals.maskError,
           accepted,
           trainedImages - accepted);
}

int main(int argc, char **argv)
{
    if (argc < 4) {
        printf("Usage (option 1): %s gold <control.csv> <trained.csv> <imgDir/> [display]\n"
               "      (option 2): %s worker <trained.csv> <imgDir/> <export.csv> [display]\n"
               "      (option 3): %s compare <trained.csv> <render.csv> <export.csv>\n"
               "      (option 4): %s overlap <trained.csv> <render.csv> <export.csv>\n"
               "      (option 5): %s verify <expert.csv> <crowd_filtered.csv> <export.csv>\n"
               "      (option 6): %s view <trained.csv> <imgDir/> [full (using full frame instead of bounding box)]\n"
               "Note: CSV files must be ordered by frameID for option 1, 2, 4 and 5, and by timecode for option 3.\n",
               argv[0], argv[0], argv[0], argv[0], argv[0], argv[0]);
        return EXIT_FAILURE;
    }

    if (!strncmp(argv[argc-1], "display", strlen("display")))
        display = true;
    else
        display = false;

    SigSevHandler::enableSignalHandler(argv[0]);
    if (!strncmp(argv[1], "view", strlen("view"))) {
        SQLDataReader frames(argv[2]);
        bool fullFrame = false;
        if (argc == 5)
            fullFrame = strncmp(argv[4], "full", strlen("full")) == 0;

        viewFile(frames, argv[3], fullFrame);

    } else if (!strncmp(argv[1], "compare", strlen("compare"))) {
        CSVComparer comparer;
        comparer.compare(argv[2], argv[3], argv[4], CSVComparer::SequenceRelative);
        comparer.printStatistics();

    } else if (!strncmp(argv[1], "overlap", strlen("overlap"))) {
        CSVComparer comparer;
        comparer.overlap(argv[2], argv[3], argv[4]);
        comparer.printStatistics();

    } else if (!strncmp(argv[1], "verify", strlen("verify"))) {
        CSVComparer comparer;
        comparer.compare(argv[2], argv[3], argv[4], CSVComparer::SequenceAbsolute);
        comparer.printStatistics();

    } else {
        printf("\nCreated output SQL file 'goldQualifications.sql'\n");
        out.open("goldQualifications.sql");
        out << "UPDATE crowdworker SET goldErrors = NULL;" << endl; // Default to NULL to all un-processed workers

        if (!strncmp(argv[1], "gold", strlen("gold"))) {
            // gold denotes absolute count
            SQLDataReader control(argv[2]);
            SQLDataReader trained(argv[3]);
            compareFiles(control, trained, string(argv[4]));
        } else {
            SQLDataReader trained(argv[2]);
             // gold denotes percentage
            compareWorkers(trained, string(argv[3]), string(argv[4]));
        }

        out.close();
    }

    return EXIT_SUCCESS;
}
