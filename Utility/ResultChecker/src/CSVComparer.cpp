#include "CSVComparer.hpp"
#include "SimpleCSVReader.hpp"

#include "Visualizer.hpp"
#include "Statistics.hpp"

#include <math.h>

using namespace std;
using namespace cv;

CSVComparer::CSVComparer()
{

}

void CSVComparer::compare(char *trainedCSV, char *sequenceCSV, char *exportCSV, CompareMethod method)
{
    frames = 0;
    boundingBoxCoverage = -1;

    ofstream out;
    out.open(exportCSV);
    out << "timecode,frameID,bb_overlap,h,s_l,s_r,e_l,e_r,n_l,n_r,l_l,l_r,k_l,k_r,f_l,f_r" << endl;

    SQLDataReader trained(trainedCSV);
    SQLDataReader sequence(sequenceCSV, method == SequenceAbsolute ? SQLEntity::Absolute : SQLEntity::Relative);

    SQLEntity *train = trained.next();
    SQLEntity *seq = sequence.next();

    // Synchronize the two files
    while (entityBehind(train, seq)) {
        delete train;
        train = trained.next();
    }

    while (entityBehind(seq, train)) {
        delete seq;
        seq = sequence.next();
    }

    // Compare files
    while (inSync(train, seq)) {
        frames++;

        SQLEntity stats = comparePlots(train, seq, method);

        exportFrameStatistics(out, stats, boundingBoxCoverage);

        delete train;
        delete seq;
        train = trained.next();
        seq = sequence.next();
    }

    out.close();
}

void CSVComparer::overlap(char *trainedCSV, char *sequenceCSV, char *exportCSV)
{
    frames = 0;
    boundingBoxCoverage = 0;

    ofstream out;
    out.open(exportCSV);
    out << "timecode,frameID,bb_overlap,h,s_l,s_r,e_l,e_r,n_l,n_r,l_l,l_r,k_l,k_r,f_l,f_r" << endl;

    SQLDataReader trained(trainedCSV, SQLEntity::Absolute);
    SQLDataReader sequence(sequenceCSV, SQLEntity::Absolute);

    SQLEntity *train = trained.next();
    SQLEntity *seq = sequence.next();

    // Synchronize the two files
    while (entityBehind(train, seq)) {
        delete train;
        train = trained.next();
    }

    while (entityBehind(seq, train)) {
        delete seq;
        seq = sequence.next();
    }

    // Compare files
    while (inSync(train, seq)) {
        frames++;

        float overlap = boxOverlap(train, seq);

        exportFrameStatistics(out, *seq, overlap);

        delete train;
        delete seq;
        train = trained.next();
        seq = sequence.next();
    }

    out.close();
}

void CSVComparer::exportFrameStatistics(ofstream &out, SQLEntity &stats, float overlap)
{
    out << "\"" << stats.get(SQLEntity::Timecode) << "\","
        << "\"" << stats.get(SQLEntity::FrameID) << "\","
        << overlap << ",";

    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        out << eucludianDistance(stats[i]) << ",";

    out << endl;
}

bool CSVComparer::inSync(SQLEntity *trained, SQLEntity *sequence)
{
    if (trained == NULL || sequence == NULL)
        return false;

    size_t t = SimpleCSVReader::stringToTime(trained->get(SQLEntity::Timecode));
    size_t c = SimpleCSVReader::stringToTime(sequence->get(SQLEntity::Timecode));
    return t - c == 0;
}

bool CSVComparer::entityBehind(SQLEntity *trained, SQLEntity *sequence)
{
    if (trained == NULL || sequence == NULL)
        return false;

    size_t t = SimpleCSVReader::stringToTime(trained->get(SQLEntity::Timecode));
    size_t c = SimpleCSVReader::stringToTime(sequence->get(SQLEntity::Timecode));
    return t < c;
}

SQLEntity CSVComparer::comparePlots(SQLEntity *trained, SQLEntity *sequence, CompareMethod method)
{
    SQLEntity stats;
    stats.copyFrom(*sequence);

    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        if (((*trained)[i].x != -1 && (*trained)[i].y != -1)
            && ((*sequence)[i].x != -1 && (*sequence)[i].y != -1))
        {
            float centerDiff = 0;
            if (method == SequenceAbsolute)
                centerDiff = trained->get(SQLEntity::Size).x / 2;

            float train_pos_x = absolutePosition((*trained)[i].x, trained->get(SQLEntity::FramePos).x - centerDiff, trained->get(SQLEntity::BBStart).x, method);
            float train_pos_y = absolutePosition((*trained)[i].y, trained->get(SQLEntity::FramePos).y - centerDiff, trained->get(SQLEntity::BBStart).y, method);
            float seq_pos_x = absolutePosition((*sequence)[i].x, sequence->get(SQLEntity::FramePos).x, sequence->get(SQLEntity::BBStart).x, method);
            float seq_pos_y = absolutePosition((*sequence)[i].y, sequence->get(SQLEntity::FramePos).y, sequence->get(SQLEntity::BBStart).y, method);

            stats[i].x = fabsf(train_pos_x - seq_pos_x);
            stats[i].y = fabsf(train_pos_y - seq_pos_y);

            globalStats[i].x += stats[i].x;
            globalStats[i].y += stats[i].y;
        } else {
            stats[i].x = 0.0f;
            stats[i].y = 0.0f;
        }
    }

    /*
     // DEBUG
    Statistics stats_blck;
    Mat img = imread(string("path/to/dir") + sequence->get(SQLEntity::FrameID), CV_LOAD_IMAGE_COLOR);
    if (method == SequenceRelative) {
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        if ((*trained)[i].x != -1) {
            if ((*trained).get(SQLEntity::FramePos).x > 0)
                (*trained)[i].x += (*trained).get(SQLEntity::FramePos).x;
        }
    }
    }
    renderImage(*trained, *sequence, img, stats_blck);
    */



    return stats;
}

float CSVComparer::absolutePosition(float pointCoordinate, float frameOffset, float boundingOffset, CompareMethod method)
{
    float pos = pointCoordinate;

    // Frame positions are made to match excact boundingBoxSize*2,
    // but at times that will result in negative values for the image position.
    // DataGenerator will produce negative-indexed images, but will instead hold it to the closest edge.
    // The final result is an offset that must be re-adjusted at render-time.
    if (frameOffset > 0)
        pos += frameOffset;

    if (boundingOffset > 0 && method == SequenceAbsolute)
        pos += boundingOffset;

    return pos;
}

float CSVComparer::eucludianDistance(Point2f point)
{
    return sqrtf((point.x * point.x) + (point.y * point.y));
}

float CSVComparer::eucludianDistanceAverage()
{
    float average = 0.0f;
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++)
        average += eucludianDistance(globalStats[i]);

    return average / (SQLEntity::DoF_end - SQLEntity::DoF_start);
}

float CSVComparer::boxOverlap(SQLEntity *trained, SQLEntity *sequence)
{
    // 1. Find coordinates of bounding boxes and intersection
    // Because everything is of absolute coordinates, remove FramePos as Size to make sizes the only factor
    // Every sequence image has the exact same size (bondingBoxSize*2)
    int bbSize =  sequence->get(SQLEntity::Size).x / 2;

    Point2f start_trained = trained->get(SQLEntity::BBStart) - trained->get(SQLEntity::FramePos);
    Point2f end_trained = trained->get(SQLEntity::BBEnd) - trained->get(SQLEntity::FramePos);

    Point2f start_sequence = sequence->get(SQLEntity::BBStart) - (sequence->get(SQLEntity::FramePos) - Point2f(bbSize, bbSize));
    Point2f end_sequence = sequence->get(SQLEntity::BBEnd) - (sequence->get(SQLEntity::FramePos) - Point2f(bbSize, bbSize));

    Point2f start_intersect = Point2f(fmaxf(start_trained.x, start_sequence.x), fmaxf(start_trained.y, start_sequence.y));
    Point2f end_intersect = Point2f(fminf(end_trained.x, end_sequence.x), fminf(end_trained.y, end_sequence.y));

    // 2. Ensure valid boxes and that they overlap
    if ((start_trained.x == -1 && start_trained.y == -1 && end_trained.x == -1 && end_trained.y == -1)
        || (start_sequence.x == -1 && start_sequence.y == -1 && end_sequence.x == -1 && end_sequence.y == -1))
    {
        return 0;
    }

    if (fmaxf(start_trained.x, start_sequence.x) > fminf(end_trained.x, end_sequence.x)
        || fmaxf(start_trained.y, start_sequence.y) > fminf(end_trained.y, end_sequence.y))
    {
        return 0;
    }

    // 3. Compute the areal for the different boxes
    float areal_trained = (end_trained.x - start_trained.x) * (end_trained.y - start_trained.y);
    float areal_sequence = (end_sequence.x - start_sequence.x) * (end_sequence.y - start_sequence.y);
    float areal_intersect = (end_intersect.x - start_intersect.x) * (end_intersect.y - start_intersect.y);

    // 4. Find overlap, and add size increase if at least as large
    if (areal_intersect == areal_trained)
        areal_intersect += areal_sequence - areal_intersect;

    float overlap = (areal_intersect * 100.0f) / areal_trained;

/*
 // DEBUG
    Statistics stats_blck;
    Mat img = imread(string("/home/baardew/bagadussii/baardew/TrainingDataGenerator/www/images/")
                     + sequence->get(SQLEntity::FrameID),
                     CV_LOAD_IMAGE_COLOR);
    rectangle(img,
              start_trained,
              end_trained,
              Scalar(255, 0, 0));
    rectangle(img,
              start_sequence,
              end_sequence,
              Scalar(0, 0, 255));
    rectangle(img,
              start_intersect,
              end_intersect,
              Scalar(0, 255, 0));

    namedWindow("Overlap", 0);
    imshow("Overlap", img);
    waitKey(0);
*/

    // 5. Return and update results
    boundingBoxCoverage += overlap;
    return overlap;
}

void CSVComparer::printStatistics()
{
    printf("[stats]\n"
           "[stats]\t=====  COMPARISON RESULTS  =====\n"
           "[stats]\t    Frames:       %10d\n"
           "[stats]\t    Bounding Box: %10.5f\n"
           "[stats]\t    Average:      %10.5f\n"
           "[stats]\n"
           "[stats]\t    Head:           %10.5f\n"
           "[stats]\t    Shoulder Left:  %10.5f\n"
           "[stats]\t    Shoulder Right: %10.5f\n"
           "[stats]\t    Elbow Left:     %10.5f\n"
           "[stats]\t    Elbow Right:    %10.5f\n"
           "[stats]\t    Hand Left:      %10.5f\n"
           "[stats]\t    Hand Right:     %10.5f\n"
           "[stats]\t    Leg Left:       %10.5f\n"
           "[stats]\t    Leg Right:      %10.5f\n"
           "[stats]\t    Knee Left:      %10.5f\n"
           "[stats]\t    Knee Right:     %10.5f\n"
           "[stats]\t    Foot Left:      %10.5f\n"
           "[stats]\t    Foot Right:     %10.5f\n"
           "[stats]\n"
           "[stats]\n",
           frames,
           boundingBoxCoverage / frames,
           eucludianDistanceAverage() / frames,
           eucludianDistance(globalStats[SQLEntity::Head]) / frames,
           eucludianDistance(globalStats[SQLEntity::ShoulderLeft]) / frames,
           eucludianDistance(globalStats[SQLEntity::ShoulderRight]) / frames,
           eucludianDistance(globalStats[SQLEntity::ElbowLeft]) / frames,
           eucludianDistance(globalStats[SQLEntity::ElbowRight]) / frames,
           eucludianDistance(globalStats[SQLEntity::HandLeft]) / frames,
           eucludianDistance(globalStats[SQLEntity::HandRight]) / frames,
           eucludianDistance(globalStats[SQLEntity::LegLeft]) / frames,
           eucludianDistance(globalStats[SQLEntity::LegRight]) / frames,
           eucludianDistance(globalStats[SQLEntity::KneeLeft]) / frames,
           eucludianDistance(globalStats[SQLEntity::KneeRight]) / frames,
           eucludianDistance(globalStats[SQLEntity::FootLeft]) / frames,
           eucludianDistance(globalStats[SQLEntity::FootRight]) / frames
        );
}
