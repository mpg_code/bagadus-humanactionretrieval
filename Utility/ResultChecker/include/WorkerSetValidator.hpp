#ifndef WORKER_SET_VALIDATOR_H
#define WORKER_SET_VALIDATOR_H

#include "SQLDataReader.hpp"
#include "SQLEntity.hpp"
#include "Statistics.hpp"
#include "Visualizer.hpp"

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <unordered_map>
#include <fstream>

class WorkerSetValidator
{
public:
    WorkerSetValidator(SQLDataReader &trained, std::string &imageDir,
                       string &csvExportFilename, bool display, ofstream &goldLogOut);
    void process();

private:
    struct GoldLogEntry {
        int totalJoints;
        int errors;
    };

    const float medianFilterThreshold = 20.0f; // Up to
    const float acceptanceThreshold = 36.0f; // Up to inclusive

    std::unordered_map<int, GoldLogEntry> goldMap;

    SQLDataReader &data;
    std::string imageDir;
    bool display;

    std::ofstream csvData;
    std::ofstream &goldLogOut;

    SQLEntity *prevEnt;
    SQLEntity *nextEnt;
    SQLEntity previousPlot;

    int totalWorkers;
    int acceptedWorkers;

    int fillFrames(std::vector<SQLEntity> &frames);
    void processSequence(std::vector<SQLEntity> &frames, bool useGoldLogging);
    cv::Point2f findCenter(int joint, std::vector<SQLEntity> &frames);
    cv::Point2f averageDeviation(int joint, std::vector<SQLEntity> &frames, cv::Point2f &center);
    void logGoldError(SQLEntity &ent, bool accepted);
    void exportGoldErrors();
    void writeEntity(SQLEntity &ent);
    void addRejected(int sessionID, GoldLogEntry &entry);
    void rejectBadFrames(std::vector<SQLEntity> &frames);
    std::vector<SQLEntity> rejectBadFramesMedian(std::vector<SQLEntity> &frames);
    float median(std::vector<float> &points);

    std::vector<SQLEntity> createAcceptedVector(std::vector<SQLEntity> &frames,
                                                int joint,
                                                cv::Point2f &center,
                                                cv::Point2f &deviation,
                                                bool logGold = false);


};

#endif
