#ifndef VISUALIZER_H
#define VISUALIZER_H

#include "SQLEntity.hpp"
#include "Statistics.hpp"

#include <vector>

void renderImage(SQLEntity &entity, Mat &image, Statistics &stats);
void renderImage(SQLEntity &control, SQLEntity &trained, Mat &image, Statistics &stats);
void renderImage(Mat &image, int joint, Point2f &prevCenter, Point2f &prevPrevCenter, Point2f &center, Point2f &deviation, vector<SQLEntity> &frames);

#endif
