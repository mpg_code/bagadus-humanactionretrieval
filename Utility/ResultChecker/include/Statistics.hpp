#ifndef STATISTICS_H
#define STATISTICS_H

#include "SQLEntity.hpp"

class Statistics
{
public:
    double error;    // Distance
    int overPlot;    // Visible when should not
    int underPlot;   // Obscured when should not
    float line;      // Points forms a line (max distance from line)
    float cluster;   // Points forms a cluster (max radius)
    float maskError; // Distance from foreground player (max distance)

    Statistics();
    Statistics(SQLEntity &entity, string &imageDir);
    Statistics(SQLEntity &control, SQLEntity &trained, string &imageDir);

    Statistics& operator+=(const Statistics& rhs) {
        this->error += rhs.error;
        this->overPlot += rhs.overPlot;
        this->underPlot += rhs.underPlot;
        this->line += rhs.line;
        this->cluster += rhs.cluster;
        this->maskError += rhs.maskError;
        return *this;
    }

    friend Statistics operator+(Statistics lhs, const Statistics& rhs) {
        return lhs += rhs;
    }

    double jointDistance(Point2f control, Point2f trained);
    double distanceErrors(SQLEntity &control, SQLEntity &trained);
    int isOverPlot(SQLEntity &control, SQLEntity &trained);
    int isUnderPlot(SQLEntity &control, SQLEntity &trained);
    float clusterDiameter(SQLEntity &ent);
    double distanceFromLine(Point2f start, Point2f end, Point2f point);
    float lineDistance(SQLEntity &ent);
    bool acceptedImage();
    float maskDistance(SQLEntity &ent, string &imageDir);
};

#endif
