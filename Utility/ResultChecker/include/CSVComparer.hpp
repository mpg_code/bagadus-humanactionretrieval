#ifndef CSV_COMPARER_H
#define CSV_COMPARER_H

#include "SQLDataReader.hpp"
#include "SQLEntity.hpp"

#include <vector>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>

class CSVComparer
{
public:
    enum CompareMethod {
        SequenceRelative,
        SequenceAbsolute,
    };

    CSVComparer();
    void compare(char *trainedCSV, char *sequenceCSV, char *exportCSV, CompareMethod method);
    void overlap(char *trainedCSV, char *sequenceCSV, char *exportCSV);
    void printStatistics();

private:
    int frames;
    SQLEntity globalStats;
    float boundingBoxCoverage;

    bool entityBehind(SQLEntity *trained, SQLEntity *sequence);
    bool inSync(SQLEntity *trained, SQLEntity *sequence);
    SQLEntity comparePlots(SQLEntity *trained, SQLEntity *sequence, CompareMethod method);
    float eucludianDistance(Point2f point);
    float eucludianDistanceAverage();
    float boxOverlap(SQLEntity *trained, SQLEntity *sequence);
    void exportFrameStatistics(std::ofstream &out, SQLEntity &stats, float overlap);
    float absolutePosition(float pointCoordinate, float frameOffset, float boundingOffset, CompareMethod method);
};

#endif
