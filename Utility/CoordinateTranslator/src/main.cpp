#include "SQLDataReader.hpp"
#include "SQLEntity.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

int main(int argc, char **argv)
{
    if (argc != 3) {
        printf("USAGE: %s <input.csv> <output.csv>\n", argv[0]);
        return EXIT_FAILURE;
    }

    /**
     * Translating coordinates (joint pixel locations)
     * is a simple matter of reading the file and exporting the final values.
     *
     * All the resulting values are absolute to the upper-left corner.
     */

    SQLDataReader in(argv[1]);
    SQLEntity *ent = in.next();

    ofstream out;
    out.open(argv[2]);

    out << "sessionID,timecode,frameID,frameX,frameY,width,height,players,control,processed,motion,sequenceNo,"
        "bb_x1,bb_y1,bb_x2,bb_y2,h_x,h_y,s_lx,s_ly,s_rx,s_ry,e_lx,e_ly,e_rx,e_ry,n_lx,n_ly,n_rx,n_ry,"
        "l_lx,l_ly,l_rx,l_ry,k_lx,k_ly,k_rx,k_ry,f_lx,f_ly,f_rx,f_ry"
        << endl;

    while (ent != NULL) {
        Point2f centerOffset;
        centerOffset.x = (ent->get(SQLEntity::Size).x) / 2;
        centerOffset.y = (ent->get(SQLEntity::Size).y) / 2;

        Point2f frameStart;
        frameStart.x = ent->get(SQLEntity::FramePos).x - centerOffset.x;
        frameStart.y = ent->get(SQLEntity::FramePos).y - centerOffset.y;

        out << ent->get(SQLEntity::SessionID) << ","
            << "\"" << ent->get(SQLEntity::Timecode) << "\","
            << "\"" << ent->get(SQLEntity::FrameID) << "\","
            << frameStart.x << "," << frameStart.y << ","
            << ent->get(SQLEntity::Size).x << "," << ent->get(SQLEntity::Size).y << ","
            << ent->get(SQLEntity::Players) << ","
            << ent->get(SQLEntity::Control) << ","
            << ent->get(SQLEntity::Processed) << ","
            << "\"" << ent->get(SQLEntity::Motion) << "\","
            << ent->get(SQLEntity::SequenceNo) << ",";

        for (int i = SQLEntity::BBStart; i < SQLEntity::DoF_start; i++) {
            out << (*ent)[i].x + frameStart.x << "," << (*ent)[i].y + frameStart.y << ",";
        }

        for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
            if ((*ent)[i].x == -1 && (*ent)[i].y == -1)
                out << "-1,-1,";
            else
                out << (*ent)[i].x + ent->get(SQLEntity::BBStart).x + frameStart.x << "," << (*ent)[i].y + ent->get(SQLEntity::BBStart).y + frameStart.y << ",";
        }

        out << endl;

        delete ent;
        ent = in.next();
    }

    out.close();

    return EXIT_SUCCESS;
}
