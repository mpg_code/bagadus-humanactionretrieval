# Shared

Shared contains configuration and setup files, with the configuration files set accordingly to the input data found here: http://home.ifi.uio.no/paalh/dataset/alfheim/

## lib
Lib contains shared code that is used across all the programs/projects. Particularly, the tracking algoritm, optical flow computation and all the video/data/sql reading and synchronization code is found here.

## Configurations
configurations files are a pair-wise set of pixel coordinates between camera and sensor data. <filed.config> coordinates mappes to camera coordinates in <camera.config>. Must have an equal number of pairs. Format:

```
<filed.config>     <camera.config>
  <x0> <y0>    =>    <x0> <y0>
  <x1> <y1>    =>    <x1> <y1>
  <x2> <y2>    =>    <x2> <y2>
     ...               ...
  <xn> <yn>    =>    <xn> <yn>
```
Or see the configuration files for an example. It also includes background images for two of the camera angles that can be used for background subtaction initialization.

## CMake.modules
Common CMake modules to find FFmpeg and OpenCV installations. If another version of OpenCV that the one specified in this repo is used, or another type of installation, then the OpenCV files here must be changed (should be present after an OpenCV install).
