#ifndef CAMERA_FIELD_MAPPER_H
#define CAMERA_FIELD_MAPPER_H

#include "SimpleCSVReader.hpp"

#include <vector>
#include <opencv2/opencv.hpp>

class CameraFieldMapper
{
public:
    CameraFieldMapper(char *cameraConfigFile, char *fieldConfigFile);

    void transformPlayer(struct PlayerPosition &player);

private:
    cv::Mat homography;

    std::vector<cv::Point2f> fileToPoints(char *filename);
};

#endif
