#ifndef SIMPLE_CSV_READER_H
#define SIMPLE_CSV_READER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>

/**
 * Reads Bagadus CSV files.
 *
 * Only extracts timesamp and coordinates.
 */
struct PlayerPosition {
    std::string timecode;
    int id;
    float x;
    float y;
    float heading;
    float direction;
    float speed;
    char eof;
};

class SimpleCSVReader
{
public:
    SimpleCSVReader(std::string filename);
    ~SimpleCSVReader();

    struct PlayerPosition nextLine();
    void close();

    static size_t stringToTime(std::string timecode);

private:
    std::ifstream file;
    std::string last;
};

#endif
