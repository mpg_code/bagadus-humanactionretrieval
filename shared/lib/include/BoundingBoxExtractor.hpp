#ifndef BOUNDING_BOX_EXTRACTOR_H
#define BOUNDING_BOX_EXTRACTOR_H

#include "SimpleCSVReader.hpp" // Contains definition of struct PlayerPosition
#include "FrameSequenceElement.hpp"

#include <vector>
#include <opencv2/opencv.hpp>

class BoundingBoxExtractor
{
public:
    BoundingBoxExtractor(int boundingMaxSize, int width, int height, unsigned char *framedata);
    BoundingBoxExtractor(int boundingMaxSize, int width, int height, cv::Mat frame);
    ~BoundingBoxExtractor();

    cv::Mat YUVtoMat(unsigned char *frame);
    struct FrameSequenceElement computeFrameSequenceElement(struct PlayerPosition position,
                                                            cv::Mat frame,
                                                            bool playerVisible = true);

    bool hasTrackingFailure();
    void freeTracking();
    void zoomScaleSequence(std::vector<FrameSequenceElement> &sequence);

    /* Alternatives are tested but not selected results from blob detection */
    std::vector<std::vector<cv::Point>> getAlternatives(cv::Rect &offset);

private:
    int boundingMaxSize;
    int width;
    int height;
    bool trackingFailure;
    struct PlayerPosition curPosition;
    cv::BackgroundSubtractorMOG2 *BGSubtractor;
    cv::Rect prevRoi;
    cv::Mat curImage;
    cv::Mat curMask;
    cv::Rect curRoi;
    cv::Rect contourOffset;
    std::vector<std::vector<cv::Point>> contours;

#ifdef BG_TEST
public:
#else
private:
#endif
    void initializeBackgroundSubtraction(cv::Mat frame);
    cv::Point2i computeBoundingBox();
    void setFrame(struct PlayerPosition position, cv::Mat frame);
    cv::Mat createNormalizedFrame(struct FrameSequenceElement &elm);

    void addMargin(cv::Rect &roi);

    cv::Mat backgroundSubtraction(cv::Mat frame);
    cv::Rect generateDefaultROI(struct PlayerPosition position);

    void compressBox(cv::Rect &roi, cv::Mat mask);
    cv::Rect findBiggestBlob(cv::Rect &roi, cv::Mat &mask);
    cv::Point2f centerShape(cv::Rect roi, cv::Mat &mask);
    void limitRoiBounds(cv::Rect &roi);

    bool hitMaskVertical(cv::Mat mask, cv::Point2i start, int height);
    bool hitMaskHorisontal(cv::Mat mask, cv::Point2i start, int width, bool isSignedChar = false);
    double roiOverlapArea(cv::Rect &roi, cv::Rect &interest);
    bool targetAsTracking(double &area, double &largestArea, cv::Rect &roi, cv::Rect &interest, double &largestOverlap);

    int median(std::vector<int> &scales);

    /* Variables configurations and numbers */
private:
    // Bounding Box
    const int PIXEL_MARGIN = 6;

    // Gaussian Filter used on foreground mask
    const int GAUSS_KSIZE_X = 7;
    const int GAUSS_KSIZE_Y = 7;

    // Foreground mask minimum value (inclusive)
    const unsigned char MASK_THRESHOLD = 100;

    // MOG2 parameters
    const int HISTORY_LENGTH = 250;
    const float MAHALANOBIS_DISTANCE = 25;
    const float BACKGROUND_RATIO = 0.8f;
    const float VAR_THRESHOLD_GEN = 128;
    const float F_VAR_INIT = 40;
    const float F_VAR_MIN = 25;
    const float F_VAR_MAX = 50;
    const int N_SHADOW_DETECTION = 0;
    const float F_CT = 0.7;
    const float F_TAU = 0.3;
};

#endif
