#ifndef TIMETAKER_H
#define TIMETAKER_H

#include <sys/time.h>
#include <stdlib.h>

/**
 * @file
 *
 * @brief A set of operations to perform timetaking of a task.
 *
 * @note The measurement uses the C time function, which means that it
 * is not completely accurate.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Starts the clock for a specified holder.
 *
 * The clock is started by setting the current time value to it.
 *
 * @param clock The clock to start.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_start(struct timeval *clock);

/**
 * @brief Stops the clock for a specified holder.
 *
 * The clock is stopped by setting the current time value to it.
 *
 * @param clock The clock to start.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_stop(struct timeval *clock);

/**
 * @brief Retrieves the actual time between two clocks.
 *
 * @param from The _start clock.
 * @param to The _stop clock.
 * @param sec The time part for seconds.
 * @param usec The time part for microseconds.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_time(struct timeval *from, struct timeval *to, long int *sec, long int *usec);

#ifdef __cplusplus
}
#endif


#ifdef MEASURE_TIME
#    define TAKE_TIME_VARS struct timeval _time_start, _time_stop; long _time_sec, _time_usec; float _time_fps;

#    define TAKE_TIME(function, counter, description)                   \
  timetaker_start(&_time_start);                                        \
  function;                                                             \
  timetaker_stop(&_time_stop);                                          \
  timetaker_time(&_time_start, &_time_stop, &_time_sec, &_time_usec);   \
  _time_fps = (counter) / (_time_sec + (_time_usec / 1000000.0f));      \
  fprintf(stderr, "%s :: Total time = %02ld.%06ld sec (%.2f FPS)\n", description, _time_sec, _time_usec, _time_fps);
#else
#    define TAKE_TIME_VARS
#    define TAKE_TIME(function, counter, description) function
#endif

#endif /* TIMETAKER_H */
