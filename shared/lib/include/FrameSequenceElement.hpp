#ifndef FRAME_SEQUENCE_ELEMENT_H
#define FRAME_SEQUENCE_ELEMENT_H

#include "SQLEntity.hpp"

struct FrameSequenceElement
{
    cv::Mat frame;
    cv::Mat mask;
    cv::Rect roi;
    cv::Rect normalizedRoi;
    cv::Mat normalized;
    cv::Mat flowField;
    cv::Point2i normalizedOffset;
    struct PlayerPosition position;
    SQLEntity *entity;
};

#endif
