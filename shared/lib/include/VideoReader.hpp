#ifndef VIDEO_READER_H
#define VIDEO_READER_H

// Contact : Vamsi
/* This is a small class to read a h264 video file and form frames.
   This is to keep things simple. And one has to mess only here when
   something goes wrong.
*/

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
}


class VideoReader {

    AVCodecContext *avCodecPtr;
    AVFormatContext *avFormatPtr;
    AVInputFormat *avFileFormat;
    AVFrame *pFrame;
    AVPacket packet;
    int pWidth;
    int pHeight;
    int frameFinished;
    int err;
    double fps;
    bool firstFrame;


public:
    VideoReader();
    ~VideoReader();
    void initialize(const char* fileName);//Load the first file
    void reloadFile(const char* fileName);//Assume that nothing changes but file
    int loadFrame(unsigned char* Y, unsigned char* U, unsigned char* V);
    int getWidth();
    int getHeight();
    double getFps();
};

#endif
