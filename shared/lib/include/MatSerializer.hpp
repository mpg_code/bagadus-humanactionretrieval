#ifndef MAT_SERIALIZER_H
#define MAT_SERIALIZER_H

#include <opencv2/opencv.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/vector.hpp>
#include <string>

class FrameMat
{
public:
    int frame;
    float heading;
    float direction;
    float speed;
    cv::Mat mat;
};

BOOST_SERIALIZATION_SPLIT_FREE(FrameMat)
namespace boost {
    namespace serialization {

        template<class Archive>
        void save(Archive &ar, const FrameMat &m, const unsigned int version)
        {
            assert(version == version);

            size_t elem_size = m.mat.elemSize();
            size_t elem_type = m.mat.type();

            ar & m.frame;
            ar & m.heading;
            ar & m.direction;
            ar & m.speed;
            ar & m.mat.cols;
            ar & m.mat.rows;
            ar & elem_size;
            ar & elem_type;

            const size_t data_size = m.mat.cols * m.mat.rows * elem_size;
            ar & boost::serialization::make_array(m.mat.ptr(), data_size);
        }

        template<class Archive>
        void load(Archive &ar, FrameMat &m, const unsigned int version)
        {
            assert(version == version);

            int cols, rows;
            size_t elem_size, elem_type;

            ar & m.frame;
            ar & m.heading;
            ar & m.direction;
            ar & m.speed;
            ar & cols;
            ar & rows;
            ar & elem_size;
            ar & elem_type;

            m.mat.create(rows, cols, elem_type);

            size_t data_size = m.mat.cols * m.mat.rows * elem_size;
            ar & boost::serialization::make_array(m.mat.ptr(), data_size);
        }

    }
}
#endif
