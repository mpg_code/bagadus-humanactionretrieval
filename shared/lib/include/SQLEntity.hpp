#ifndef SQL_ENTITY_H
#define SQL_ENTITY_H

#include <vector>
#include <string>
#include <opencv2/opencv.hpp>

#define TRAINING_IMAGE_SIZE 500

using namespace cv;
using namespace std;

class SQLEntity
{
public:
    enum ReadMode {
        Absolute,
        Relative,
    };

    enum TupleEntity {
        FramePos,
        Size,

        BBStart = 2,
        BBEnd = 3,
        Head = 4,
        ShoulderLeft = 5,
        ShoulderRight = 6,
        ElbowLeft = 7,
        ElbowRight = 8,
        HandLeft = 9,
        HandRight = 10,
        LegLeft = 11,
        LegRight = 12,
        KneeLeft = 13,
        KneeRight = 14,
        FootLeft = 15,
        FootRight = 16,
    };

    enum StringEntity {
        Timecode,
        FrameID,
        Motion,
        Username,
    };

    enum IntEntity {
        SessionID,
        Players,
        Control,
        Processed,
        SequenceNo,
    };

public:
    // Indexes to where the DoFs are within operator[]
    static const int DoF_start = 4;
    static const int DoF_end = 17;

    SQLEntity();
    SQLEntity(string csvLine, ReadMode readMode = Relative);

    void copyFrom(SQLEntity &ent);

    Point2f get(TupleEntity entity);
    string get(StringEntity entity);
    int get(IntEntity entity);

    Point2f& operator[] (const int index);

private:
    Point2f csvToPoint(istringstream &iss);

    float trainingImageScaling(int realSize); // Step 0
    Point2f boundingBoxToPixels(float scale, int x, int y); // Step 1
    float boundingBoxScaling(Point2f start, Point2f end); // Step 2
    Point2f jointToPixels(float scaling, int x, int y); // Step 3

    std::string eraseQuotationMarks(std::string &token);

    Point2f positions[17]; // position, size (width,height), 2 bounding box, 13 DoF
    string *meta_string[4]; // Timestamp, frameID, motion, username
    int meta_number[5]; // sessionID, players, processed, control, sequenceNo
};

#endif
