#ifndef SQL_DATA_READER_H
#define SQL_DATA_READER_H

#include "SQLEntity.hpp"

#include <fstream>

using namespace std;

class SQLDataReader
{
public:
    SQLDataReader(char *filenameS, SQLEntity::ReadMode readMode = SQLEntity::Relative);
    ~SQLDataReader();

    SQLEntity* next();
    
private:
    ifstream file;
    SQLEntity::ReadMode readMode;
};

#endif
