#ifndef OPTICAL_FLOW_H
#define OPTICAL_FLOW_H

#include <vector>
#include <opencv2/opencv.hpp>

#ifdef USE_CUDA
#include <opencv2/superres/optical_flow.hpp>
#endif

class OpticalFlow
{
public:
    OpticalFlow();
    ~OpticalFlow();

    /**
     * IN: image_A, image_B
     * OUT: features, status and error
     */
    void computeSparse(cv::Mat image_A,
                       cv::Mat image_B,
                       std::vector<cv::Point2f> &features,
                       std::vector<uchar> &status,
                       std::vector<float> &error);

    void computeDense(cv::Mat image_A,
                      cv::Mat image_B,
                      cv::Mat &opticalFlow);

    std::vector<cv::Point2f> getPrevSparseFeatures();
    cv::Mat drawOptFlowMap(const cv::Mat& flow);
    cv::Mat drawOptFlowChannel(const cv::Mat& flow, int direction);
    cv::Mat drawOptFlowVector(const cv::Mat &flow, const cv::Mat &image);
    cv::Mat* expandChannels(cv::Mat &flow);
    void normalizeChannels(cv::Mat *channels);

    const int VECTOR_CHANNELS = 4;
    
private:
    // Sparse parameters
    const int MAX_EDGES = 5000;
    const double QUALITY_LEVEL = 0.1;
    const double MIN_DISTANCE = 0.7;

    // Dense parameters
    /*
     * DEFAULT VALUES
     
     const double TAU = 0.25;
     const double LAMBDA = 0.15;
     const double THETA = 0.3;
     const int N_SCALES = 5;
     const int WARPS = 5;
     const double EPSILON = 0.01;
     const int ITERATIONS = 300;
     
    */
     
    const double TAU = 0.15;
    const double LAMBDA = 0.15;
    const double THETA = 0.03;
    const int N_SCALES = 5;
    const int WARPS = 5;
    const double EPSILON = 0.01;
    const int ITERATIONS = 300;
    
    std::vector<cv::Point2f> prevSparseFeatures;

#ifdef USE_CUDA
    cv::Ptr<cv::superres::DenseOpticalFlowExt> denseOptic;
#else
    cv::Ptr<cv::DenseOpticalFlow> denseOptic;
#endif
};

#endif
