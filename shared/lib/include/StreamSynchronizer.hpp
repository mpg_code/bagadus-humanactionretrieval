#ifndef STREAM_SYNCHRONIZER_H
#define STREAM_SYNCHRONIZER_H

#include "VideoReader.hpp"
#include "SimpleCSVReader.hpp"

class StreamSynchronizer
{
public:
    StreamSynchronizer(std::string csvFilename, std::string clipFolder, std::string clipFilename);
    ~StreamSynchronizer();

    bool getVideo(unsigned char *frame);
    bool getPlayer(struct PlayerPosition &player);
    struct PlayerPosition peakPlayer();
    bool skipFrames(int frames);

    bool syncCsvToVideo(bool showProgress = false);
    bool syncVideoToCsv(bool showProgress = false);

    int getWidth();
    int getHeight();

private:
    // Constants measure time in ms
    const int CsvProcessDelay = 3400;
    const ssize_t clipSkipBuffer = CsvProcessDelay;

    SimpleCSVReader csv;
    VideoReader video;
    std::ifstream clipList;

    size_t videoTime;
    size_t videoTimeStep;

    size_t frameBytesize;
    unsigned char *framebuf;
    unsigned char *y_ptr;
    unsigned char *u_ptr;
    unsigned char *v_ptr;

    struct PlayerPosition currentPosition;
    struct PlayerPosition nextPosition;

    std::string clipFolder;

    // Control variables for clip management
    bool getNextClip;
    bool isFirstClip;

    void log();
    bool readVideoFrame();
    bool openNextClip();
};

#endif
