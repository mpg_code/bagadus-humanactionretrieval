#include "CameraFieldMapper.hpp"

#include <stdio.h>
#include <stdlib.h>

using namespace cv;

const int MINIMUM_REQUIRED_POINTS = 4;

CameraFieldMapper::CameraFieldMapper(char *cameraConfigFile, char *fieldConfigFile)
{
    std::vector<Point2f> cPoints = fileToPoints(cameraConfigFile);
    std::vector<Point2f> fPoints = fileToPoints(fieldConfigFile);

    if (cPoints.size() != fPoints.size()
        || cPoints.size() < MINIMUM_REQUIRED_POINTS
        || fPoints.size() < MINIMUM_REQUIRED_POINTS) {
        fprintf(stderr, "Calibration data points does not match.\n");
        exit(EXIT_FAILURE);
    }
    
    homography = findHomography(fPoints, cPoints);
}

void CameraFieldMapper::transformPlayer(struct PlayerPosition &player)
{
    std::vector<Point2f> vec, out;
    vec.push_back(Point2f(player.x, player.y));

    perspectiveTransform(vec, out, homography);

    player.x = out[0].x;
    player.y = out[0].y;
}

std::vector<Point2f> CameraFieldMapper::fileToPoints(char *filename)
{
    std::vector<Point2f> vec;
    std::ifstream file;
    std::string valX, valY;

    file.open(filename, std::ifstream::in);
    if (!file.is_open())
        std::cerr << "Cannot open file: " << filename << std::endl;

    while (file >> valX >> valY) {
        vec.push_back(Point2f(atof(valX.c_str()), atof(valY.c_str())));
    }

    file.close();

    return vec;
}
