#include "StreamSynchronizer.hpp"

#include <stdio.h>
#include <iostream>

#define DEBUG false

#if DEBUG
#    define dLog(argv...) { fprintf(stderr, argv); }
#    define dLogWait(argv...) {fprintf(stderr, argv); cin.ignore(); }
#else
#    define dLog(argv...)
#    define dLogWait(argv...)
#endif

using namespace std;

StreamSynchronizer::StreamSynchronizer(string csvFilename, string clipFolder, string clipFilename) :
    csv(csvFilename),
    videoTime(0),
    clipFolder(clipFolder),
    getNextClip(true),
    isFirstClip(true)
{

    dLog("\n\nCSV File: %s\nClip Folder: %s\nClip Filename: %s\n\n", csvFilename.c_str(), clipFolder.c_str(), clipFilename.c_str());

    clipList.open(clipFilename, ifstream::in);
    if (!clipList.is_open()) {
        fprintf(stderr, "Cannot open video clips list file: %s\n", clipFilename.c_str());
        exit(EXIT_FAILURE);
    }

    currentPosition = csv.nextLine();
    nextPosition = csv.nextLine();

    openNextClip();

    frameBytesize = (video.getHeight() + video.getHeight()/2) * video.getWidth();
    framebuf = (unsigned char*)malloc(frameBytesize);
    y_ptr = framebuf;
    u_ptr = &framebuf[video.getHeight() * video.getWidth()];
    v_ptr = &framebuf[(video.getHeight() + video.getHeight()/4) * video.getWidth()];

    video.loadFrame(y_ptr, u_ptr, v_ptr);

    double interval = (1.0 / video.getFps()) * 1000.0;
    videoTimeStep = (size_t)interval;

    dLogWait("FPS: %f TIME: %ld, INTERVAL: %ld\nCSV Current: %s, Next: %s\n",
             video.getFps(), videoTime, videoTimeStep,
             currentPosition.timecode.c_str(), nextPosition.timecode.c_str());
}

StreamSynchronizer::~StreamSynchronizer()
{
    free(framebuf);
}

bool StreamSynchronizer::openNextClip()
{
    string clipName;
    string clip;
    bool first = true;

    while (first || ((ssize_t)SimpleCSVReader::stringToTime(currentPosition.timecode)
                     - ((ssize_t)videoTime + CsvProcessDelay + clipSkipBuffer)) > 0) {
        first = false;

        if (!getline(clipList, clipName))
            return false;

        clip = clipFolder + "/" + clipName;

        // Clean the clipName string (i.e. remove running number tag and file extension to get start time)
        clipName = clipName.erase(0, 5);
        clipName = clipName.erase(29, clipName.length());
        videoTime = SimpleCSVReader::stringToTime(clipName) + CsvProcessDelay;
    }

    if (isFirstClip) {
        video.initialize(clip.c_str());
        isFirstClip = false;
    } else {
        video.reloadFile(clip.c_str());
    }

    dLogWait("Opening video clip: %s (%s)\n", clip.c_str(), clipName.c_str());
    return true;
}

bool StreamSynchronizer::readVideoFrame()
{
    if (getNextClip) {
        if (!openNextClip())
            return false;

        getNextClip = false;
    }

    int read = video.loadFrame(y_ptr, u_ptr, v_ptr);
    if (read == -1) {
        return false;
    }

    videoTime += videoTimeStep;

    // Frame is skipped
    if (read == 0) {
        getNextClip = true;
        return readVideoFrame();
    }

    return true;
}

int StreamSynchronizer::getWidth()
{
    return video.getWidth();
}

int StreamSynchronizer::getHeight()
{
    return video.getHeight();
}

bool StreamSynchronizer::getVideo(unsigned char *frame)
{
    dLog("%s CALL\n", __FUNCTION__);

    if (!memcpy(frame, framebuf, frameBytesize)) {
        log();
        return false;
    }

    if (!readVideoFrame()) {
        log();
        return false;
    }

    syncCsvToVideo();
    return true;
}

bool StreamSynchronizer::getPlayer(struct PlayerPosition &player)
{
    dLog("%s CALL\n", __FUNCTION__);

    player = currentPosition;
    currentPosition = nextPosition;
    if (player.eof) {
        log();
        return false;
    }

    nextPosition = csv.nextLine();
    return syncVideoToCsv();
}

struct PlayerPosition StreamSynchronizer::peakPlayer()
{
    return nextPosition;
}

bool StreamSynchronizer::skipFrames(int frames)
{
    for (int i = 0; i < frames; i++) {
        if (!readVideoFrame())
            return false;

        if (!syncCsvToVideo())
            return false;
    }

    return true;
}

bool StreamSynchronizer::syncVideoToCsv(bool showProgress)
{
    dLog("ADVANCING VIDEO: video duration interval = [%ld, %ld], csv position = %ld (video from target = %ld)",
         videoTime, videoTime+videoTimeStep, csv.stringToTime(currentPosition.timecode),
         (videoTime + videoTimeStep) - csv.stringToTime(currentPosition.timecode));

    // EOF is sucsessfull sync, since there is a 1 iteration delay,
    // and the next iteration will catch the  EOF signal present in current.
    if (currentPosition.eof)
        return true;

    float totalTimeDifference = (csv.stringToTime(currentPosition.timecode) - videoTime) / 100;
    while (videoTime + videoTimeStep < csv.stringToTime(currentPosition.timecode)) {
        if (!readVideoFrame()) {
            log();
            return false;
        }

        if (showProgress && !DEBUG) {
            float percentage = 100 - ((csv.stringToTime(currentPosition.timecode)
                                       - (videoTime + videoTimeStep)) / totalTimeDifference);
            fprintf(stderr, "Fast-Forwaring Video to CSV: %.2f%%\r",
                    percentage > 100 || percentage < 0 ? 100.0f : percentage);
        }
    }

    if (showProgress && !DEBUG)
        fprintf(stderr, "\n");


    log();
    return true;
}

bool StreamSynchronizer::syncCsvToVideo(bool showProgress)
{
    dLog("ADVANCING CSV: video duration interval = [%ld, %ld], csv position = %ld (csv from target = %ld)\n",
         videoTime, videoTime+videoTimeStep, csv.stringToTime(currentPosition.timecode),
         csv.stringToTime(currentPosition.timecode) - videoTime);

    size_t cur = csv.stringToTime(currentPosition.timecode);
    float totalTimeDifference = (cur - videoTime) / 100;

    while (cur < videoTime)
    {
        currentPosition = nextPosition;
        cur = csv.stringToTime(currentPosition.timecode);
        if (currentPosition.eof) {
            log();
            return false;
        }

        nextPosition = csv.nextLine();

        if (showProgress && !DEBUG) {
            float percentage = 100 - ((cur - videoTime) / totalTimeDifference);
            fprintf(stderr, "Fast-Forwarding CSV to Video: %.2f%%\r", percentage > 100 || percentage < 0 ? 100.0f : percentage);
        }
    }

    if (showProgress && !DEBUG)
        fprintf(stderr, "\n");


    log();
    return true;
}

void StreamSynchronizer::log()
{
#if DEBUG
    char videoDate[50];
    sprintf(videoDate, ":::%ld.%ld", videoTime/1000, videoTime%1000);
    printf("\n\nVIDEO: interval = [%ld-%ld] (%s)\nCSV: position = %ld (%s)\n",
           videoTime, videoTime+videoTimeStep, videoDate,
           csv.stringToTime(currentPosition.timecode),
           currentPosition.timecode.c_str());
    cin.ignore();
#endif
}
