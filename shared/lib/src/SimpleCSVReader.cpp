#include "SimpleCSVReader.hpp"

SimpleCSVReader::SimpleCSVReader(std::string filename)
{
    file.open(filename.c_str(), std::ifstream::in);
    if (!file.is_open())
        std::cerr << "Cannot open file: " << filename << std::endl;
}

SimpleCSVReader::~SimpleCSVReader()
{
    file.close();
}

struct PlayerPosition SimpleCSVReader::nextLine()
{
    struct PlayerPosition meta;
    std::string data;

    if (!std::getline(file, data)) {
        meta.eof = true;
        return meta;
    } else if (data.length() <= 0) {
        meta.eof = true;
        return meta;
    } else {
        meta.eof = false;
    }

    std::istringstream ss(data);
    std::string token;

    std::getline(ss, token, ',');

    // Timecode, remove the ""s if present
    if (token.at(0) == '"') {
        token.erase(0, 1);
        token.erase(token.length()-1, 1);
    }

    meta.timecode = token;
    last = token;

    // ID
    std::getline(ss, token, ',');
    meta.id = std::stoi(token);

    // X-pos
    std::getline(ss, token, ',');
    meta.x = std::stof(token);

    // Y-pos
    std::getline(ss, token, ',');
    meta.y = std::stof(token);

    // Heading
    std::getline(ss, token, ',');
    meta.heading = std::stof(token);

    // Direction
    std::getline(ss, token, ',');
    meta.direction = std::stof(token);

    // Energy
    std::getline(ss, token, ',');
    // ignore

    // Speed
    std::getline(ss, token, ',');
    meta.speed = std::stof(token);

    // Ignore rest

    return meta;
}

void SimpleCSVReader::close()
{

}

size_t SimpleCSVReader::stringToTime(std::string timecode)
{

    size_t tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_usec;
    char s_usec[10];
    sscanf((char*)timecode.c_str(), "%zu-%zu-%zu %zu:%zu:%zu.%s", &tm_year, &tm_mon, &tm_mday, &tm_hour, &tm_min, &tm_sec, s_usec);

    // Only use the first 3 digits in the subsection part. -48 to convert between ASCII char and number
    tm_usec
        = ((s_usec[0] - 48) * 100)
        + ((s_usec[1] - 48) *  10)
        + ((s_usec[2] - 48) *   1);

    size_t time
        = (tm_hour   * 3600000)
        + (tm_min    *   60000)
        + (tm_sec    *    1000)
        + (tm_usec   *       1);

    return time;
}
