#include "timetaker.h"
#define MICROSECONDS_IN_A_SECOND 1000000

/**
 * @file
 *
 * @brief A set of operations to perform timetaking of a task.
 *
 * @note The measurement uses the C time function, which means that it
 * is not completely accurate.
 */

/**
 * @brief Starts the clock for a specified holder.
 *
 * The clock is started by setting the current time value to it.
 *
 * @param clock The clock to start.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_start(struct timeval *clock)
{
    if (clock == NULL) return -1;
    return gettimeofday(clock, NULL);
}

/**
 * @brief Stops the clock for a specified holder.
 *
 * The clock is stopped by setting the current time value to it.
 *
 * @param clock The clock to start.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_stop(struct timeval *clock)
{
    if (clock == NULL) return -1;
    return gettimeofday(clock, NULL);
}

/**
 * @brief Retrieves the actual time between two clocks.
 *
 * @param from The _start clock.
 * @param to The _stop clock.
 * @param sec The time part for seconds.
 * @param usec The time part for microseconds.
 * @return -1 on failure, otherwise 0.
 */
int timetaker_time(struct timeval *from, struct timeval *to, long int *sec, long int *usec)
{
    if (from == NULL || to == NULL || sec == NULL || usec == NULL) return -1;

    *sec = to->tv_sec - from->tv_sec;
    *usec = to->tv_usec - from->tv_usec;

    if (*sec < 0)
        *sec = 0;

    if (*usec < 0) {
        --(*sec);
        *usec += MICROSECONDS_IN_A_SECOND;
    } else if (*usec >= MICROSECONDS_IN_A_SECOND) {
        (*sec) += *usec / MICROSECONDS_IN_A_SECOND;
        *usec = *usec % MICROSECONDS_IN_A_SECOND;
    }

    return 0;
}
