#include "OpticalFlow.hpp"

#include <stdio.h>

using namespace std;
using namespace cv;
#ifdef USE_CUDA
using namespace cv::superres;
#endif

OpticalFlow::OpticalFlow()
{
#ifdef USE_CUDA
    denseOptic = createOptFlow_DualTVL1_GPU();
#else
    denseOptic = createOptFlow_DualTVL1();
#endif
    denseOptic->set("tau", TAU);
    denseOptic->set("lambda", LAMBDA);
    denseOptic->set("theta", THETA);
    denseOptic->set("nscales", N_SCALES);
    denseOptic->set("warps", WARPS);
    denseOptic->set("epsilon", EPSILON);
    denseOptic->set("iterations", ITERATIONS);
    denseOptic->set("useInitialFlow", 0);
}

OpticalFlow::~OpticalFlow()
{

}

void OpticalFlow::computeSparse(Mat image_A,
                                Mat image_B,
                                vector<Point2f> &features,
                                vector<uchar> &status,
                                vector<float> &error)
{
    goodFeaturesToTrack(image_A, prevSparseFeatures, MAX_EDGES, QUALITY_LEVEL, MIN_DISTANCE);
    goodFeaturesToTrack(image_B, features, MAX_EDGES, QUALITY_LEVEL, MIN_DISTANCE);

    calcOpticalFlowPyrLK(image_A, image_B, prevSparseFeatures, features, status, error);
}

void OpticalFlow::computeDense(cv::Mat image_A,
                               cv::Mat image_B,
                               cv::Mat &opticalFlow)
{
    denseOptic->calc(image_A, image_B, opticalFlow);

// Alternative LK implementation (not as good)
/*
  vector<Point2f> listOfPixels;
  for (int y = 0; y < image_A.rows; y++)
  for (int x = 0; x < image_A.cols; x++)
  listOfPixels.push_back(Point2f(x, y));

  vector<Point2f> flow;
  vector<uchar> status;
  vector<float> error;

  calcOpticalFlowPyrLK(image_A, image_B, listOfPixels, flow, status, error);

  for (int y = 0; y < opticalFlow.rows; y++) {
  for (int x = 0; x < opticalFlow.cols; x++) {
  int index = y * opticalFlow.cols + x;

  if (status[index]) {
  printf("%f and %f\n", x - flow[index].x, y - flow[index].y);
  opticalFlow.at<Vec2f>(Point(x, y)) = Vec2f(x - flow[index].x, y - flow[index].y);
  }
  }
  }
*/
}

vector<Point2f> OpticalFlow::getPrevSparseFeatures()
{
    return prevSparseFeatures;
}

Mat OpticalFlow::drawOptFlowMap(const Mat& flow)
{
    Mat cflowmap(flow.cols, flow.rows, CV_8UC3);

    for (int y = 0; y < cflowmap.rows; y++) {
        for (int x = 0; x < cflowmap.cols; x++) {
            const Point2f& fxy = flow.at<Point2f>(y, x);

            int dirColor_R = 0;
            int dirColor_G = 0;
            int dirColor_B = 0;
            if (fxy.y < 0 && fxy.x < 0) {
                dirColor_R = 1;
            } else if (fxy.y > 0 && fxy.x < 0) {
                dirColor_G = 1;
            } else if (fxy.y < 0 && fxy.x > 0) {
                dirColor_B = 1;
            } else {
                dirColor_R = 1;
                dirColor_G = 1;
            }

            int scalar = sqrt(fxy.x * fxy.x + fxy.y * fxy.y) * 50;
            if (scalar > 255)
                scalar = 255;
            if (scalar < 0)
                scalar = 0;

            dirColor_R *= scalar;
            dirColor_G *= scalar;
            dirColor_B *= scalar;

            circle(cflowmap, Point(x,y), 0, Scalar(dirColor_B, dirColor_G, dirColor_R));
        }
    }

    return cflowmap;
}

Mat OpticalFlow::drawOptFlowChannel(const Mat& flow, int direction)
{
    Mat cflowmap(flow.cols, flow.rows, CV_8UC3);

    for (int y = 0; y < cflowmap.rows; y++) {
        for (int x = 0; x < cflowmap.cols; x++) {
            float fv = flow.at<float>(y, x);

            int dirColor_R = 0;
            int dirColor_G = 0;
            int dirColor_B = 0;

            if (direction == 0) {
                dirColor_R = 1;
            } else if (direction == 1) {
                dirColor_G = 1;
            } else if (direction == 2) {
                dirColor_B = 1;
            } else {
                dirColor_R = 1;
                dirColor_G = 1;
            }

            int scalar = fv * 5000;
            if (scalar > 255)
                scalar = 255;
            if (scalar < 0)
                scalar = 0;

            dirColor_R *= scalar;
            dirColor_G *= scalar;
            dirColor_B *= scalar;

            circle(cflowmap, Point(x,y), 0, Scalar(dirColor_B, dirColor_G, dirColor_R));
        }
    }

    return cflowmap;
}

Mat OpticalFlow::drawOptFlowVector(const Mat &flow, const Mat &image)
{
    Mat frame(image.rows, image.cols, CV_8UC3);
    Mat convertedImage(image.rows, image.cols, CV_8UC3);

    if (image.type() != frame.type())
        cvtColor(image, convertedImage, CV_GRAY2RGB);
    else
        image.copyTo(convertedImage);

    convertedImage.copyTo(frame);

    const int sizeFac = 4;
    resize(frame, frame, Size(image.cols * sizeFac, image.rows * sizeFac), 0, 0);

    for (int y = 0; y < flow.rows; y++) {
        for (int x = 0; x < flow.cols; x++) {
            if (x % 4 == 2 && y % 4 == 2) {
                Point2f vector = flow.at<Point2f>(y, x);
                Point2i origin(x * sizeFac, y * sizeFac);
                line(frame, origin, Point2i((x + (vector.x / 2)) * sizeFac, (y + (vector.y / 2)) * sizeFac), Scalar(0, 255, 255), 1);
            }
        }
    }

    for (int y = 0; y < flow.rows; y++) {
        for (int x = 0; x < flow.cols; x++) {
            if (x % 4 == 2 && y % 4 == 2) {
                Point2i origin(x * sizeFac, y * sizeFac);
                circle(frame, origin, 0, Scalar(128, 0, 128));
            }
        }
    }

    return frame;
}

Mat* OpticalFlow::expandChannels(Mat &flow)
{
    Mat *channels = new Mat[VECTOR_CHANNELS];

    for (int i = 0; i < VECTOR_CHANNELS; i++)
        channels[i] = Mat(flow.rows, flow.cols, CV_32FC1, Scalar(0));

    for (int y = 0; y < flow.rows; y++) {
        for (int x = 0; x < flow.cols; x++) {
            const Point2f &intensity = flow.at<Point2f>(y, x);

            if (intensity.x < 0) {
                channels[0].at<float>(y, x) = intensity.x * -1;
            } else {
                channels[1].at<float>(y, x) = intensity.x;
            }

            if (intensity.y < 0) {
                channels[2].at<float>(y, x) = intensity.y * -1;
            } else {
                channels[3].at<float>(y, x) = intensity.y;
            }
        }
    }

    return channels;
}

void OpticalFlow::normalizeChannels(Mat *channels)
{
    for (int channel = 0; channel < VECTOR_CHANNELS; channel++) {

        Mat &flow = channels[channel];

        // Find norm
        float sum = 0;
        for (int y = 0; y < flow.rows; y++)
            for (int x = 0; x < flow.cols; x++)
                sum += flow.at<float>(y, x);

        // Normalize
        float mean = sum / (flow.rows * flow.cols);

        for (int y = 0; y < flow.rows; y++)
            for (int x = 0; x < flow.cols; x++)
                flow.at<float>(y, x) /= mean;
    }
}
