#include "SQLDataReader.hpp"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

SQLDataReader::SQLDataReader(char *filename, SQLEntity::ReadMode readMode) :
    readMode(readMode)
{
    file.open(filename, ifstream::in);
    if (!file.is_open()) {
        printf("Cannot open file: %s\n", filename);
        exit(EXIT_FAILURE);
    }

    // Skip header line
    string data;
    getline(file, data);
}

SQLDataReader::~SQLDataReader()
{
    file.close();
}

SQLEntity* SQLDataReader::next()
{
    if (!file.is_open())
        return NULL;

    string data;
    if (!getline(file, data))
        return NULL;

    return new SQLEntity(data, readMode);
}
