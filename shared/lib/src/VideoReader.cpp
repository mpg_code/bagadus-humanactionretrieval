// Contact : Vamsi
// This is a common code for reading video files using ffmpeg. So not many
// comments.
// The idea is that it get a call from the viewer to load frame, then it just
// loads the frame into the memories from viewer.

#include "VideoReader.hpp"

#include <stdio.h>
#include <iostream>
#include <sys/time.h>

unsigned long GetTimeStamp() {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec*(unsigned long)1000000+tv.tv_usec;
}

int VideoReader::loadFrame(unsigned char* Y, unsigned char* U, unsigned char* V) {
    if(firstFrame) {
        firstFrame = false;
    } else {
        pFrame = avcodec_alloc_frame();
        int readErr = av_read_frame(avFormatPtr, &packet);
        err = avcodec_decode_video2(avCodecPtr, pFrame, &frameFinished, &packet);
        if(!frameFinished){
            if (err == 0) {
                av_free_packet(&packet);
                avcodec_free_frame(&pFrame);
                return 0;
            }
            /*
              std::cerr << "Failed to get frame from decoded video\n";
              if(err < 0){
              std::cerr << "Error decoding frame, returned " << err << std::endl;
              }else if(err == 0){
              std::cerr << "No image could be found when decoding video\n";
              }
              if(readErr != 0){
              std::cerr << "Read frame returned " << readErr << std::endl;
              }
              std::cerr << "SKIPPING FRAME\n";
            */
            av_free_packet(&packet);
            avcodec_free_frame(&pFrame);
            return -1;
        }
    }

    int i;
    for(i=0; i<pHeight; i++)
        memcpy(Y + i*pWidth, pFrame->extended_data[0] + i*pFrame->linesize[0], pWidth);

    for(i=0; i<pHeight/2; i++) {
        memcpy(U + i*pWidth/2, pFrame->extended_data[1] + i*pFrame->linesize[0]/2, pWidth/2);
        memcpy(V + i*pWidth/2, pFrame->extended_data[2] + i*pFrame->linesize[0]/2, pWidth/2);
    }

    av_free_packet(&packet);
    avcodec_free_frame(&pFrame);
    return 1;
}

int VideoReader::getHeight() {
    return pHeight;
}

int VideoReader::getWidth() {
    return pWidth;
}

double VideoReader::getFps() {
    return fps;
}


// This method is to load files, in the example we just reload the file,
// but more complicated stuff can be accomplished by loading different files.
// This is carefully written and experimented with to not leave any memories
// unhandled. Changing any of the clearing lines can cause serious problems.
// Remember: Videos are huge!

void VideoReader::reloadFile(const char* fileName) {

    //Clear previous pointers.
    avcodec_close(avCodecPtr);
    av_free(avCodecPtr);
    av_opt_free(avFormatPtr);
    avFormatPtr = NULL;
    avCodecPtr = NULL;

    AVDictionary **opts;
    AVDictionary *codec_opts;
    AVDictionary *format_opts = NULL;
    int counterTemp=0;;
    avFormatPtr = avformat_alloc_context();
    avFileFormat = av_find_input_format(fileName);

    err = avformat_open_input(&avFormatPtr, fileName, NULL, &format_opts);

    if(err!=0) {
        std::cout<<"Error opening video file '"<<fileName<<"'"<<std::endl;
        exit(1);
    }
    avFormatPtr->max_analyze_duration = 50000;
    err = avformat_find_stream_info(avFormatPtr, &format_opts);
    int i, videoStream;

    for (i=0; i<avFormatPtr->nb_streams; i++)
        if(avFormatPtr->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            videoStream = i;
            break;
        }


    if(videoStream == -1)
        std::cout<<"Could not find any video stream"<<std::endl;

    avCodecPtr = avFormatPtr->streams[videoStream]->codec;

    AVCodec *avCodec=NULL;


    avCodec = avcodec_find_decoder(avCodecPtr->codec_id);
    pWidth = avCodecPtr->width;
    pHeight = avCodecPtr->height;

    if(avCodec==NULL) {
        std::cout<<"error codec not found"<<std::endl;
        exit(1);
    }

    if(avcodec_open2(avCodecPtr, avCodec, &format_opts)<0) {
        std::cout<<"error opening codec"<<std::endl;
        exit(1);
    }
    firstFrame = true;
    pFrame = avcodec_alloc_frame();

    //Trying for the first frame.
    while(av_read_frame(avFormatPtr, &packet)>=0) {

        err = avcodec_decode_video2(avCodecPtr, pFrame, &frameFinished, &packet);
        //Loading parameters
        counterTemp++;
        av_free_packet(&packet);
        if(pFrame->linesize[0]>0)
            break;

    }
//  avcodec_free_frame(&pFrame);
}
void VideoReader::initialize(const char* fileName) {
    avFormatPtr = NULL;
    avCodecPtr = NULL;

    avcodec_register_all();
    av_register_all();
    AVDictionary **opts;
    AVDictionary *codec_opts;
    AVDictionary *format_opts = NULL;
    unsigned long start, end;
    int counterTemp=0;

    avFormatPtr = avformat_alloc_context();

    avFileFormat = av_find_input_format(fileName);

    err = avformat_open_input(&avFormatPtr, fileName, NULL, &format_opts);

    if(err!=0) {
        std::cout<<"Error opening video file '"<<fileName<<"'"<<std::endl;
        exit(1);
    }
    avFormatPtr->max_analyze_duration = 50000;
    start = GetTimeStamp();
    err = avformat_find_stream_info(avFormatPtr, &format_opts);

    int i, videoStream;
    for (i=0; i<avFormatPtr->nb_streams; i++)
        if(avFormatPtr->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            videoStream = i;
            break;
        }


    if(videoStream == -1)
        std::cout<<"Could not find any video stream"<<std::endl;

    avCodecPtr = avFormatPtr->streams[videoStream]->codec;
    fps = ((double)avFormatPtr->streams[videoStream]->r_frame_rate.num)/
        ((double)avFormatPtr->streams[videoStream]->r_frame_rate.den);

    AVCodec *avCodec=NULL;


    avCodec = avcodec_find_decoder(avCodecPtr->codec_id);
    pWidth = avCodecPtr->width;
    pHeight = avCodecPtr->height;

    if(avCodec==NULL) {
        std::cout<<"error codec not found"<<std::endl;
        exit(1);
    }



    if(avcodec_open2(avCodecPtr, avCodec, &format_opts)<0) {
        std::cout<<"error opening codec"<<std::endl;
        exit(1);
    }
    firstFrame = true;

    pFrame = avcodec_alloc_frame();

    //Trying for the first frame.
    while(av_read_frame(avFormatPtr, &packet)>=0) {

        err = avcodec_decode_video2(avCodecPtr, pFrame, &frameFinished, &packet);
        counterTemp++;
        //std::cout<<pFrame->linesize[0]<<std::endl;
        av_free_packet(&packet);
        if(pFrame->linesize[0]>0)
            break;

    }
    //avcodec_free_frame(&pFrame);
}

//Empty initializer for now, we need the video object file to play around
// the viewer.
VideoReader::VideoReader() {


}

VideoReader::~VideoReader() {
    av_free_packet(&packet);
    avcodec_free_frame(&pFrame);
}
