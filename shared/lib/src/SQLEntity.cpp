#include "SQLEntity.hpp"

#include <string>
#include <iterator>

#define NaN (0.0f / 0.0f)

SQLEntity::SQLEntity()
{
    meta_number[0] = 0;
    for (int i = DoF_start; i < DoF_end; i++)
        positions[i] = Point2f(0, 0);
}

SQLEntity::SQLEntity(string csvLine, ReadMode readMode)
{
    istringstream iss(csvLine);
    string token;
    float num_x, num_y;

    // sessionID
    getline(iss, token, ',');
    meta_number[0] = atoi(token.c_str());

    // timecode
    getline(iss, token, ',');
    meta_string[0] = token.compare("NULL") ? new string(eraseQuotationMarks(token)) : new string("");

    // frameID
    getline(iss, token, ',');
    meta_string[1] = token.compare("NULL") ? new string(eraseQuotationMarks(token)) : new string("");

    // frame (x,y) aka. position
    getline(iss, token, ',');
    num_x = token.compare("NULL") ? atof(token.c_str()) : -1.0f;
    getline(iss, token, ',');
    num_y = token.compare("NULL") ? atof(token.c_str()) : -1.0f;
    positions[0] = Point2f(num_x, num_y);

    // width/height aka. size
    getline(iss, token, ',');
    num_x = token.compare("NULL") ? atof(token.c_str()) : -1.0f;
    getline(iss, token, ',');
    num_y = token.compare("NULL") ? atof(token.c_str()) : -1.0f;
    positions[1] = Point2f(num_x, num_y);

    // players
    getline(iss, token, ',');
    meta_number[1] = token.compare("NULL") ? atoi(token.c_str()) : 1;

    // control
    getline(iss, token, ',');
    meta_number[2] = atoi(token.c_str());

    // processed
    getline(iss, token, ',');
    meta_number[3] = atoi(token.c_str());

    // motion
    getline(iss, token, ',');
    meta_string[2] = token.compare("NULL") ? new string(eraseQuotationMarks(token)) : new string("");

    // sequenceNo
    getline(iss, token, ',');
    meta_number[4] = token.compare("NULL") ? atoi(token.c_str()) : 1;

    // Note that x and y has the exact same value (boundingBoxSize * 2)
    float initial_scaling = trainingImageScaling(positions[1].x);

    // bb_1, bb_2
    Point2f bb_start = csvToPoint(iss);
    if (readMode == Absolute)
        positions[2] = bb_start;
    else
        positions[2] = boundingBoxToPixels(initial_scaling, bb_start.x, bb_start.y);

    Point2f bb_end = csvToPoint(iss);
    if (readMode == Absolute)
        positions[3] = bb_end;
    else
        positions[3] = boundingBoxToPixels(initial_scaling, bb_end.x, bb_end.y);

    float full_scaling = 1;
    if (bb_start.x != -1 && bb_start.y != -1 && bb_end.x != -1 && bb_end.y != -1)
        full_scaling = initial_scaling * boundingBoxScaling(bb_start, bb_end);

    // h, sl, sr, el, er, nl, nr, ll, lr, kl, kr, fl, fr
    for (int i = SQLEntity::DoF_start; i < SQLEntity::DoF_end; i++) {
        Point2f original = csvToPoint(iss);
        if (readMode == Absolute)
            positions[i] = original;
        else
            positions[i] = jointToPixels(full_scaling, original.x, original.y);
    }

    // See if 'username' is present at the end, if not then ignore
    if (!iss.eof()) {
        getline(iss, token, ',');
        meta_string[3] = token.compare("NULL") ? new string(token) : new string("");
    } else {
        meta_string[3] = new string("");
    }
}

void SQLEntity::copyFrom(SQLEntity &ent)
{
    meta_string[0] = new string(ent.get(Timecode));
    meta_string[1] = new string(ent.get(FrameID));
    meta_string[2] = new string(ent.get(Motion));
    meta_string[3] = new string(ent.get(Username));

    // SessionID
    meta_number[0] = 0;

    meta_number[1] = ent.get(Players);

    meta_number[4] = ent.get(SequenceNo);

    // Control
    meta_number[2] = 0;

    // Processed
    meta_number[3] = 1;

    positions[0] = ent.get(FramePos);
    positions[1] = ent.get(Size);

    // BBStart
    positions[2] = Point2f(-1, -1);

    // BBEnd
    positions[3] = Point2f(-1, -1);

    for (int i = DoF_start; i < DoF_end; i++)
        positions[i] = Point2f(-1, -1);
}

inline string SQLEntity::eraseQuotationMarks(string &token)
{
    if (token.at(0) == '"') {
        string edit = token.erase(0, 1);
        return edit.erase(edit.length() - 1, edit.length());
    }

    return token;
}

Point2f& SQLEntity::operator[] (const int index)
{
    return positions[index];
}

Point2f SQLEntity::get(TupleEntity entity)
{
    switch (entity) {
    case FramePos:
        return positions[0];
    case Size:
        return positions[1];
    case BBStart:
    case BBEnd:
    case Head:
    case ShoulderLeft :
    case ShoulderRight:
    case ElbowLeft:
    case ElbowRight:
    case HandLeft:
    case HandRight:
    case LegLeft:
    case LegRight:
    case KneeLeft:
    case KneeRight:
    case FootLeft:
    case FootRight:
        return positions[entity];
    default:
        fprintf(stderr, "ERROR: Unknown TupleEntity '%d'\n", entity);
        exit(EXIT_FAILURE);
    }
}

string SQLEntity::get(StringEntity entity)
{
    switch (entity) {
    case Timecode:
        return *meta_string[0];
    case FrameID:
        return *meta_string[1];
    case Motion:
        return *meta_string[2];
    case Username:
        return *meta_string[3];
    default:
        fprintf(stderr, "ERROR: Unknown StringEntity '%d'\n", entity);
        exit(EXIT_FAILURE);
    }
}

int SQLEntity::get(IntEntity entity)
{
    switch (entity) {
    case SessionID:
        return meta_number[0];
    case Players:
        return meta_number[1];
    case Control:
        return meta_number[2];
    case Processed:
        return meta_number[3];
    case SequenceNo:
        return meta_number[4];
    default:
        fprintf(stderr, "ERROR: Unknown IntEntity '%d'\n", entity);
        exit(EXIT_FAILURE);
    }
}

Point2f SQLEntity::csvToPoint(istringstream &iss)
{
    string token;
    float num_x, num_y;

    getline(iss, token, ',');
    num_x = token.compare("NULL") ? atof(token.c_str()) : NaN;
    getline(iss, token, ',');
    num_y = token.compare("NULL") ? atof(token.c_str()) : NaN;

    return Point2f(num_x, num_y);
}

float SQLEntity::trainingImageScaling(int realSize)
{
    return (float)TRAINING_IMAGE_SIZE / realSize;
}

float SQLEntity::boundingBoxScaling(Point2f start, Point2f end)
{
    // bs = bounding scale
    float bsx = TRAINING_IMAGE_SIZE / (end.x - start.x);
    float bsy = TRAINING_IMAGE_SIZE / (end.y - start.y);
    float bs = fmin(bsx, bsy) / 2.0f; // div-by-2 since upscaling is limited to half of a full frame
    return fmax(bs, 1.0f); // No scaling below 1
}

Point2f SQLEntity::boundingBoxToPixels(float scale, int x, int y)
{
    if (x == -1 && y == -1)
        return Point2f(x, y);

    return Point2f(x / scale, y / scale);
}

Point2f SQLEntity::jointToPixels(float scaling, int x, int y)
{
    if (x == -1 && y == -1)
        return Point2f(-1.0f, -1.0f);

    float px = x / scaling;
    float py = y / scaling;
    return Point2f(px, py);
}
