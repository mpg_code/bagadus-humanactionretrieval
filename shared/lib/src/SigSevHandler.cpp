/**
 * Code segment to get a stack trace when OpenCV assertion fails.
 *
 * It is basically a signal handler.
 */

#include "SigSevHandler.hpp"

namespace SigSevHandler {

    static char* exe = 0;

    // Comment in to get messages (usually library trace),
    // of that of which is commented out
    void signalHandler(int sig, siginfo_t *info, void *secret) {
        void *trace[16];
//        char **messages = (char **)NULL;
        int trace_size = 0;
        ucontext_t *uc = (ucontext_t *)secret;

        if (sig == SIGSEGV) {
            printf("Got signal %d, faulty address is %p, "
                   "from %llx\n", sig, info->si_addr,
                   uc->uc_mcontext.gregs[REG_RIP]);
        } else {
            printf("Got signal %d;\n", sig);
        }

        trace_size = backtrace(trace, 16);
        trace[1] = (void *) uc->uc_mcontext.gregs[REG_RIP];
//        messages = backtrace_symbols(trace, trace_size);

        printf("[bt] Execution path:;\n");

        for (int i = 1; i < trace_size; i++) {
//            printf("[bt] %s;\n", messages[i]);
            char syscom[1024];
            sprintf(syscom,"addr2line %p -e %s", trace[i] , exe);
            system(syscom);

        }

        exit(EXIT_FAILURE);
    }
    
    void enableSignalHandler(char *executable)
    {
        exe = executable;

        struct sigaction sa;
        sa.sa_sigaction = &signalHandler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART | SA_SIGINFO;

        // Register all signals than needs to be caught
        sigaction(SIGABRT, &sa, NULL);
        sigaction(SIGSEGV, &sa, NULL);
    }
};
