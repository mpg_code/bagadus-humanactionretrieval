#include "BoundingBoxExtractor.hpp"

#include <algorithm>
#include <stdio.h>

#include <opencv2/video/background_segm.hpp>

using namespace cv;
using namespace std;

BoundingBoxExtractor::BoundingBoxExtractor(int boundingMaxSize, int width, int height, unsigned char *framedata) :
    boundingMaxSize(boundingMaxSize),
    width(width),
    height(height),
    trackingFailure(false)
{
    initializeBackgroundSubtraction(YUVtoMat(framedata));
}

BoundingBoxExtractor::BoundingBoxExtractor(int boundingMaxSize, int width, int height, Mat frame) :
    boundingMaxSize(boundingMaxSize),
    width(width),
    height(height),
    trackingFailure(false)
{
    initializeBackgroundSubtraction(frame);
}

void BoundingBoxExtractor::initializeBackgroundSubtraction(Mat frame)
{
    BGSubtractor = new BackgroundSubtractorMOG2(HISTORY_LENGTH, MAHALANOBIS_DISTANCE);

    BGSubtractor->set("backgroundRatio", BACKGROUND_RATIO);
    BGSubtractor->set("varThresholdGen", VAR_THRESHOLD_GEN);
    BGSubtractor->set("fVarInit", F_VAR_INIT);
    BGSubtractor->set("fVarMin", F_VAR_MIN);
    BGSubtractor->set("fVarMax", F_VAR_MAX);
    BGSubtractor->set("nShadowDetection", N_SHADOW_DETECTION);
    BGSubtractor->set("fCT", F_CT);
    BGSubtractor->set("fTau", F_TAU);

    backgroundSubtraction(frame);
}

BoundingBoxExtractor::~BoundingBoxExtractor()
{
    delete BGSubtractor;
}

Mat BoundingBoxExtractor::YUVtoMat(unsigned char *yuv)
{
    Mat frame;
    Mat mat_yuv(height + height/2, width, CV_8UC1, yuv);
    cvtColor(mat_yuv, frame, COLOR_YUV2RGB_YV12);
    return frame;
}

void BoundingBoxExtractor::setFrame(struct PlayerPosition position, Mat frame)
{
    curPosition = position;
    curImage = frame;
    curMask = backgroundSubtraction(frame);
}

struct FrameSequenceElement BoundingBoxExtractor::computeFrameSequenceElement(struct PlayerPosition position,
                                                                              cv::Mat frame,
                                                                              bool playerVisible)
{
    setFrame(position, frame);

    FrameSequenceElement elm;
    if (playerVisible) {
        curRoi = Rect(0, 0, width, height);
        elm.normalizedOffset = computeBoundingBox();
    } else {
        prevRoi = curRoi = Rect(0, 0, 0, 0);
        elm.normalizedOffset = Point2i(0, 0);
    }

    elm.frame = curImage;
    elm.mask = curMask;
    elm.roi = curRoi;
    elm.position = curPosition;
    elm.entity = NULL;
    elm.normalized = createNormalizedFrame(elm);
    return elm;
}

Point2i BoundingBoxExtractor::computeBoundingBox()
{
    trackingFailure = false;

    // 1. Generate the initial Region Of Interest
    Rect roi, origin;
    roi = origin = generateDefaultROI(curPosition);

    // 2. Compress based on initial ROI and foreground mask
    // Replace with OpenCV findRoi method?
    compressBox(roi, curMask);

    // Test if we actually have something, if not abort
    if (roi.width == 0 || roi.height == 0) {
        prevRoi = Rect(0, 0, 0, 0);
        return Point2i(0, 0);
    }

    // 3. Narrow down the shape to the largest blob
    Rect narrowRoi = findBiggestBlob(roi, curMask);
    roi.x += narrowRoi.x;
    roi.y += narrowRoi.y;
    roi.width = narrowRoi.width;
    roi.height = narrowRoi.height;

    // 4. Add margin to account for inaccuracies/noise
    addMargin(roi);

    // 5. Stabilize the images to remove external movements as much as possible
    Point2f center = centerShape(roi, curMask);
    roi.x += center.x;
    roi.y += center.y;
    roi.width -= center.x;
    roi.height -= center.y;

    // 6. Crop stabilized ROI to avoid out-of-bounds
    limitRoiBounds(roi);

    // 7. Keep state
    prevRoi = curRoi = roi;

    // DEBUG START
    if (!(roi.x >= 0 && roi.y >= 0 && roi.x < width && roi.y < height
          && roi.x + roi.width < curImage.cols && roi.y + roi.height < curImage.rows)) {
        fprintf(stderr, "\n\n\nROI failure => x = %d, y = %d, width = %d, height = %d\n", roi.x, roi.y, roi.width, roi.height);
        fprintf(stderr, "Image resulution: %d x %d, requesting %d x %d\n\n\n",
                curImage.cols, curImage.rows, roi.x + roi.width, roi.y + roi.height);
        exit(EXIT_FAILURE);
    }
    // DEBUG END

    return Point2i(roi.x - origin.x, roi.y - origin.y);
}

Mat BoundingBoxExtractor::createNormalizedFrame(struct FrameSequenceElement &elm)
{
    // Obtain grayscale image
    int actualBoundingSize = boundingMaxSize * 2;

    Mat frame;
    cvtColor(elm.frame, frame, CV_BGR2GRAY);

    Mat maskedFull;
    frame.copyTo(maskedFull, elm.mask);

    Mat roiMask(frame.size(), CV_8UC1, Scalar(0));
    rectangle(roiMask,
              Point(elm.roi.x, elm.roi.y),
              Point(elm.roi.x + elm.roi.width, elm.roi.y + elm.roi.height),
              Scalar(255),
              CV_FILLED);

    Mat masked;
    maskedFull.copyTo(masked, roiMask);

    // Obtain normalized frame positions
    Point2i centerRoi(elm.roi.x + elm.roi.width / 2, elm.roi.y + elm.roi.height / 2);
    Rect sliceRoi(centerRoi.x - boundingMaxSize, centerRoi.y - boundingMaxSize, actualBoundingSize, actualBoundingSize);

    Point2i centerOffset(0, 0);
    if (sliceRoi.x < 0) {
        sliceRoi.width -= fabsf(sliceRoi.x);
        centerOffset.x = fabsf(sliceRoi.x);
        sliceRoi.x = 0;
    }

    if (sliceRoi.y < 0) {
        sliceRoi.height -= fabsf(sliceRoi.y);
        centerOffset.y = fabsf(sliceRoi.y);
        sliceRoi.y = 0;
    }

    if (sliceRoi.x + sliceRoi.width >= elm.frame.cols)
        sliceRoi.width -= (sliceRoi.x + sliceRoi.width) - elm.frame.cols;

    if (sliceRoi.y + sliceRoi.width >= elm.frame.rows)
        sliceRoi.width -= (sliceRoi.y + sliceRoi.height) - elm.frame.rows;

    // Create normalized frame
    Mat normalized(actualBoundingSize, actualBoundingSize, CV_8UC1, Scalar(0));

    if (sliceRoi.width <= 0
        || sliceRoi.height <= 0
        || sliceRoi.width > actualBoundingSize
        || sliceRoi.height > actualBoundingSize)
    {
        return normalized;
    }

    elm.normalizedRoi = sliceRoi;

    // Copy only roi elements
    for (int y = 0; y < sliceRoi.height; y++)
        for (int x = 0; x < sliceRoi.width; x++)
            normalized.at<char>(y + centerOffset.y, x + centerOffset.x)
                = masked.at<char>(y + sliceRoi.y, x + sliceRoi.x);

    return normalized;
}

Rect BoundingBoxExtractor::generateDefaultROI(struct PlayerPosition position)
{
    // A box is defined with two points: the top left starting corner + width and height from that point
    Point2i point;
    point.x = max((int)position.x - boundingMaxSize, 0);
    point.y = max((int)position.y - boundingMaxSize, 0);


    int widthDifference = (point.x + boundingMaxSize*2) - (width - 1);
    if (widthDifference < 0)
        widthDifference = 0;

    int heightDifference = (point.y + boundingMaxSize*2) - (height - 1);
    if (heightDifference < 0)
        heightDifference = 0;

    Size size;
    size.width = (boundingMaxSize * 2) - widthDifference;
    size.height = (boundingMaxSize * 2) - heightDifference;

    return Rect(point, size);
}

void BoundingBoxExtractor::limitRoiBounds(cv::Rect &roi)
{
    while (roi.x < 0 || roi.x + roi.width > width - 1) {
        roi.x++;
        roi.width--;
    }

    while (roi.y < 0 || roi.y + roi.height > height - 1) {
        roi.y++;
        roi.height--;
    }
}

void BoundingBoxExtractor::compressBox(Rect &roi, Mat mask)
{
    // Left Edge
    while (!hitMaskVertical(mask, Point2i(roi.x, roi.y), roi.y + roi.height) && roi.x < roi.x + roi.width) {
        roi.x++;
        roi.width--;
    }

    // Top Edge
    while (!hitMaskHorisontal(mask, Point2i(roi.x, roi.y), roi.x + roi.width) && roi.y < roi.y + roi.height) {
        roi.y++;
        roi.height--;
    }

    // Right Edge
    while (!hitMaskVertical(mask, Point2i(roi.x + roi.width, roi.y), roi.y + roi.height) && roi.x > 0 && roi.width > 0) {
        roi.width--;
    }

    // Bottom Edge
    while (!hitMaskHorisontal(mask, Point2i(roi.x, roi.y + roi.height), roi.x + roi.width) && roi.y > 0 && roi.height > 0) {
        roi.height--;
    }
}

bool BoundingBoxExtractor::hitMaskVertical(Mat mask, Point2i start, int height)
{
    if (start.x >= mask.cols)
        return true;

    for (int row = start.y; row < height; row++)
        if (mask.at<uchar>(row, start.x) >= MASK_THRESHOLD)
            return true;

    return false;
}

bool BoundingBoxExtractor::hitMaskHorisontal(Mat mask, Point2i start, int width, bool isSignedChar)
{
    if (start.y >= mask.rows)
        return true;

    for (int column = start.x; column < width; column++) {
        if (isSignedChar) {
            if (mask.at<uchar>(start.y, column) >= MASK_THRESHOLD)
                return true;
        } else {
            if (mask.at<char>(start.y, column) != 0)
                return true;
        }
    }

    return false;
}

Point2f BoundingBoxExtractor::centerShape(Rect roi, Mat &mask)
{
    Point2f mean(0, 0);
    int numbers = 0;

    Mat roiMask = mask(roi);

    for (int y = 0; y < roiMask.rows; y++) {
        for (int x = 0; x < roiMask.cols; x++) {
            if (roiMask.at<uchar>(y, x) != 0) {
                mean.x += x;
                mean.y += y;
                numbers++;
            }
        }
    }

    mean.x /= numbers;
    mean.y /= numbers;

    Point2f offset;
    Point2f imageCenter(roiMask.cols / 2.0f, roiMask.rows / 2.0f);

    offset.x = ceil(mean.x - imageCenter.x);
    offset.y = ceil(mean.y - imageCenter.y);

    return offset;
}


Mat BoundingBoxExtractor::backgroundSubtraction(Mat frame)
{
    Mat tempFrame(frame.size(), CV_8UC3);
    Mat mask(frame.size(), CV_8UC1);

    GaussianBlur(frame, tempFrame, Size(GAUSS_KSIZE_X, GAUSS_KSIZE_Y), 0);
    BGSubtractor->operator()(tempFrame, mask);

    return mask;
}

bool BoundingBoxExtractor::targetAsTracking(double &area, double &largestArea, Rect &roi, Rect &interest, double &largestOverlap)
{
    double intersection = roiOverlapArea(roi, interest);

    if (intersection > largestOverlap) {
        largestOverlap = intersection;
        largestArea = area;
        return true;
    }

    if (area > largestArea && intersection > 0) {
        largestArea = area;
        largestOverlap = intersection;
        return true;
    }

    return false;
}

Rect BoundingBoxExtractor::findBiggestBlob(Rect &roi, Mat &mask)
{
    contourOffset = Rect(roi.x, roi.y, roi.width, roi.height);

    Mat workMask = mask.clone();
    double largestArea = 0;
    double largestOverlap = 0;

    vector<Vec4i> hierarchy;
    findContours(workMask(roi), contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    bool selection = false;
    Rect roiSelection;
    for (uint i = 0; i < contours.size(); i++) {
        double area = contourArea(contours[i], false);
        Rect interest = boundingRect(contours[i]);

        if (targetAsTracking(area, largestArea, roi, interest, largestOverlap)) {
            roiSelection = interest;
            selection = true;
        }
    }

    // If failure to track properly, then see if it is possible to use largest blob
    if (!selection) {
        double large = 0;

        for (uint i = 0; i < contours.size(); i++) {
            double area = contourArea(contours[i], false);
            Rect interest = boundingRect(contours[i]);

            if (area > large) {
                large = area;
                roiSelection = interest;
                selection = true;
            }
        }
    }

    if (!selection)
        trackingFailure = true;

    return roiSelection;
}

double BoundingBoxExtractor::roiOverlapArea(Rect &roi, Rect &interest)
{
    // Need to modify the interest to match global camera pixel position
    Rect adapted(interest.x + roi.x, interest.y + roi.y, interest.width, interest.height);
    Rect intersection = adapted & prevRoi;
    return intersection.area();
}

void BoundingBoxExtractor::addMargin(Rect &roi)
{
    assert(PIXEL_MARGIN % 2 == 0);

    // Add a few pixels to account for noise/mismatches
    for (int i = 0; i < PIXEL_MARGIN && roi.x > 0; i++) {
        roi.x--;
        roi.width++;
    }

    for (int i = 0; i < PIXEL_MARGIN && roi.y > 0; i++) {
        roi.y--;
        roi.height++;
    }

    for (int i = 0; i < PIXEL_MARGIN && roi.x + roi.width < width - 1; i++)
        roi.width++;

    for (int i = 0; i < PIXEL_MARGIN && roi.y + roi.height < height - 1; i++)
        roi.height++;
}

void BoundingBoxExtractor::zoomScaleSequence(vector<FrameSequenceElement> &sequence)
{
    // In the firs run, simply upscale (zoom) from the center image
    // to the point when the topmost foreground pixel hits 10% below maximum height

    // Assume square-sized images
    int size = sequence[0].normalized.rows;
    int topLimit = size * 0.05f;
    vector<int> sequencePixelScales;

    for (uint i = 0; i < sequence.size(); i++) {
        int pixelScale = 0;
        Mat scaledImage = sequence[i].normalized;

        while (!hitMaskHorisontal(scaledImage, Point2i(pixelScale, pixelScale + topLimit), size, false) && pixelScale < size) {
            pixelScale++;
            resize(sequence[i].normalized, scaledImage, Size(size + pixelScale, size + pixelScale), 0, 0, INTER_CUBIC);
        }

        sequencePixelScales.push_back(pixelScale);
    }

    int pixelScale = median(sequencePixelScales);
    for (uint i = 0; i < sequence.size(); i++) {
        Mat scaledImage;
        Mat drawImage;
        // *2 as scaling happens above/left and below/right
        Rect cropRegion(pixelScale, pixelScale, size / 2, size / 2);
        // *4 as size increase double of pixels, and scaling is double
        resize(sequence[i].normalized, scaledImage, Size((size / 2) + (pixelScale * 2), (size / 2) + (pixelScale * 2)));
        sequence[i].normalized = scaledImage(cropRegion);
    }
}

int BoundingBoxExtractor::median(vector<int> &scales)
{
    sort(scales.begin(), scales.end());

    if (scales.size() % 2 != 0 )
        return scales[((scales.size() + 1) / 2)];
    else
        return scales[(scales.size() / 2)];
}

bool BoundingBoxExtractor::hasTrackingFailure()
{
    return trackingFailure;
}

void BoundingBoxExtractor::freeTracking()
{
    prevRoi = Rect(0, 0, 0, 0);
}

vector<vector<Point>> BoundingBoxExtractor::getAlternatives(Rect &offset)
{
    offset = contourOffset;
    return contours;
}
