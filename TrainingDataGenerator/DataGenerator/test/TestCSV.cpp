#include "SimpleCSVReader.hpp"

#include <iostream>

int main()
{
    SimpleCSVReader csv("2013-11-03_tromso_stromsgodset_raw_first.csv");

    for (int i = 0; i < 10; i++) {
        struct PlayerPosition p = csv.nextLine();

        if (p.eof)
            break;

        std::cout << "Timecode: " << p.timecode << " (" << csv.stringToTime(p.timecode) << ")" << std::endl
                  << "X: " << p.x << std::endl
                  << "Y: " << p.y << std::endl
                  << "Heading: " << p.heading << std::endl
                  << "Direction: " << p.direction << std::endl
                  << "Speed: " << p.speed << std::endl;
    }

    csv.close();

    return 0;
}
