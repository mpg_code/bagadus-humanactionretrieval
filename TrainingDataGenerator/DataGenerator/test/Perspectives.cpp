#include "Perspectives.hpp"

using namespace cv;

/**
 * This file is for debug purposes only, and should not be looked at.
 * (Unless you find a bug).
 *
 * Draws homography lines and points to example streams.
 * It is called from the main function in the actual program
 * and does not contain its own test program.
 */

static void transformPlayer(struct PlayerPosition &player, Mat &homography)
{
    std::vector<Point2f> vec, out;
    vec.push_back(Point2f(player.x, player.y));
    
    perspectiveTransform(vec, out, homography);
    
    player.x = out[0].x;
    player.y = out[0].y;
}

void printPerspecitve0(Mat &image, Mat &homography)
{
    struct PlayerPosition player;

    player.x = 0;
    player.y = 0;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 68;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 34;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 51;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 51;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 17;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 17;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 34;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 43.75;
    player.y = 34;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 0;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 68;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 43.75;
    player.y = 0;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));


    float line_start_x;
    float line_start_y;
    float line_end_x;
    float line_end_y;

    player.x = 0;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 0;
    player.y = 68;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    
    player.x = 0;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 0;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);    
    
    player.x = 0;
    player.y = 68;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 68;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 0;
    player.y = 34;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 34;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 15.3125;
    player.y = 68;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 15.3125;
    player.y = 0;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 0;
    player.y = 17;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 17;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 0;
    player.y = 51;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 51;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 43.75;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 43.75;
    player.y = 68;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);


    // NO_DISTORTION

    player.x = 0;
    player.y = 0;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 68;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 34;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 51;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 51;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 17;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 17;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 34;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 43.75;
    player.y = 34;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 0;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 15.3125;
    player.y = 68;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 43.75;
    player.y = 0;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 0;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 0;
    player.y = 68;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);
    
    player.x = 0;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 0;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);    
    
    player.x = 0;
    player.y = 68;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 68;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 0;
    player.y = 34;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 34;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 15.3125;
    player.y = 68;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 15.3125;
    player.y = 0;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 0;
    player.y = 17;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 17;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);
    
    player.x = 0;
    player.y = 51;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.5;
    player.y = 51;    
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 43.75;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 43.75;
    player.y = 68;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

}

void printPerspecitve1(Mat &image, Mat &homography)
{
    struct PlayerPosition player;

    player.x = 0;
    player.y = 67.66;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 16.41;
    player.y = 53.81;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 16.41;
    player.y = 42.78;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 67.66;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 43.11;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 24.82;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 0;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 42.78;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 53.81;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 67.66;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    float line_start_x;
    float line_start_y;
    float line_end_x;
    float line_end_y;

    player.x = 0;
    player.y = 67.66;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 67.66;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 88.49;
    player.y = 53.81;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 16.41;
    player.y = 53.81;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 52.42;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.42;
    player.y = 67.66;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 16.41;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 88.49;
    player.y = 0;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 88.49;
    player.y = 42.78;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 16.41;
    player.y = 42.78;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 16.41;
    player.y = 24.82;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 88.49;
    player.y = 24.82;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    // UNDISTORTED
    player.x = 0;
    player.y = 67.66;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 16.41;
    player.y = 53.81;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 16.41;
    player.y = 42.78;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 67.66;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 43.11;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 24.82;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 0;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 42.78;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 53.81;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 67.66;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));
    
    player.x = 0;
    player.y = 67.66;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 67.66;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 88.49;
    player.y = 53.81;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 16.41;
    player.y = 53.81;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 52.42;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 52.42;
    player.y = 67.66;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 16.41;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 88.49;
    player.y = 0;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 88.49;
    player.y = 42.78;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 16.41;
    player.y = 42.78;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 16.41;
    player.y = 24.82;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 88.49;
    player.y = 24.82;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

}

void printPerspecitve2(cv::Mat &image, cv::Mat &homography)
{
    struct PlayerPosition player;

    player.x = 104.89;
    player.y = 0;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 0;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 24.82;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 93.85;
    player.y = 34.10;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 13.84;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 13.84;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 53.81;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 53.81;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 67.66;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 13.84;
    transformPlayer(player, homography);
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    float line_start_x;
    float line_start_y;
    float line_end_x;
    float line_end_y;

    player.x = 52.42;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 0;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 52.42;
    player.y = 13.84;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 13.84;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 52.42;
    player.y = 53.81;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 53.81;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 104.89;
    player.y = 0;
    transformPlayer(player, homography);
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 67.66;
    transformPlayer(player, homography);
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);
    
    // UNDISTORTED

    player.x = 104.89;
    player.y = 0;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 0;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 24.82;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 93.85;
    player.y = 34.10;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 13.84;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 13.84;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 53.81;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 88.49;
    player.y = 53.81;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 104.89;
    player.y = 67.66;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 13.84;
    rectangle(image, Point(player.x - 10, player.y -10), Point(player.x + 10, player.y + 10), Scalar(255, 0, 0));

    player.x = 52.42;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 0;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 52.42;
    player.y = 13.84;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 13.84;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 52.42;
    player.y = 53.81;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 53.81;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);

    player.x = 104.89;
    player.y = 0;
    line_start_x = player.x;
    line_start_y = player.y;
    player.x = 104.89;
    player.y = 67.66;
    line_end_x = player.x;
    line_end_y = player.y;
    line(image, Point(line_start_x, line_start_y), Point(line_end_x, line_end_y), Scalar(128, 0, 255), 2);
}
