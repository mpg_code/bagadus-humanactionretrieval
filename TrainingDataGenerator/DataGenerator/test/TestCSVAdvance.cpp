#include "SimpleCSVReader.hpp"

#include <iostream>

int main()
{
    SimpleCSVReader csv("2013-11-03_tromso_stromsgodset_raw_first.csv");

    struct PlayerPosition p;
    p.timecode = "0000-00-00 00:00:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-00-00 00:00:00.5000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-00-00 00:00:01.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-00-00 00:01:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-00-00 01:00:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-00-01 00:00:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-01-00 00:00:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0001-00-00 00:00:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;

    p.timecode = "0000-01-01 00:00:00.0000";
    std::cout << "Timecode: " << p.timecode << std::endl
	      << "\tms: " << csv.stringToTime(p.timecode)
	      << std::endl;
    
    csv.close();

    return 0;

}
