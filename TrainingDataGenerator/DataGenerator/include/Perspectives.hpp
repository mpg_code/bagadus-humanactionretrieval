#ifndef PERSPECTIVE_TEST_H
#define PERSPECTIVE_TEST_H

#include "SimpleCSVReader.hpp"

#include <stdio.h>
#include <vector>

#include <opencv2/opencv.hpp>

void printPerspecitve0(cv::Mat &image, cv::Mat &homography);
void printPerspecitve1(cv::Mat &image, cv::Mat &homography);
void printPerspecitve2(cv::Mat &image, cv::Mat &homography);

#endif
