#ifndef UTILITY_H
#define UTILITY_H

#include "SQLFileCreator.hpp"
#include "StreamSynchronizer.hpp"
#include "CameraFieldMapper.hpp"
#include "MatSerializer.hpp"
#include "OpticalFlow.hpp"
#include "FrameSequenceElement.hpp"
#include "BoundingBoxExtractor.hpp"

#include <stdio.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/stream.hpp>

#define ONE_SECOND 1000

namespace Utility {
    struct ZLibArchiver {
        std::ofstream *ofs;
        boost::archive::binary_oarchive *outStream;
        boost::iostreams::filtering_streambuf<boost::iostreams::output> *dataFile;
    };

    std::vector<int> createPNGCompression();
    struct ZLibArchiver createZLibArchiver(char *filename);
    void deleteZLibArchiver(ZLibArchiver &arch);
    void checkProgramArguments(int argc, char **argv);
    bool isNewSequence(struct PlayerPosition cur, struct PlayerPosition prev, bool useSequences);
    void flushSequence(std::vector<FrameSequenceElement> &sequence,
                       std::vector<int> &index,
                       struct ZLibArchiver &arch,
                       BoundingBoxExtractor &bbe,
                       int boundingBoxSize);
};

#endif
