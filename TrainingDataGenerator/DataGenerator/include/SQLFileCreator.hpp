#ifndef SQL_FILE_CREATOR_H
#define SQL_FILE_CREATOR_H

#include "SimpleCSVReader.hpp"

#include <opencv2/opencv.hpp>

class SQLFileCreator
{

public:
    SQLFileCreator(std::string filePrefix, int boundingSize);
    ~SQLFileCreator();
    void write(struct PlayerPosition &player, std::string filename, int sequenceNo, cv::Rect boundingBox);

private:
    std::ofstream out;
    int boundingBoxSize;
};

#endif
