#include "timetaker.h"
#include "SigSevHandler.hpp"
#include "Utility.hpp"
#include "Perspectives.hpp"

using namespace cv;
using namespace std;
using namespace boost::archive;
using namespace boost::iostreams;

//#define DEBUG

#define MAX_PLAYER_ID 15

inline bool insideFrame(struct PlayerPosition &player, int frameWidth, int frameHeight)
{
    return !(player.x < 0 || player.y < 0 || player.x > frameWidth || player.y > frameHeight);
}

inline bool boundingBoxInsideFrame(struct PlayerPosition &player, StreamSynchronizer &resources, int boundingBoxSize)
{
    return !(player.x - boundingBoxSize < 0 ||  player.x + boundingBoxSize >= resources.getWidth()
             ||  player.y - boundingBoxSize < 0 ||  player.y + boundingBoxSize >= resources.getHeight());
}

// The program tracks one player at a time to generate multi-frame samples.
// Therefore, CSV data is the main source of progress while the video stream
// is kept along with it.
inline struct PlayerPosition getNextPosition(StreamSynchronizer &stream, int trackID)
{
    struct PlayerPosition player;

    bool terminate = false;
    int limit = 0;
    player.id = -1;
    while (player.id != trackID) {
        limit++;

        if (!stream.getPlayer(player)) {
            terminate = true;
            break;
        }

        if (limit >= MAX_PLAYER_ID * 2) {
            printf("\nNo player or more data for player with ID %d.\n", trackID);
            terminate = true;
            break;
        }
    }

    player.eof = terminate;

    return player;
}

inline bool stringEndsWith(char* base, const char* str)
{
    int blen = strlen(base);
    int slen = strlen(str);
    return (blen >= slen) && (0 == strcmp(base + blen - slen, str));
}


inline int getNextTrackPlayer(char *argv = NULL)
{
    static ifstream trackRecord;
    static int staticTracking;

    if (trackRecord.is_open()) {
        string num;
        getline(trackRecord, num);
        return stoi(num.c_str());
    } else if (argv != NULL && stringEndsWith(argv, ".txt")) {
        trackRecord.open(argv);
        return getNextTrackPlayer();
    } else if (argv != NULL) {
        staticTracking = stoi(argv);
    }

    return staticTracking;
}

int main(int argc, char** argv)
{
    SigSevHandler::enableSignalHandler(argv[0]);

    Utility::checkProgramArguments(argc, argv);
    int  trackPlayerId        = getNextTrackPlayer(argv[6]);
    bool useSequenceNumbering = argc > 12 ? (argv[12][0] == '#' ? true : false) : false;
    int  framesToSkip         = stoi(argv[7]);
    int  iterationLimit       = stoi(argv[8]);
    int  boundingBoxSize      = stoi(argv[9]);
    char *namePrefix          = argv[10];
    Mat  background           = imread(argv[11], CV_LOAD_IMAGE_COLOR);

    // Video reading
    CameraFieldMapper mapper(argv[4], argv[5]);
    StreamSynchronizer resources(argv[1], argv[2], argv[3]);
    resources.syncCsvToVideo(true);

    // Image and SQL creation
    vector<int> compression_params = Utility::createPNGCompression();
    SQLFileCreator sql(namePrefix, boundingBoxSize);

    // Optical Flow creation
    BoundingBoxExtractor *bbextractor = NULL;
    Utility::ZLibArchiver serArchive = Utility::createZLibArchiver(argv[10]);

    // Single linear memory with pointers to copy directly to memory
    unsigned char *framedata = (unsigned char*)malloc((resources.getHeight() + resources.getHeight() / 2) * (resources.getWidth()));

    // Player tracking
    struct PlayerPosition player;
    struct PlayerPosition prevPlayer;
    prevPlayer.timecode = "0000-00-00 00:00:00.000000";

    // Program end statistics
    struct timeval start;
    struct timeval stop;
    timetaker_start(&start);

    // Image and sequence counters
    int progressCounter = 0;
    int sequenceCounter = useSequenceNumbering ? -1 : 0;
    bool firstFrame = true;
    bool notSwitched = true;
    vector<FrameSequenceElement> sequence;
    vector<int> sequenceIndex;

    while (true) {
        progressCounter++;

        if (iterationLimit > 0 && progressCounter-1 >= iterationLimit) {
            printf("\nStop on: Iteration limit\n");
            break;
        }

        player = getNextPosition(resources, trackPlayerId);
        if (player.eof) {
            printf("\nStop on: No more CSV data\n");
            break;
        }

        // If hour jump then switch configuration settings; plotted manually here
        if (player.timecode.c_str()[12] == '9' && notSwitched) {
            notSwitched = false;
            mapper = CameraFieldMapper((char*)"/home/baardew/bagadussii/baardew/shared/configurations/camera1.config",
                                       (char*)"/home/baardew/bagadussii/baardew/shared/configurations/field1.config");
            background = imread("/home/baardew/bagadussii/baardew/shared/configurations/background1.config", CV_LOAD_IMAGE_COLOR);
        }

        // If difference is greater than one second then it is a new sequence
        if (Utility::isNewSequence(player, prevPlayer, useSequenceNumbering)) {
            sequenceCounter++;
            trackPlayerId = getNextTrackPlayer();
        }

        prevPlayer = player;

        // If next is a new sequence, then the last frame is not to be exported (otherwise empty frames are exported).
        if (Utility::isNewSequence(resources.peakPlayer(), prevPlayer, useSequenceNumbering)) {
            delete bbextractor;
            bbextractor = NULL;
            firstFrame = true;

            Utility::flushSequence(sequence, sequenceIndex, serArchive, *bbextractor, boundingBoxSize);
            continue;
        }

        if (!resources.getVideo(framedata)) {
            printf("\nStop on: Mo more video frames\n");
            break;
        }

        if (bbextractor == NULL)
            bbextractor = new BoundingBoxExtractor(boundingBoxSize, resources.getWidth(), resources.getHeight(), background);

        fprintf(stderr, "\r%d/%d (%.2f%%) [%s]",
                progressCounter+1, iterationLimit, ((float)(progressCounter+1) / iterationLimit) * 100, player.timecode.c_str());

        mapper.transformPlayer(player);
        if (!insideFrame(player, resources.getWidth(), resources.getHeight()))
            continue;

        // Create RGB image
        Mat mRgb;
        Mat mYuv(resources.getHeight() + resources.getHeight() / 2, resources.getWidth(), CV_8UC1, framedata);
        cvtColor(mYuv, mRgb, COLOR_YUV2RGB_YV12);

        // Do not crop stuff outside video frame
        if (boundingBoxInsideFrame(player, resources, boundingBoxSize)) {
            struct FrameSequenceElement elm = bbextractor->computeFrameSequenceElement(player, bbextractor->YUVtoMat(framedata));

            // The first frame of a sequence is always garbage, both due to optical flow and bg-subtraction.
            // The frame is therefore not exported, but kept for optical flow computation.
            if (firstFrame) {
                firstFrame = false;
                bbextractor->freeTracking();
                continue;
            }

            sequence.push_back(elm);
            sequenceIndex.push_back(progressCounter);

#           ifdef DEBUG
            struct PlayerPosition trackerPosition = player;
#           endif

            // Adjust player position according to bbextractor result
            player.x = elm.normalizedRoi.x + elm.normalizedRoi.width / 2;
            player.y = elm.normalizedRoi.y + elm.normalizedRoi.height / 2;

            // Find bounding box according to tracker data
            int start_x = player.x - boundingBoxSize;
            int start_y = player.y - boundingBoxSize;
            int end_x = player.x + boundingBoxSize;
            int end_y = player.y + boundingBoxSize;
            if (start_x < 0) {
                end_x += abs(start_x);
                start_x = 0;
            }
            if (start_y < 0) {
                end_y += abs(start_y);
                start_y = 0;
            }
            if (end_x >= resources.getWidth()) {
                int diff = end_x - resources.getWidth();
                start_x -= diff * 2 - 1;
                end_x -= diff;
            }
            if (end_y >= resources.getWidth()) {
                int diff = end_y - resources.getHeight();
                start_y -= diff * 2 - 1;
                end_y -= diff;
            }
            if (end_x - start_x > boundingBoxSize * 2)
                end_x -= (end_x - start_x) - (boundingBoxSize * 2);
            if (end_y - start_y > boundingBoxSize * 2)
                end_y -= (end_y - start_y) - (boundingBoxSize * 2);

#ifndef DEBUG
            // Generate image and SQL
            Rect roi(start_x, start_y, end_x - start_x, end_y - start_y);
            Mat resized_image = mRgb(roi);

            char name[50];

            // Write image
            sprintf(name, "image_export/%s-%06d.png", namePrefix, progressCounter);
            sql.write(player, name, sequenceCounter, elm.roi);

            if (!imwrite(name, resized_image, compression_params)) {
                fprintf(stderr, "Write image failed\n");
                break;
            }
#else
            // Optional printing of homography mapping (render on top of input image)
            //	printPerspecitve0(mRgb, homography);
            //	printPerspecitve1(mRgb, homography);
            //	printPerspecitve2(mRgb, homography);

            // Image export and center
//            cvtColor(elm.mask, mRgb, COLOR_GRAY2RGB);

            // Find bounding box according to tracker data
            int bb_start_x = trackerPosition.x - boundingBoxSize;
            int bb_start_y = trackerPosition.y - boundingBoxSize;
            int bb_end_x = trackerPosition.x + boundingBoxSize;
            int bb_end_y = trackerPosition.y + boundingBoxSize;
            bb_start_x = bb_start_x < 0 ? 0 : bb_start_x;
            bb_start_y = bb_start_y < 0 ? 0 : bb_start_y;
            bb_end_x = bb_end_x >= resources.getWidth() ? bb_end_x - (bb_end_x - resources.getWidth()) : bb_end_x;
            bb_end_y = bb_end_y >= resources.getHeight() ? bb_end_y - (bb_end_y - resources.getHeight()) : bb_end_y;

            // ZXY Box
            rectangle(mRgb, Point(bb_start_x, bb_start_y), Point(bb_end_x, bb_end_y), Scalar(0, 0, 255));
            rectangle(mRgb, Point(trackerPosition.x - 2, trackerPosition.y - 2), Point(trackerPosition.x + 2, trackerPosition.y + 2), Scalar(0, 255, 0));

            // Tracker Box
            rectangle(mRgb, Point(start_x, start_y), Point(end_x, end_y), Scalar(128, 128, 255));
            rectangle(mRgb, Point(player.x - 2, player.y - 2), Point(player.x + 2, player.y + 2), Scalar(0, 128, 0));

            // Roi Box
            rectangle(mRgb, Point(elm.roi.x, elm.roi.y), Point(elm.roi.x + elm.roi.width, elm.roi.y + elm.roi.height), Scalar(128, 255, 128));
            rectangle(mRgb,
                      Point(elm.roi.x + (elm.roi.width / 2) - 2, elm.roi.y + (elm.roi.height / 2) - 2),
                      Point(elm.roi.x + (elm.roi.width / 2) + 2, elm.roi.y + (elm.roi.height / 2) + 2),
                      Scalar(128, 0, 0));

            // Optical flow usage boundaries
            rectangle(mRgb,
                      Point(elm.normalizedRoi.x, elm.normalizedRoi.y),
                      Point(elm.normalizedRoi.x + elm.normalizedRoi.width, elm.normalizedRoi.y + elm.normalizedRoi.height),
                      Scalar(255, 0, 0));

            // Text
            rectangle(mRgb, Point(0, 0), Point(490, 25), Scalar(255, 255, 255), CV_FILLED);
            putText(mRgb, player.timecode + " seq= " + to_string(sequenceCounter), Point(5, 20), FONT_HERSHEY_PLAIN, 1.5, Scalar(0, 100, 200), 2);

            // Enable this to export bounding box mask in full-frame
            /*
              mRgb = Mat(mRgb.size(), CV_8UC1, Scalar(0));
              int boundCenterX = elm.normalizedRoi.x + (elm.normalizedRoi.width / 2);
              int boundCenterY = elm.normalizedRoi.y + (elm.normalizedRoi.height / 2);
              rectangle(mRgb,
              Point(boundCenterX - boundingBoxSize, boundCenterY - boundingBoxSize),
              Point(boundCenterX + boundingBoxSize, boundCenterY + boundingBoxSize),
              Scalar(255),
              CV_FILLED);
            */

            char name[50];
            sprintf(name, "tracking_images/%06d-%d.png", progressCounter, trackPlayerId);

            if (!imwrite(name, mRgb, compression_params)) {
                fprintf(stderr, "Write debug image failed\n");
                break;
            }
#endif
        }

        // Done with this frame. Before continuing with the next iteration skip some frames
        if (!resources.skipFrames(framesToSkip)) {
            printf("\nStop on: Mo more video frames (skip video)\n");
            break;
        }
    }

    if (!useSequenceNumbering)
        Utility::flushSequence(sequence, sequenceIndex, serArchive, *bbextractor, boundingBoxSize);

    timetaker_stop(&stop);

    long sec, usec;
    timetaker_time(&start, &stop, &sec, &usec);

    printf("\n\n==============================================================================\n"
           "| Extracted %10d frames and %2d sequences in %3ld:%02ld.%06ld or %6.2ffps |\n"
           "==============================================================================\n\n",
           progressCounter, sequenceCounter + 1, sec / 60, sec % 60, usec, (float)progressCounter / sec);

    Utility::deleteZLibArchiver(serArchive);
    delete bbextractor;
    free(framedata);

    return EXIT_SUCCESS;
}
