#include "Utility.hpp"

using namespace cv;
using namespace std;
using namespace boost::archive;
using namespace boost::iostreams;

vector<int> Utility::createPNGCompression()
{
    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
    return compression_params;
}

struct Utility::ZLibArchiver Utility::createZLibArchiver(char *filename)
{
    ZLibArchiver arch;

    string serializedFilename = string(filename);
    serializedFilename.append(".ser.zlib");

    fprintf(stderr, "Created Optical Flow Serialized File: '%s'\n", serializedFilename.c_str());

    arch.ofs = new ofstream(serializedFilename, ofstream::binary | ofstream::out);

    arch.dataFile = new filtering_streambuf<output>;
    arch.dataFile->push(zlib_compressor());
    arch.dataFile->push(*(arch.ofs));

    arch.outStream = new binary_oarchive(*(arch.dataFile));

    return arch;
}

void Utility::deleteZLibArchiver(struct Utility::ZLibArchiver &arch)
{
    delete arch.outStream;
    delete arch.dataFile;
    delete arch.ofs;
}

void Utility::checkProgramArguments(int argc, char **argv)
{
    printf("\nProvided Arguments:\n");
    for (int i = 0; i < argc; i++)
        printf("\t[%d] = %s\n", i, argv[i]);
    printf("\n\n");

    if (argc < 12) {
        printf("Usage: %s <csv> <clipDir> <clipFile> <cam.conf> <field.conf> <playerId> <frameSkip> <iterLimit> <boundingSize> <namePrefix> <background.png> [# (enumerate sequences)]\n", argv[0]);
        exit(EXIT_SUCCESS);
    }
}

bool Utility::isNewSequence(struct PlayerPosition cur, struct PlayerPosition prev, bool useSequences)
{
    if (!useSequences)
        return false;

    return abs(SimpleCSVReader::stringToTime(cur.timecode) - SimpleCSVReader::stringToTime(prev.timecode)) > ONE_SECOND;
}

void Utility::flushSequence(vector<FrameSequenceElement> &sequence,
                            vector<int> &index,
                            struct Utility::ZLibArchiver &arch,
                            BoundingBoxExtractor &bbe,
                            int boundingBoxSize)
{
    fprintf(stderr, "Normalizing sequence for optical flow...");
    bbe.zoomScaleSequence(sequence);
    fprintf(stderr, "  DONE!\nComputing optical flow and exporting...");

    OpticalFlow opticalFlow;
    for (uint i = 1; i < sequence.size(); i++) {
        /*
         * EXPORT:
         */
        Mat flowField(boundingBoxSize * 2, boundingBoxSize * 2, CV_8UC1);
        opticalFlow.computeDense(sequence[i-1].normalized, sequence[i].normalized, flowField);
        /*
         */

        /*
         * NORMALIZED:
         Mat flowField(boundingBoxSize * 2, boundingBoxSize * 2, CV_8UC1);
         opticalFlow.computeDense(sequence[i-1].normalized, sequence[i].normalized, flowField);

         Mat vectors = opticalFlow.drawOptFlowVector(flowField, sequence[i].normalized);
         namedWindow("Vectors", 0);
         imshow("Vectors", vectors);
         waitKey(0);
        */

        /*
         * FULL:
         Mat flowField(sequence[i].frame.rows, sequence[i].frame.cols, CV_8UC1);
         Mat convertedImageA(sequence[i].frame.rows, sequence[i].frame.cols, CV_8UC1);
         Mat convertedImageB(sequence[i].frame.rows, sequence[i].frame.cols, CV_8UC1);
         cvtColor(sequence[i-1].frame, convertedImageA, CV_RGB2GRAY);
         cvtColor(sequence[i].frame, convertedImageB, CV_RGB2GRAY);

         opticalFlow.computeDense(convertedImageA, convertedImageB, flowField);

         Mat convertedImage(sequence[i].frame.rows, sequence[i].frame.cols, CV_8UC1);
         cvtColor(sequence[i].frame, convertedImage, CV_RGB2GRAY);

         Mat vectors = opticalFlow.drawOptFlowVector(flowField, convertedImage);

         sequence[i].normalizedRoi.x += 50;
         sequence[i].normalizedRoi.y += 50;
         sequence[i].normalizedRoi.width -= 100;
         sequence[i].normalizedRoi.height -= 100;

         sequence[i].normalizedRoi.x *= 4;
         sequence[i].normalizedRoi.y *= 4;
         sequence[i].normalizedRoi.width *= 4;
         sequence[i].normalizedRoi.height *= 4;

         namedWindow("Vectors", 0);
         imshow("Vectors", vectors(sequence[i].normalizedRoi));
         waitKey(0);
        */

        /*
         * CROP:
         Mat flowField(sequence[i].normalizedRoi.height, sequence[i].normalizedRoi.width, CV_8UC1);
         Mat convertedImageA(sequence[i].normalizedRoi.height, sequence[i].normalizedRoi.width, CV_8UC1);
         Mat convertedImageB(sequence[i].normalizedRoi.height, sequence[i].normalizedRoi.width, CV_8UC1);
         cvtColor(sequence[i-1].frame(sequence[i-1].normalizedRoi), convertedImageA, CV_RGB2GRAY);
         cvtColor(sequence[i].frame(sequence[i].normalizedRoi), convertedImageB, CV_RGB2GRAY);

         opticalFlow.computeDense(convertedImageA, convertedImageB, flowField);

         Mat convertedImage(sequence[i].normalizedRoi.height, sequence[i].normalizedRoi.width, CV_8UC1);
         cvtColor(sequence[i].frame(sequence[i].normalizedRoi), convertedImage, CV_RGB2GRAY);

         Mat vectors = opticalFlow.drawOptFlowVector(flowField, convertedImage);
         namedWindow("Vectors", 0);
         imshow("Vectors", vectors(Rect(200, 200, 400, 400)));
         waitKey(0);
        */

        FrameMat fm;
        fm.frame = index[i];
        fm.heading = sequence[i].position.heading;
        fm.direction = sequence[i].position.direction;
        fm.speed = sequence[i].position.speed;
        fm.mat = flowField;

        *(arch.outStream) << fm;
    }

    fprintf(stderr, "   DONE!\n");

    sequence.clear();
    index.clear();
}
