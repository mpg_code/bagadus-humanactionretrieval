#include "SQLFileCreator.hpp"

using namespace std;
using namespace cv;

SQLFileCreator::SQLFileCreator(std::string filePrefix, int boundingSize) :
    boundingBoxSize(boundingSize * 2)
{
    out.open("sql/" + filePrefix + "_imageSetup.sql");
}

SQLFileCreator::~SQLFileCreator()
{
    out.close();
}

void SQLFileCreator::write(struct PlayerPosition &player, string filename, int sequenceNo, Rect boundingBox)
{
    out << "INSERT INTO trainingdata (sessionID, frameID, timecode, sequenceNo, frameX, frameY, width, height, bb_x1, bb_y1, bb_x2, bb_y2) VALUES (0, '"
        << filename << "', '"
        << player.timecode << "', '"
        << to_string(sequenceNo) << "', "
        << player.x << ", "
        << player.y << ", "
        << boundingBoxSize << ", "
        << boundingBoxSize << ", "
        << boundingBox.x << ", "
        << boundingBox.y << ", "
        << boundingBox.x + boundingBox.width << ", "
        << boundingBox.y + boundingBox.height << ");"
        << std::endl;
}
