<?php
function processArguments_calibrate() {
    global $calibrate_idx;
    global $log;

    if ($calibrate_idx === 0) {
        // index 0 is always performed in javascript
        return;
    }

    if ($log) {
        ChromePhp::log("processArguments_calibrate: verification process");
    }
}

function processArguments_train() {
    global $log;
    global $debug;
    global $disableMotion;

    // If a new image is requested, then processing is skipped
    $doSkip = 'false';
    if (isset($_COOKIE['skipImage']) && !$debug)
        $doSkip = $_COOKIE['skipImage'];

    if ($log) {
        ChromePhp::log("processArguments_train.doSkip=".$doSkip);
    }

    if ($doSkip === 'true') {
        return true;
    }

    if (!hasValidInput() && !$debug) {
        return false;
    }

    $isControlTest = isControl();
    if ($isControlTest === true) {
        $_SESSION['control_count']++;

        if ($debug) {
            echo 'IS CONTROL   ';
        }
    }

    if (imageAlreadyExists($isControlTest) === true && !$debug) {
        return true;
    }

    if ($debug) {
        echo 'INSERTING DATA  ';
    } else {
        insertImage($isControlTest);
    }

    return true;
}

function insertImage($isControlTest) {
    global $log;
    global $debug;
    global $disableMotion;

    if ($log) {
        ChromePhp::log("Inserting Data to SQL");
    }

    $players = 1;
    $motion = "disabled";

    if (!$disableMotion) {
        $motion = $_COOKIE['motion'];
        $players = $_COOKIE['players'];
    }

    if ($isControlTest) {
        $goldTestNumber = 0;
        $query = DB::query("SELECT MAX(goldTest) AS max FROM goldLog WHERE sessionID = %i AND frameID = %s",
                           $_SESSION['session_id'], $_COOKIE['image_name']);

        if ($log) {
            ChromePhp::log("goldTestNumber = ".$query[0]['max']." numeric = ".is_numeric($query[0]['max']));
        }

        if (is_numeric($query[0]['max'])) {
            $goldTestNumber = $query[0]['max'] + 1;
        }

        DB::insert('goldLog', array(
                       'sessionID' => $_SESSION['session_id'],
                       'frameID' => $_COOKIE['image_name'],
                       'goldTest' => $goldTestNumber,
                       'h_x' => $_COOKIE['h_x'],
                       'h_y' => $_COOKIE['h_y'],
                       's_lx' => $_COOKIE['s_lx'],
                       's_ly' => $_COOKIE['s_ly'],
                       's_rx' => $_COOKIE['s_rx'],
                       's_ry' => $_COOKIE['s_ry'],
                       'e_lx' => $_COOKIE['e_lx'],
                       'e_ly' => $_COOKIE['e_ly'],
                       'e_rx' => $_COOKIE['e_rx'],
                       'e_ry' => $_COOKIE['e_ry'],
                       'n_lx' => $_COOKIE['n_lx'],
                       'n_ly' => $_COOKIE['n_ly'],
                       'n_rx' => $_COOKIE['n_rx'],
                       'n_ry' => $_COOKIE['n_ry'],
                       'l_lx' => $_COOKIE['l_lx'],
                       'l_ly' => $_COOKIE['l_ly'],
                       'l_rx' => $_COOKIE['l_rx'],
                       'l_ry' => $_COOKIE['l_ry'],
                       'k_lx' => $_COOKIE['k_lx'],
                       'k_ly' => $_COOKIE['k_ly'],
                       'k_rx' => $_COOKIE['k_rx'],
                       'k_ry' => $_COOKIE['k_ry'],
                       'f_lx' => $_COOKIE['f_lx'],
                       'f_ly' => $_COOKIE['f_ly'],
                       'f_rx' => $_COOKIE['f_rx'],
                       'f_ry' => $_COOKIE['f_ry'],
                       'bb_x1' => $_COOKIE['bb_x1'],
                       'bb_y1' => $_COOKIE['bb_y1'],
                       'bb_x2' => $_COOKIE['bb_x2'],
                       'bb_y2' => $_COOKIE['bb_y2']
                       ));

        DB::update('trainingdata',
                   array('processed' => DB::sqleval('processed + 1')),
                   "frameID = %s AND sessionID = -1",
                   $_COOKIE['image_name']);
    } else {
        DB::insert('trainingdata', array(
                       'sessionID' => $_SESSION['session_id'],
                       'frameID' => $_COOKIE['image_name'],
                       'motion' => $motion,
                       'players' => $players,
                       'h_x' => $_COOKIE['h_x'],
                       'h_y' => $_COOKIE['h_y'],
                       's_lx' => $_COOKIE['s_lx'],
                       's_ly' => $_COOKIE['s_ly'],
                       's_rx' => $_COOKIE['s_rx'],
                       's_ry' => $_COOKIE['s_ry'],
                       'e_lx' => $_COOKIE['e_lx'],
                       'e_ly' => $_COOKIE['e_ly'],
                       'e_rx' => $_COOKIE['e_rx'],
                       'e_ry' => $_COOKIE['e_ry'],
                       'n_lx' => $_COOKIE['n_lx'],
                       'n_ly' => $_COOKIE['n_ly'],
                       'n_rx' => $_COOKIE['n_rx'],
                       'n_ry' => $_COOKIE['n_ry'],
                       'l_lx' => $_COOKIE['l_lx'],
                       'l_ly' => $_COOKIE['l_ly'],
                       'l_rx' => $_COOKIE['l_rx'],
                       'l_ry' => $_COOKIE['l_ry'],
                       'k_lx' => $_COOKIE['k_lx'],
                       'k_ly' => $_COOKIE['k_ly'],
                       'k_rx' => $_COOKIE['k_rx'],
                       'k_ry' => $_COOKIE['k_ry'],
                       'f_lx' => $_COOKIE['f_lx'],
                       'f_ly' => $_COOKIE['f_ly'],
                       'f_rx' => $_COOKIE['f_rx'],
                       'f_ry' => $_COOKIE['f_ry'],
                       'bb_x1' => $_COOKIE['bb_x1'],
                       'bb_y1' => $_COOKIE['bb_y1'],
                       'bb_x2' => $_COOKIE['bb_x2'],
                       'bb_y2' => $_COOKIE['bb_y2']
                       ));

        DB::update('trainingdata',
                   array('processed' => DB::sqleval('processed + 1')),
                   "frameID = %s AND sessionID = 0",
                   $_COOKIE['image_name']);
    }

    DB::update('crowdworker', array('endTime' => DB::sqleval('CURRENT_TIMESTAMP')),
               'sessionID = %i AND username = %s', $_SESSION['session_id'], $_SESSION['username']);

    $query = DB::query("SELECT sequenceNo FROM trainingdata WHERE frameID = %s AND sessionID = 0 LIMIT 1", $_COOKIE['image_name']);

    DB::update('trainingdata',
               array('sequenceNo' => $query[0]['sequenceNo']),
               'frameID = %s AND sessionID = %i',
               $_COOKIE['image_name'], $_SESSION['session_id']);

    if ($log) {
        ChromePhp::log("Inserting Data to SQL: COMPLETED");
    }
}

?>