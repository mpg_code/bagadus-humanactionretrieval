<?php
$calibrate_idx; // Current index for calibration
$training_idx;  // Current index for training

/**
 * Obtain and set worker username
 */
if (!isset($_SESSION['username'])) {
    if (!isset($_POST['username'])) {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=index.php">';
    } if (strlen($_POST['username']) <= 0) {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=index.php">';
    }

    $_SESSION['username'] = $_POST['username'];

    if ($log) {
        ChromePhp::log("Username: ".$_SESSION['username']);
    }

} else {
    if ($log) {
        ChromePhp::log("Username: ".$_SESSION['username']);
    }
}





/**
 *  Check that browser and cookies are enabled
 */
$have_cookie = false;
if ((isset($_GET['ccheck']) && $_GET['ccheck'] == true)
    || (isset($_COOKIE['cookie_check']) && $_COOKIE['cookie_check'] == 'existence')) {

    // Double test, but only if ccheck is set and not cookie
    if (isset($_COOKIE['cookie_check']) && $_COOKIE['cookie_check'] == 'existence') {
        $have_cookie = true;
    }
} else {
    // Dont have a cookie, test if possible
    $_SESSION['page'] = $_SERVER['HTTP_REFERER'];
    setcookie('cookie_check', 'existence', time() + 3600);
    header("location: {$_SERVER['PHP_SELF']}?ccheck=true");
    exit;
}

if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') === false || !$have_cookie) {
    echo 'Your browser is not Google Chrome and/or cookies are not enabled.';
    exit;
}





/**
 * Create a session ID if none exists, or use least worked sessionID
 */
$sessID = -1;
if (!isset($_SESSION['session_id']) || !is_numeric($_SESSION['session_id'])) {
    // Use existing session number if exists
    $query = DB::query("SELECT c.sessionID, COUNT(*) AS images FROM crowdworker c, trainingdata t ".
                       "WHERE c.sessionID = t.sessionID AND username = %s ".
                       "GROUP BY sessionID ORDER BY images ASC", $_SESSION['username']);

    if (count($query) != 0 && $query[0]['images'] != $limit_training) {
        $sessID = $query[0]['sessionID'];
    }

    // Get a new session number
    while ($sessID == -1) {
        $sessID = mt_rand(1000000000, 2147483647);
        $query = DB::query("SELECT sessionid FROM trainingdata where sessionid = %i", $sessID);

        if (count($query) != 0) {
            $sessID = -1;
        }
    }

    $_SESSION['session_id'] = $sessID;
    $_SESSION['control_count'] = 0;

    if ($debug && isset($_SERVER['HTTP_REFERER'])) {
        echo 'SERVER='.getServerName().' PREV='.$_SERVER['HTTP_REFERER'].'   ';
    }

    // Display welcome page if not arrived from it.
    // Used as a feature to not double-display.
    // Skip if it is a new session request
    if (isset($_SERVER['HTTP_REFERER'])
        && strcmp(getServerName(), $_SERVER['HTTP_REFERER']) != 0
        && strcmp(getServerName().'index.php', $_SERVER['HTTP_REFERER']) != 0
        && strcmp(getServerName().'resetsession.php', $_SERVER['HTTP_REFERER']) != 0)
    {
        if ($debug) {
            echo 'GOTO: Welcome';
        } else {
            echo '<META HTTP-EQUIV="Refresh" Content="0; url=index.php">';
        }

        exit;
    }
}




/**
 * Check that this worker has completed its work
 */
if (filledQuota()) {
    if ($debug) {
        echo 'FULL QUOTA';
    } else {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=finished.php">';
    }

    exit;
}




/**
 * Retrieve training/calibrating index counters
 */
if (!isset($_COOKIE['training_idx']) || !isset($_SESSION['session_id'])) {
    $training_idx = 0;
    setcookie("training_idx", 0);
} else {
    $training_idx = $_COOKIE['training_idx'];
    if ($training_idx === 'NaN') {
        setcookie("calibrate_idx", 0);
        setcookie("training_idx", 0);
        $training_idx = 0;
    } else {
        $training_idx = intval($training_idx);
    }
}

if (!isset($_COOKIE['calibrate_idx']) || !isset($_SESSION['session_id'])) {
    $calibrate_idx = 0;
    setcookie("calibrate_idx", 0);
} else {
    $calibrate_idx = $_COOKIE['calibrate_idx'];
    if ($calibrate_idx === 'NaN') {
        $calibrate_idx = 0;
        setcookie("calibrate_idx", 0);
    } else {
        $calibrate_idx = intval($calibrate_idx);
    }
}

if ($log) {
    ChromePhp::log("Calibrate=".$calibrate_idx.", Training=".$training_idx);
}


/**
 * Register new user if not existing
 */
DB::insertIgnore('crowdworker',
                 array('sessionId' => $_SESSION['session_id'],
                       'username' => $_SESSION['username'],
                       'startTime' => DB::sqleval('CURRENT_TIMESTAMP'),
                       'goldErrors' => DB::sqleval('NULL')));




/**
 * Check that there is more work to be done
 */
if ($training_times > 0) {
    $query = DB::query("SELECT COUNT(*) AS frames FROM trainingdata WHERE processed < %i AND sessionid != %i",
                       $training_times, $sessID);
    if ($query[0]['frames'] == 0) {
        if ($debug) {
            echo 'GOTO: Finished';
        } else {
            echo '<META HTTP-EQUIV="Refresh" Content="0; url=finished.php">';
        }

        exit;
    }
}





/**
 * Hand out a sequence to be trained (or keep current if already provided).
 */
provideSequence();





/**
 * Process calibration (if applicable)
 * The counters first counts 'calibrate' from 0 to max,
 * then 'training' from 0 to max.
 */
if ($calibrate_idx < $calibrate_max) {

    // Can only get here if counter is reset but training has been perfomed.
    // Reset the other counter when done
    if ($training_idx > 0) {
        trainingVerificationAndRegistration();
        setcookie("training_idx", 0);

        // Check that this worker has completed its work:
        // it might be after registering the last image
        if (filledQuota()) {
            if ($debug) {
                echo 'FULL QUOTA';
            } else {
                echo '<META HTTP-EQUIV="Refresh" Content="0; url=finished.php">';
            }
        }

        // Refresh page to get to the next step
        echo '<META HTTP-EQUIV="Refresh" Content="0">';
        exit;
    }

    if ($calibrate_idx > 0) {
        processArguments_calibrate();
    }

    if ($log) {
        ChromePhp::log("calibrate idx = ".$calibrate_idx);
    }

    // Special defined for index 0; javascript mouse calibration
    if ($calibrate_idx === 0) {
        if ($log) {
            ChromePhp::log("calibrate mouse");
        }

        setcookie("image_name", "NULL");
        setcookie("image_idx", "c".$calibrate_idx);
        setcookie("calibrate_idx", $calibrate_idx);
    } else {
        if ($log) {
            ChromePhp::log("calibrate ground truth");
        }

        // Location for possible additional calibration steps
    }

    if ($debug) {
        echo 'GOTO: Calibrate: '.$calibrate_idx;
        setcookie("calibrate_idx", $calibrate_idx+1);
    } else {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=presentation.html">';
    }

    exit;
}

if ($training_idx < ($training_max + $control_times)) {
    if ($training_idx == 0) {
        processArguments_calibrate();
    }

    if ($training_idx > 0) {
        trainingVerificationAndRegistration();
    }

    provideSequence();
    $imageMeta = getImage();
    setcookie("image_name", $imageMeta['frameID']);
    setcookie("image_idx", $training_idx);
    setcookie("image_width", $imageMeta['width']);
    setcookie("image_height", $imageMeta['height']);

    setcookie("training_idx", $training_idx);

    /**
     * Check that this worker has completed its work
     */
    if (filledQuota()) {
        if ($debug) {
            echo 'FULL QUOTA';
            setcookie("training_idx", $training_idx+1);
        } else {
            echo '<META HTTP-EQUIV="Refresh" Content="0; url=finished.php">';
        }

        exit;
    }


    if ($debug) {
        echo 'GOTO: Training: '.$training_idx;
        setcookie("training_idx", $training_idx+1);
    } else {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=presentation.html">';
    }

    exit;
}




/**
 * Maxed out the counters, reset and perform recalibration tasks.
 * Then simply refresh the page or go to finished if completed the work
 */
setcookie("calibrate_idx", 0);
$_SESSION['control_count'] = 0;

if (filledQuota()) {
    if ($debug) {
        echo 'FULL QUOTA';
    } else {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=finished.php">';
    }

    exit;
}

if ($debug) {
    echo 'RESET';
} else {
    echo '<META HTTP-EQUIV="Refresh" Content="0">';
}

exit;



/**
 * Helper functions
 */
function getImage() {
    global $training_idx;
    global $training_times;
    global $debug;

    $query;
    if (doControl()) {
        if ($debug) {
            echo "Provided gold sample   ";
        }

        $query = DB::query("SELECT frameID, width, height FROM trainingdata WHERE sessionID = -1 AND sequenceNo = %i ".
                           "AND frameID NOT IN (SELECT frameID FROM goldLog WHERE sessionID = %i) ORDER BY processed, RAND() LIMIT 1",
                           $_SESSION['sequence_no'], $_SESSION['session_id']);
    } else {
        if ($debug) {
            echo "Provided normal sample   ";
        }

        if ($training_times === 0) {
            $query = DB::query("SELECT frameID, width, height FROM trainingdata WHERE sessionID = 0 AND sequenceNo = %i ".
                               "AND frameID NOT IN (SELECT frameID FROM trainingdata WHERE sessionID = %i) ORDER BY processed, RAND() LIMIT 1",
                               $_SESSION['sequence_no'], $_SESSION['session_id']);
        } else {
            $query = DB::query("SELECT frameID, width, height FROM trainingdata WHERE processed < %i AND sessionID = 0 AND sequenceNo = %i ".
                               "AND frameID NOT IN (SELECT frameID FROM trainingdata WHERE sessionID = %i) ORDER BY processed, RAND() LIMIT 1",
                               $training_times, $_SESSION['sequence_no'], $_SESSION['session_id']);
        }
    }

    if (count($query) === 0) {
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=finished.php">';
        exit;
    }

    return $query[0];
}

function getServerName() {
    $serverURL = 'http';

    $serverURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $serverURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
    } else {
        $serverURL .= $_SERVER["SERVER_NAME"];
    }

    $serverURL .= "/";
    return $serverURL;
}

function trainingVerificationAndRegistration() {
    global $log;

    if (!processArguments_train()) {
        if ($log) {
            ChromePhp::log("Invalid training input");
        }

        removeWorker();
        echo '<META HTTP-EQUIV="Refresh" Content="0; url=inprecise.html">';
        exit;
    }
}
?>