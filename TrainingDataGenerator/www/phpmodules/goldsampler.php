<?php
function doControl() {
    global $log;

    global $training_idx;
    global $training_max;
    global $control_times;
    $control = $_SESSION['control_count'];
    $idx = $training_idx + 1;

    if ($log) {
        ChromePhp::log("doControl() :: control = ".$control." idx = ".$idx);
    }

    if ($control_times == 0 || $control >= $control_times || $control_times <= 0 || $training_idx > $training_max) {
        if ($log) {
            ChromePhp::log("doControlTest = false");
        }

        return false;
    }

    $interval = intval($training_max / $control_times);
    $expected = intval(ceil($idx / $interval));

    if ($control + 1 > $expected) {
        if ($log) {
            ChromePhp::log("doControlTest = false");
        }

        return false;
    }

    if ($control < $expected && $idx % $interval == 0) {
        if ($log) {
            ChromePhp::log("doControlTest = true");
        }

        return true;
    }

    $doControlTest = (rand() % $training_max) === 0;

    if ($log) {
        ChromePhp::log("doControlTest = ".($doControlTest === true ? "true" : "false"));
    }

    return $doControlTest;
}

function isControl() {
    global $debug;
    global $log;

    $query = DB::query("SELECT control FROM trainingdata where frameID = %s AND sessionID = -1", $_COOKIE['image_name']);
    $isControlFrame = count($query) != 0;

    if ($debug) {
        echo "checking if control image '".$_COOKIE['image_name']."' = ".($isControlFrame == true ? 'true' : 'false')."   ";
    }

    if ($log) {
        ChromePhp::log("checking if control image '".$_COOKIE['image_name']."' = ".($isControlFrame == true ? 'true' : 'false'));
    }

    return $isControlFrame;
}
?>