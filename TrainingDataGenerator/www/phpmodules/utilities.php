<?php
$sqlJoints = array('h_x', 'h_y',
                   's_lx', 's_ly', 's_rx', 's_ry',
                   'e_lx', 'e_ly', 'e_rx', 'e_ry',
                   'n_lx', 'n_ly', 'n_rx', 'n_ry',
                   'l_lx', 'l_ly', 'l_rx', 'l_ry',
                   'k_lx', 'k_ly', 'k_rx', 'k_ry',
                   'f_lx', 'f_ly', 'f_rx', 'f_ry',
                   'bb_x1', 'bb_y1', 'bb_x2', 'bb_y2'); // bb_* technically not a joint, but same (x, y) value pair


// Array prefixes equivalent to javascript
$sqlJointsPrefix = array('h_',
                         's_l', 's_r',
                         'e_l', 'e_r',
                         'n_l', 'n_r',
                         'l_l', 'l_r',
                         'k_l', 'k_r',
                         'f_l', 'f_r');


function imageAlreadyExists($isControlTest) {
    if (!isset($_SESSION['session_id']) || !isset($_COOKIE['image_name'])) {
        throw new Exception('SQL present error isset');
    }

    // If, for some reason, the data pair is already present, ignore the insert
    if ($isControlTest) {
        return false; // control images allow duplicates due to limited gold sample set
    } else {
        $query = DB::query("SELECT sessionID FROM trainingdata where sessionID = %i AND frameID = %s",
                           $_SESSION['session_id'], $_COOKIE['image_name']);
    }

    return count($query) != 0;
}

function provideSequence() {
    global $log;

    if (!isset($_SESSION['sequence_no'])) {
        $_SESSION['sequence_no'] = getLeastWorkedSequence();

        if ($log) {
            ChromePhp::log("Distributed sequence: ".$_SESSION['sequence_no']);
        }

    } else {
        $query = DB::query("SELECT (sequence.frames - COUNT(*)) AS frames FROM trainingdata, ".
                           "(SELECT COUNT(*) AS frames FROM trainingdata WHERE sessionID = 0 AND sequenceNo = %i) AS sequence WHERE sequenceNo = %i AND sessionID = %i",
                           $_SESSION['sequence_no'], $_SESSION['sequence_no'], $_SESSION['session_id']);

        if (!is_numeric($_SESSION['sequence_no']) || $query[0]['frames'] == 0) {
            $_SESSION['sequence_no'] = getLeastWorkedSequence();
        }

        if ($log) {
            ChromePhp::log("Distributed sequence: ".$_SESSION['sequence_no']);
        }
    }
}

function moreFramesAvailable() {
    global $training_times;

     if ($training_times === 0)
         return true;

     $query = DB::query("SELECT frameID, width, height FROM trainingdata WHERE processed < %i AND sessionID = 0;", $training_times);

     return count($query) > 0;
}

function hasValidInput() {
    global $sqlJoints;
    global $disableMotion;

    if (!isset($_SESSION['session_id']) || !isset($_COOKIE['image_name'])) {
        return false;
    }

    if (!is_numeric($_SESSION['session_id']) || !is_string($_COOKIE['image_name'])) {
        return false;
    }

    foreach ($sqlJoints as $column) {
        if (!isset($_COOKIE[$column])) {
            return false;
        }

        if (!is_numeric($_COOKIE[$column])) {
            return false;
        }
    }

    if (!$disableMotion) {
        if (!isset($_COOKIE['motion']) || !isset($_COOKIE['players'])) {
            return false;
        }

        if (!is_string($_COOKIE['motion']) || !is_numeric($_COOKIE['players'])) {
            return false;
        }
    }

    return true;
}

function removeWorker() {
    // MySQL cannot update and select from same table; requires two steps to update processed counters

    $query = DB::query("SELECT frameID AS image_name FROM trainingdata WHERE sessionID = %i", $_SESSION['session_id']);

    foreach ($query as $row) {
        DB::update('trainingdata', array('processed' => DB::sqleval('processed - 1')), 'frameID = %s', $row['image_name']);
    }

    DB::delete('trainingdata', 'sessionID = %i', $_SESSION['session_id']);
}

function filledQuota() {
    global $limit_training;

    if (!isset($_SESSION['username'])) {
        return false;
    }

    $query = DB::query("SELECT count(*) AS count FROM crowdworker c, trainingdata t WHERE c.sessionID = t.sessionID AND c.username = %s AND c.sessionID = %i",
                       $_SESSION['username'], $_SESSION['session_id']);

    return $query[0]['count'] >= $limit_training;
}

function stringToSequence($frameid) {
    // We need the second last number, which is the sequence number
    preg_match_all('/\d+/', $frameid, $matches);
    $offset = count($matches[0]) - 2;

    if ($offset < 0) {
        return "";
    }

    return intval($matches[0][$offset]);
}

function stringToIndex($frameid)
{
    // We need the last number, which is the image number
    preg_match_all('/\d+/', $frameid, $matches);
    $offset = count($matches[0]) - 1;

    if ($offset < 0) {
        return "";
    }

    return intval($matches[0][$offset]);
}

function getLeastWorkedSequence()
{
    $query = DB::query("SELECT sequenceNo, SUM(trainedframes)/COUNT(sequenceno) AS workload FROM sequenceframes GROUP BY sequenceno ORDER BY workload, RAND()");
    return $query[0]['sequenceNo'];
}
?>
