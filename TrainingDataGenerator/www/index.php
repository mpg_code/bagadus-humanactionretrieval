<?php

session_start();

echo "<body>".PHP_EOL."<div align=\"center\">".PHP_EOL;
require('manual.html');

$workername = "";
$itemRule = "required";
if (isset($_SESSION['username'])) {
    $workername = $_SESSION['username'];
    $itemRule = "disabled";
}

echo
"<p id=\"errorMessage\">".PHP_EOL
."<form action=\"server.php\" method=\"post\">".PHP_EOL
."<label style=\"display:block;margin-top:20px;letter-spacing:2px;\">Enter Microworkers Username and click continue</label>".PHP_EOL
."<input name=\"username\" value=\"".$workername."\" maxlength=\"64\"  ".$itemRule."/>".PHP_EOL
."<br/>".PHP_EOL
."<br/>".PHP_EOL
."<input type=\"submit\" name=\"submit\" value=\"Continue\"/>".PHP_EOL
."</form>".PHP_EOL
."</p>";

echo
"</div>".PHP_EOL
."<script type=\"text/javascript\">".PHP_EOL
."var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;".PHP_EOL
."var cookies_enabled = navigator.cookieEnabled;".PHP_EOL
."if (!is_chrome || !cookies_enabled) {".PHP_EOL
."document.getElementById(\"errorMessage\").innerHTML = \"<b>You must have cookies (for client-server communication) enabled and use Google Chrome as web broser (to have all required features)</b>\";".PHP_EOL
."}".PHP_EOL
."</script>".PHP_EOL
."</body>";

?>