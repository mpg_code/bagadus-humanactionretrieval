<?php
session_start();

require('phpconfig.php');

if ($multiSessions) {
    unset($_SESSION['session_id']);
    unset($_SESSION['sequence_no']);
    setcookie("calibrate_idx", 0);
    setcookie("training_idx", 0);
    setcookie("skipImage", true);
    echo '<META HTTP-EQUIV="Refresh" Content="0; url=server.php">';
}
?>