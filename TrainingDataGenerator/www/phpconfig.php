<?php
require('phpmodules/meekrodb.2.3.class.php');

$imageServer;

$debug         = false; // Debug runs this file without inserting any data or jumping to other pages
$log           = true; // Enable logging to console (Requires Chrome plugin)
$useTestServer = true; // When testing, use local server
$disableMotion = true; // Enable or disable labeling of motion
$multiSessions = true; // Enable workers to do more than one run

$calibrate_max  =  1; // Number of calibration images, first is always the mouse calibration step
$training_max   = 12; // Number of training images before new calibration round (actual $training_max + $control_times)
$training_times =  1; // Number of times a frame is to be trained, 0 = infinate
$control_times  =  0; // Number of control images per $training_max, 0 is possible
$limit_training = 24; // Maximum number of training images per user (username, not sessionID), can't be larger than number of images in database

if ($log) {
    include('phpmodules/ChromePhp.php');
}

// Establish a DB connection
if ($useTestServer) {
    $imageServer = "http://localhost/images/";

    DB::$user = 'root';
    DB::$password = 'roMIC#99@nokLITT';
    DB::$dbName = 'WintherProduksjoner';
    DB::$host = 'localhost';
    DB::$port = '3306';
    DB::$encoding = 'utf8';
    DB::$error_handler = false; // since we're catching errors, don't need error handler
    DB::$throw_exception_on_error = true;

    set_error_handler("errorHandlerDebug");
} else {
    $imageServer = "http://mat.ndlab.net/baardew/images/";

    DB::$user = 'baardew';
    DB::$password = 'eb9Eemei';
    DB::$dbName = 'baardew';
    DB::$host = 'localhost';
    DB::$port = '3306';
    DB::$encoding = 'utf8';

    set_error_handler("errorHandler");
}

function errorHandler($errno, $errmsg) {
    clearAllCookies();
    echo 'Something broke. Please try again later.';
    exit;
}

function errorHandlerDebug($errno, $errmsg, $errfile, $errline) {
    echo '<b>Error:</b> '.$errno.'<br/>'
        .'<b>In file:</b> '.$errfile.' <b>line</b> '.$errline.'<br/>'
        .'<b>DETAILS:</b> '.$errmsg;
    exit;
}

function clearAllCookies() {
    if (isset($_SERVER['HTTP_COOKIE'])) {
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
        foreach ($cookies as $cookie) {
            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time() - 1000);
            setcookie($name, '', time() - 1000, '/');
        }
    }
}

?>