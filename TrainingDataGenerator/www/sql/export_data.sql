USE baardew;

/**
 * Updating with non-primary key values is unsafe, enable unsafe update:
 */
SET SQL_SAFE_UPDATES = 0;

/**
 * Push missing information from initial entries to the plotted entries.
 */
UPDATE trainingdata A
JOIN trainingdata B ON A.frameID = B.frameID
SET A.motion = B.motion,
    A.timecode = B.timecode,
    A.frameX = B.frameX,
    A.frameY = B.frameY,
    A.width = B.width,
    A.height = B.height,
    A.sequenceNo = B.sequenceNo
WHERE A.sessionID != 0 AND B.sessionID = 0;

/**
 * Update initial goldError results to filter out non-completed work
 *
 *  REMEMBER TO UPDATE THE TRESHOLD VALUE '24' TO SESSION TRAINING_MAX VALUE
 */
 /*
UPDATE crowdworker c SET goldErrors = 100 WHERE (SELECT COUNT(*) FROM trainingdata t WHERE t.sessionid = c.sessionid) < 24;
 */
/**
 * Full query of items to ensure correct order. Retrives the trained data.
 */
SELECT t.sessionID, concat("\"", timecode, "\"") as timecode, concat("\"", frameID, "\"") as frameid, frameX, frameY, width, height, players, control, processed, concat("\"", motion, "\"") as motion, sequenceNo, bb_x1, bb_y1, bb_x2, bb_y2, h_x, h_y, s_lx, s_ly, s_rx, s_ry, e_lx, e_ly, e_rx, e_ry, n_lx, n_ly, n_rx, n_ry, l_lx, l_ly, l_rx, l_ry, k_lx, k_ly, k_rx, k_ry, f_lx, f_ly, f_rx, f_ry
FROM trainingdata t, crowdworker c
WHERE t.sessionID != 0 AND t.sessionID != -1
      AND t.sessionID = c.sessionID
ORDER BY frameID;
