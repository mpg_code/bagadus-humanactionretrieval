USE baardew;

/**
 * Updating with non-primary key values is unsafe, enable unsafe update:
 */
SET SQL_SAFE_UPDATES = 0;

/**
 * Full query of items to ensure correct order. Retrives the trained data.
 * To preserve compatebility with the SQLEntity class constants are used.
 */
SELECT g.sessionID, "timecode", frameID, -1, -1, -1, -1, -1, -1, -1, "motion", bb_x1, bb_y1, bb_x2, bb_y2, h_x, h_y, s_lx, s_ly, s_rx, s_ry, e_lx, e_ly, e_rx, e_ry, n_lx, n_ly, n_rx, n_ry, l_lx, l_ly, l_rx, l_ry, k_lx, k_ly, k_rx, k_ry, f_lx, f_ly, f_rx, f_ry, c.username
FROM goldLog g, crowdworker c
WHERE g.sessionID = c.sessionID
ORDER BY frameID;
