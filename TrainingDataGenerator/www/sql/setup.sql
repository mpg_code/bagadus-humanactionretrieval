DROP TABLE IF EXISTS trainingdata;
DROP TABLE IF EXISTS crowdworker;
DROP TABLE IF EXISTS feedback;
DROP TABLE IF EXISTS goldLog;

CREATE TABLE trainingdata
(
       /* Metadata */
       sessionID   int,
       timecode    varchar(30),
       frameID     varchar(50),
       frameX      int,
       frameY      int,
       width       int,
       height      int,
       players     int,
       control     int DEFAULT 0,
       processed   int DEFAULT 0,
       motion      varchar(10),
       sequenceNo  int,

       /* Bounding box pointers, 1 = start (upper left corner), 2 = end (bottom right corner) */
        bb_x1 int,
        bb_y1 int,
        bb_x2 int,
        bb_y2 int,

       /* Positional Data <locaction>_<(l)eft|(r)ight><x|y>
        * Location:
        * h = head
        * s = shoulder
        * e = elbow
        * n = hand
        * l = leg
        * k = knee
        * f = foot
        */

       h_x int,
       h_y int,

       s_lx int,
       s_ly int,
       s_rx int,
       s_ry int,

       e_lx int,
       e_ly int,
       e_rx int,
       e_ry int,

       n_lx int,
       n_ly int,
       n_rx int,
       n_ry int,

       l_lx int,
       l_ly int,
       l_rx int,
       l_ry int,

       k_lx int,
       k_ly int,
       k_rx int,
       k_ry int,

       f_lx int,
       f_ly int,
       f_rx int,
       f_ry int,

       /* Table configuration */
       PRIMARY KEY (sessionID, frameID)
);

CREATE TABLE crowdworker
(
        sessionID  int,
        username   varchar(64),
        goldErrors int,
        startTime  timestamp DEFAULT 0,
        endTime    timestamp DEFAULT 0,
        realname   varchar(64),
        credit     boolean DEFAULT FALSE,

        PRIMARY KEY (sessionID)
);

CREATE TABLE feedback
(
        sessionID int,
        entryNo   int,
        comment   varchar(512),

        PRIMARY KEY (sessionID, entryNo)
);

CREATE TABLE goldLog
(
       /* Metadata */
       sessionID   int,
       frameID     varchar(50),
       goldTest    int,
       sequenceNo  int,

       /* Bounding box pointers */
        bb_x1 int,
        bb_y1 int,
        bb_x2 int,
        bb_y2 int,

       /* Positional Data <locaction>_<(l)eft|(r)ight><x|y>
        * Location:
        * h = head
        * s = shoulder
        * e = elbow
        * n = hand
        * l = leg
        * k = knee
        * f = foot
        */

       h_x int,
       h_y int,

       s_lx int,
       s_ly int,
       s_rx int,
       s_ry int,

       e_lx int,
       e_ly int,
       e_rx int,
       e_ry int,

       n_lx int,
       n_ly int,
       n_rx int,
       n_ry int,

       l_lx int,
       l_ly int,
       l_rx int,
       l_ry int,

       k_lx int,
       k_ly int,
       k_rx int,
       k_ry int,

       f_lx int,
       f_ly int,
       f_rx int,
       f_ry int,

       /* Table configuration */
       PRIMARY KEY (sessionID, frameID, goldTest)
);

/**
 * View for finding load-balancing on sequences
 */
CREATE VIEW sequenceframes AS
SELECT sequenceNo, frameID, COUNT(frameID) - 1 as trainedframes
FROM trainingdata WHERE sequenceNo IS NOT NULL
GROUP BY sequenceNo, frameID;
