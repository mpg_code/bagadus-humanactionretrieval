
/* Obtain sessionID and username with their respective trained and gold sizes */
select l.sessionID, c.username, count(*) as goldLog, (select count(*) from trainingdata t where t.sessionid = l.sessionid) as training from goldLog l, crowdworker c where  l.sessionid = c.sessionid group by sessionid order by training, username;

/* Get users, sessionid without any gold samples */
select c.sessionid, c.username, count(*) as trained from trainingdata t, crowdworker c where t.sessionid = c.sessionid and c.sessionid in (select sessionid from crowdworker where sessionid not in (select sessionid from goldLog)) group by username, sessionid;

/* Users without any work at all */
select sessionID, username, 0 as goldLog, NULL as goldErrors, 0 as training from crowdworker where sessionid not in (select sessionid from trainingdata) and sessionid not in (select sessionid from goldLog);

/* Get the time users have spent */
select sessionid, username, (select timediff(endtime, starttime) from crowdworker c2 where c2.sessionid = c.sessionid and c2.username = c.username) as time from crowdworker c;

/* Get trained images per day */
SELECT EXTRACT(DAY FROM endTime) - 7 AS day, count(*) AS images FROM crowdworker c, trainingdata t WHERE c.sessionID = t.sessionID GROUP BY day HAVING day > 0;

/* Get full statistics for everything */
SELECT sessionID,
       TIMEDIFF(endTime, startTime) AS timeUsed,
       username,
       (SELECT COUNT(*) FROM trainingdata t WHERE t.sessionID = c.sessionID) AS trainedImages,
       (SELECT COUNT(*) FROM goldLog l WHERE l.sessionID = c.sessionID) AS goldLogs,
       goldErrors
FROM crowdworker c
ORDER BY trainedImages,
         goldErrors DESC,
         goldLogs,
         timeUsed,
         username,
         sessionID;

/* Get sequence numbers and number of frames in every sequence */
SELECT DISTINCT(sequenceNo), COUNT(sequenceNo) AS frames FROM trainingdata WHERE sessionID = 0 GROUP BY sequenceNo;

/* Get number of frames processed in each sequence */
SELECT DISTINCT(sequenceNo), (SELECT COUNT(frameID) FROM trainingdata t2 WHERE t2.sequenceNo = t1.sequenceNo AND sessionID != 0) AS frames FROM trainingdata t1;

/* Get the current work completion weight for each sequence */
SELECT processed.sequenceNo, (processed.frames / lengths.frames) AS sequenceNo FROM (SELECT DISTINCT(sequenceNo), COUNT(sequenceNo) AS frames FROM trainingdata WHERE sessionID = 0 GROUP BY sequenceNo) AS lengths, (SELECT DISTINCT(sequenceNo), (SELECT COUNT(frameID) FROM trainingdata t2 WHERE t2.sequenceNo = t1.sequenceNo AND sessionID != 0) AS frames FROM trainingdata t1) AS processed WHERE lengths.sequenceNo = processed.sequenceNo;

/* Get sequence with least amout of work done, random if more has same score */
SELECT sequenceNo FROM (SELECT processed.sequenceNo, (processed.frames / lengths.frames) AS trained FROM (SELECT DISTINCT(sequenceNo), COUNT(sequenceNo) AS frames FROM trainingdata WHERE sessionID = 0 GROUP BY sequenceNo) AS lengths, (SELECT DISTINCT(sequenceNo), (SELECT COUNT(frameID) FROM trainingdata t2 WHERE t2.sequenceNo = t1.sequenceNo AND sessionID != 0) AS frames FROM trainingdata t1) AS processed WHERE lengths.sequenceNo = processed.sequenceNo) AS workload ORDER BY trained ASC, RAND() LIMIT 1;

