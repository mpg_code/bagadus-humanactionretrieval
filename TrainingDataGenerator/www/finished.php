<?php
require('phpconfig.php');
require('phpmodules/utilities.php');

echo '<body>'.PHP_EOL
.'<div align="center">'.PHP_EOL
.'<h1>baardew Master Training Data</h1>'.PHP_EOL
.'<p>Finished! Thank you for participating.</p>'.PHP_EOL
.'<br/>'.PHP_EOL;

if ($multiSessions && moreFramesAvailable()) {
    echo '<p>If you like, you can <a href="resetsession.php">Start a New Session</a>.</p>'.PHP_EOL.'<br/>'.PHP_EOL;
}

echo '<p>Any feedback you have will be appriciated and can be entered here (maximum 500 characters):</p>'.PHP_EOL
.'<form action="feedback.php" method="post">'.PHP_EOL
.'<textarea name="feed_text" maxlength="500" cols="50" rows="10"></textarea>'.PHP_EOL
.'<br/>'.PHP_EOL
.'Your Name: <input type="txt" name="feed_name" maxlength="64"/>'.PHP_EOL
.'Credit Me Publicly: <input type="checkbox" name="feed_credit"/>'.PHP_EOL
.'<br/>'.PHP_EOL
.'<br/>'.PHP_EOL
.'<input type="submit" name="submit" value="Submit"/>'.PHP_EOL
.'</form>'.PHP_EOL
.'<p><b>DISCLAIMER:</b><br/> It is voluntary to enter your real name. If you opt in to be credited without a real name, your user-name will be used.<br/>'.PHP_EOL
.'Information entered will not be sold or distributed further, but your name or user-name will be available online for others to read.<br/>'.PHP_EOL
.'Entered names can be filtered. Your work effort is in no way connected, directly or indirectly, to the published list.</p>'.PHP_EOL
.'</div>'.PHP_EOL
.'</body>';
?>