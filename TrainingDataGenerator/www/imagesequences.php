<?php
///////////
// START //
///////////
Header("content-type: application/x-javascript");

require('phpconfig.php');
require('phpmodules/utilities.php');

echo 'var disableMotion = '.($disableMotion ? 'true' : 'false').';'.PHP_EOL;
echo 'var imageServer = "'.$imageServer.'";'.PHP_EOL;

if (!isset($_COOKIE['image_name'])) {
    echo 'var preImages = [];'.PHP_EOL.'var postImages = [];';
    exit;
}

$frameId = $_COOKIE['image_name'];
$frameSequenceNumber = stringToIndex($frameId);
$frameLimit = 30;

getPostImages();
getPreImages();

/////////
// END //
/////////

function getPostImages() {
    global $frameId;
    global $frameSequenceNumber;
    global $frameLimit;

    $query = DB::query("SELECT DISTINCT frameid FROM trainingdata ".
                       "WHERE frameid > %s AND sequenceNo = (SELECT sequenceNo from trainingdata WHERE frameid = %s LIMIT 1) ".
                       "ORDER BY frameid ASC LIMIT %i", $frameId, $frameId, $frameLimit);


    echo 'var postImages = [';

    $first = true;
    $current = $frameSequenceNumber;
    foreach ($query as $row) {
        $next = stringToIndex($row['frameid']);
        if ($next - $current > 1) {
            break;
        }
        $current = $next;

        if (!$first) {
            echo ', ';
        } else {
            $first = false;
        }

        echo '"'.$row['frameid'].'"';
    }

    echo '];'.PHP_EOL;
}

function getPreImages() {
    global $frameId;
    global $frameSequenceNumber;
    global $frameLimit;

    $query = DB::query("SELECT DISTINCT frameid FROM trainingdata ".
                       "WHERE frameid < %s AND sequenceNo = (SELECT sequenceNo from trainingdata WHERE frameid = %s LIMIT 1) ".
                       "ORDER BY frameid DESC LIMIT %i", $frameId, $frameId, $frameLimit);


    echo 'var preImages = [';

    $first = true;
    $current = $frameSequenceNumber;
    foreach ($query as $row) {
        $next = stringToIndex($row['frameid']);
        if ($next - $current > 1) {
            break;
        }
        $current = $next;

        if (!$first) {
            echo ', ';
        } else {
            $first = false;
        }

        echo '"'.$row['frameid'].'"';
    }

    echo '];'.PHP_EOL.'preImages.reverse();'.PHP_EOL;
}

?>