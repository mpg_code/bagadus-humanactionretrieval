<?php
Header("content-type: application/x-javascript");
session_start();

$percentage = 0;

require('phpconfig.php');

// Just ignore error, set it to N/A
if (!isset($_SESSION['username']) || !(isset($_SESSION['session_id']) && is_numeric($_SESSION['session_id']))) {
    $percentage = "\"n/a\"";
} else {
    $query = DB::query("SELECT count(*) AS count FROM crowdworker c, trainingdata t WHERE c.sessionID = t.sessionID AND c.username = %s AND c.sessionID = %i",
                       $_SESSION['username'], $_SESSION['session_id']);

    if (count($query) == 0) {
        $percentage = "\"n/a\"";
    } else  {
        $percentage = intval($query[0]['count'] * 100 / $limit_training);
    }
}

echo 'var progressionPercent = '.$percentage.';'.PHP_EOL;
?>