///////////
// START //
///////////

// To disable logging, simply redefine the logging function to do nothing
//console.log = function() {}

$(window).keypress(function(e) {
  var video = document.getElementById("previewVideo");
  if (e.which == 32) { // spacebar
    if (video.paused == true)
      video.play();
    else
      video.pause();
  }
});

// The zooming function is taken from
// http://odyniec.net/projects/imgareaselect/
// http://odyniec.net/projects/imgareaselect/examples-callback.html#submitting-selection-coordinates-demo


// An initial test to ensure that things are set up correctly
var settingsFailure = isNaN(parseInt(getCookie("calibrate_idx")))
    || isNaN(parseInt(getCookie("training_idx")))
    || getCookie("image_idx").length === 0;
if (settingsFailure) {
    console.log("settingsFailure!"
                + "calibrate_idx=" + parseInt(getCookie("calibrate_idx"))
                + "training_idx=" + parseInt(getCookie("training_idx"))
                + "image_idx='" + getCookie("image_idx")+"'");
    window.location.href = "server.php";
}

// By default, no registration is to be done when leaving this page
document.cookie = "skipImage=true";

// Display the progression
document.getElementById('progress_text').innerHTML = "PROGRESS: " + progressionPercent + "&#37";

// If motion is not to be entered, then disable those parts
if (disableMotion) {
    document.getElementById("players").style.visibility = "hidden";
    document.getElementById("motion").style.visibility = "hidden";
    document.getElementById("noplayer").style.visibility = "hidden";
}

// Configuration
var videoFPS = 15; // FPS for next/prev video sequences
var errorTolerance = 11; // For calibration and comparison, max distance from perfect click
var repeatFrame = 3; // Number of times current frame is rendered to preview

// Global variables
var frameCount    = 0;
var motionSet     = disableMotion; // If motion is not to be set (true), then consider it already set
var err_x         = 0;
var err_y         = 0;
var limit_x       = 500;
var limit_y       = 500;
// DoF = Degree of Freedom, here used as joint points
var DoF_name      = ["EMPTY_SET"];
var DoF_data      = ["EMPTY_SET"];
var DoF_guide     = [];
var guide_radius  = 10
var DoF_idx       = 0;
var canvas        = document.getElementById("canvas");
var context       = canvas.getContext("2d");
var img           = new Image();
var guide         = new Image();

canvas.addEventListener("mousedown", getPosition, false);

// By default, image selection is entire canvas
document.cookie = "bb_x1=" + 0;
document.cookie = "bb_y1=" + 0;
document.cookie = "bb_x2=" + 500;
document.cookie = "bb_y2=" + 500;

var img_idx = getCookie("image_idx");
console.log("img_idx = " + img_idx);

if (img_idx == "c0") {
    img.src   = imageServer + "SingleCalibration.png";
    guide.src = imageServer + "SingleCalibration.png";

    DoF_name = ["Please click in center of <span style=\"color:red\">Top Left</span> circle",
                "Please click in center of <span style=\"color:red\">Top Right</span> circle",
                "Please click in center of <span style=\"color:red\">Bottom Left</span> circle",
                "Please click in center of <span style=\"color:red\">Bottom Right</span> circle",
                "Please click in center of <span style=\"color:red\">Center</span> circle",
                "Press Commit"];

    DoF_guide = [[63, 63], [188, 63], [63, 188], [188, 188], [129, 129]];

    DoF_data = ["h_", "h_", "h_", "h_", "h_"];

    document.getElementById("additional_text").innerHTML
        = 'Press the center of each figure in the image below, given the location to click on the box above.<br />'
        + 'Be as accurate as possible, please.<br />'
        + 'When finished, press <i>Commit</i>';

    canvas.style.cursor = "crosshair";
    document.getElementById("zoom").disabled = true;
    document.getElementById("noplayer").disabled = true;
    document.getElementById("newimage").disabled = true;
    motionSet = true;

} else {
    console.log("Image: "  + getCookie("image_name").replace("%2F", "/"));
    img.src = imageServer + getCookie("image_name").replace("%2F", "/");

    guide.src = imageServer + "ClickGuide.jpg";

    DoF_name = ["Please select <span style=\"color:red\">Region</span> with player",
                "Please click in the <span style=\"color:red\">Center of the Head</span>",
                "Please click on the <span style=\"color:red\">Right Shoulder</span>",
                "Please click on the <span style=\"color:red\">Right Elbow</span>",
                "Please click on the <span style=\"color:red\">Right Hand</span>",
                "Please click on the <span style=\"color:red\">Right Hip</span>",
                "Please click on the <span style=\"color:red\">Right Knee</span>",
                "Please click on the <span style=\"color:red\">Right Heel</span>",
                "Please click on the <span style=\"color:red\">Left Shoulder</span>",
                "Please click on the <span style=\"color:red\">Left Elbow</span>",
                "Please click on the <span style=\"color:red\">Left Hand</span>",
                "Please click on the <span style=\"color:red\">Left Hip</span>",
                "Please click on the <span style=\"color:red\">Left Knee</span>",
                "Please click on the <span style=\"color:red\">Left Heel</span>",
                "Please select Motion"];

    // First is drawn outside image to prevent it form beeing displayed
    DoF_guide = [[-guide_radius, -guide_radius],
                 [135, 25], [108, 49], [85, 62], [34, 96], [102, 130], [83, 169], [53, 235],
                 [160, 53], [195, 80], [216, 129], [141, 137], [134, 171], [111, 220]];

    DoF_data = ["NULL", "h_", "s_r", "e_r", "n_r", "l_r", "k_r", "f_r", "s_l", "e_l", "n_l", "l_l", "k_l", "f_l"];

    if (disableMotion) {
        DoF_name.pop();
    }

    err_x = parseInt(getCookie("err_x"));
    err_y = parseInt(getCookie("err_y"));
    if (err_x === NaN || err_y === NaN) {
        document.cookie = "training_idx=0";
        document.cookie = "calibrate_idx=0";
        window.location.href = "server.php";
    }

    // Have a default size scaling factor of 5.
    // Size of this and next/prev images width are set to fit a 1024*1280 screen
    canvas.width = 500; // = parseInt(getCookie("image_width"));;
    canvas.height = 500;// = parseInt(getCookie("image_height"));
    console.log("img_dim = " + canvas.width + " x " + canvas.height);

    // Enable image area selection
    $('#canvas').imgAreaSelect({
        onSelectEnd: function (img, selection) {
            if (DoF_idx-1 === 0) {
                $('input[name="x1"]').val(selection.x1);
                $('input[name="y1"]').val(selection.y1);
                $('input[name="x2"]').val(selection.x2);
                $('input[name="y2"]').val(selection.y2);
            }
        }
    });
}

var DoF_max  = DoF_name.length;
if (disableMotion && img_idx != "c0")
    DoF_max++;

document.getElementById("marker").innerHTML = DoF_name[DoF_idx++];

console.log(img.src);
img.onload = function() {
    context.drawImage(img, 0, 0, 500, 500);
}

console.log(guide.src);
guide.onload = function() {
    var can = document.getElementById("previewPoint");
    var ctx = can.getContext("2d");
    ctx.drawImage(guide, 0, 0, 250, 250);
    drawGuide();
}

loadVideo();


/////////
// END //
/////////

function loadVideo()
{
    var encoder = new Whammy.Video(videoFPS);
    var totalFrames = preImages.length + postImages.length + repeatFrame;
    var frames = new Array();
    var currentImage = getCookie("image_name").replace("%2F", "/");
    if (currentImage === 'NULL')
        return;

    var i;
    for (i = 0; i < preImages.length; i++)
        processPreviewImage(imageServer+preImages[i], encoder, totalFrames, i, frames);

    // Fetch the same image, again, but will ensure correct format and frame index
    processCurrentImage(imageServer+currentImage, encoder, totalFrames, preImages.length, frames);

    for (i = 0; i < postImages.length; i++)
        processPreviewImage(imageServer+postImages[i], encoder, totalFrames, preImages.length + repeatFrame + i, frames);
}

function compilePreview(video, frames, totalframes)
{
    var i;
    for (i = 0; i < totalframes; i++)
        video.add(frames[i]);

    var output = video.compile();
    var url = (window.webkitURL || window.URL).createObjectURL(output);
    var postVideo = document.getElementById("previewVideo");
    postVideo.src = url;
}

function processPreviewImage(file, video, totalframes, index, frames)
{
    var img = new Image();
    img.src = file;
    img.onload = function() {
        frames[index] = getCanvas(img);

        if (++frameCount === totalframes)
            compilePreview(video, frames, totalframes);
    }
}

function processCurrentImage(file, video, totalframes, index, frames)
{
    var img = new Image();
    img.src = file;
    img.onload = function() {
        var f = getMarkedCanvas(img);

        var i;
        for (i = index; i < index + repeatFrame; i++, frameCount++)
            frames[i] = f;

        if (frameCount === totalframes)
            compilePreview(video, frames, totalframes);
    }
}

function getCanvas(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    return canvas;
}

function getMarkedCanvas(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    // Render the marking border
    ctx.beginPath();
    ctx.rect(0, 0, 200, 200);
    ctx.lineWidth = 6;
    ctx.strokeStyle = 'red';
    ctx.stroke();

    return canvas;
}

function getBase64Image(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    return canvas.toDataURL();
}

function recolorOldSelection()
{
    if ((DoF_idx > 2) || (DoF_idx > 1 && img_idx == "c0")) {
        var prev_x = getCookie(DoF_data[DoF_idx-2] + "x");
        var prev_y = getCookie(DoF_data[DoF_idx-2] + "y");
        context.fillStyle="#00FF00";
        context.fillRect(prev_x-2, prev_y-2, 4, 4);
    }
}

function nextDoF()
{
    // Update all user entries
    document.getElementById("marker").innerHTML = DoF_name[DoF_idx++];
    if (DoF_idx == DoF_max) {
        if (motionSet) {
            document.getElementById("commit").disabled = false;
            document.getElementById("marker").innerHTML = "Press <span style=\"color:red\">Commit</span>";
            document.getElementById("previewVideo").pause();
        } else {
            document.getElementById("previewVideo").play();
        }

        document.getElementById("obscured").disabled = true;
    } else {
        document.getElementById("previewVideo").pause();
    }

    drawGuide();
}

function drawGuide()
{
    var can = document.getElementById("previewPoint");
    var ctx = can.getContext("2d");
    ctx.drawImage(guide, 0, 0, 250, 250);

    if (DoF_idx < DoF_max) {
        console.log("Drawing Guide " + DoF_guide[DoF_idx-1]);
        var centerX = DoF_guide[DoF_idx-1][0];
        var centerY = DoF_guide[DoF_idx-1][1];

        ctx.beginPath();
        ctx.arc(centerX, centerY, guide_radius, 0, 2 * Math.PI, false);
        ctx.lineWidth = 3;
        ctx.strokeStyle = '#FF0000';
        ctx.stroke();
    }
}

function mark(y, x) {
    if (DoF_idx-1 == 0 && img_idx != "c0") {
        // Do nothing, this is the zoom process
        // which is handleded by the zoom function
        return;
    }

    if (img_idx != "c0") {
        y -= err_y;
        x -= err_x;
    }

    console.log(y + " x " + x + " @ " + DoF_idx);
    if (x > limit_x || y > limit_y) {
        window.location.href = "inprecise.html";
    }

    // Color prev selection as old
    recolorOldSelection();

    // Draw new selection
    context.fillStyle="#FF0000";
    context.fillRect(x-2, y-2, 4, 4);

    if (img_idx == "c0") {
        calibrateMouse(y, x);
    }

    document.cookie = DoF_data[DoF_idx-1] + "x=" + x;
    document.cookie = DoF_data[DoF_idx-1] + "y=" + y;

    nextDoF();
}

function zoom()
{
    document.getElementById('zoom').blur();
    var form = document.getElementById("zooms");

    // Update guides and markers
    document.getElementById("marker").innerHTML = DoF_name[DoF_idx++];
    $('#canvas').imgAreaSelect({disable:true,hide:true});
    canvas.style.cursor="crosshair";
    drawGuide();

    document.getElementById("zoom").disabled = true;
    document.getElementById("obscured").disabled = false;
    document.getElementById("undo").disabled = false;
    document.getElementById("motion").disabled = false;
    document.getElementById("noplayer").disabled = true;
    document.getElementById("players").disabled = false;

    var x1 = parseInt(form.elements['x1'].value);
    var y1 = parseInt(form.elements['y1'].value);
    var x2 = parseInt(form.elements['x2'].value);
    var y2 = parseInt(form.elements['y2'].value);

    if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2)) {
        x1 = 0;
        y1 = 0;
        x2 = 500;
        y2 = 500;
    }

    console.log("Selection is (" + x1 + ", " +  y1 + ") -> (" + x2 + ", " + y2 + ")");

    document.cookie = "bb_x1=" + x1;
    document.cookie = "bb_y1=" + y1;
    document.cookie = "bb_x2=" + x2;
    document.cookie = "bb_y2=" + y2;

    limit_x = x2;
    limit_y = y2;

    var scaleX = 500.0 / (x2 - x1);
    var scaleY = 500.0 / (y2 - y1);
    var scale = Math.min(scaleX, scaleY) / 2;
    if (scale < 1)
        scale = 1;

    console.log("Zooming: " + scale);

    // Create blackground if image outside canvas
    context.rect(0, 0, 500, 500);
    context.fillStyle = "black";
    context.fill();

    // Rescale the image
    context.drawImage(img, -x1 * scale, -y1 * scale, 500 * scale, 500 * scale);

    // Create a canvas that we will use as a mask
    var maskCanvas = document.createElement('canvas');
    maskCanvas.width = canvas.width;
    maskCanvas.height = canvas.height;
    var maskCtx = maskCanvas.getContext('2d');

    // This color is the one of the filled shape
    maskCtx.fillStyle = "black";
    maskCtx.fillRect(0, 0, maskCanvas.width, maskCanvas.height);
    maskCtx.globalCompositeOperation = 'xor';
    maskCtx.rect(0, 0, (x2 - x1) * scale, (y2 - y1) * scale);
    maskCtx.fill();

    // Draw mask on the image
    context.drawImage(maskCanvas, 0, 0);
}

function calibrateMouse(y, x)
{
    var actual = [[128, 128], [128, 384], [384, 128], [384, 384], [256, 256]];
    err_y -= y - actual[DoF_idx-1][0];
    err_x -= x - actual[DoF_idx-1][1];

    var delta = eucludianDistance(x, y, actual[DoF_idx-1][1], actual[DoF_idx-1][0]);

    if (delta > errorTolerance)
        window.location.href = "inprecise.html";
}

function eucludianDistance(x1, y1, x2, y2)
{
    var diffx = x1 - x2;
    var diffy = y1 - y2;
    if (diffx < 0)
        diffx *= -1;
    if (diffy < 0)
        diffy *= -1;

    return Math.sqrt(((diffx * diffx) + (diffy * diffy)));
}

function obscure()
{
    document.getElementById('obscured').blur();

    recolorOldSelection();

    document.cookie = DoF_data[DoF_idx-1] + "x=-1";
    document.cookie = DoF_data[DoF_idx-1] + "y=-1";

    console.log("OBSCURED @ " + DoF_idx);

    nextDoF();
}

function undo()
{
    document.getElementById('undo').blur();

    if (DoF_idx < 3) {
        history.go(0);
        return;
    }

    console.log("UNDO @ " + DoF_idx);

    zoom();
    var newIdx = DoF_idx - 3; // DoF_idx starts at 1 above, we remove 1 and nextDoF adds 1 hence the -3

    for (var i = 0; i < newIdx; i++) {
        DoF_idx = i + 2; // +2 to get the name of the DoF in array
        recolorOldSelection();
    }

    DoF_idx = newIdx;
    nextDoF();
}

function motionChanged()
{
    var ddl = document.getElementById("motion");
    var motion = ddl.options[ddl.selectedIndex].value;

    console.log("Motion set to: " + motion);

    if (motion != "NULL" || disableMotion)
        motionSet = true;
    else
        motionSet = false;

    if (DoF_idx == DoF_max && motionSet) {
        document.getElementById("commit").disabled = false;
        document.getElementById("marker").innerHTML = "Press <span style=\"color:red\">Commit</span>";
        document.getElementById("previewVideo").pause();
    } else {
        document.getElementById("commit").disabled = true;
        if (DoF_idx == DoF_max) {
            document.getElementById("marker").innerHTML = "Select <span style=\"color:red\">Motion</span>"
            document.getElementById("previewVideo").play();
        }
    }
}

function commit()
{
    if (img_idx == "c0") {
        err_y /= 5;
        err_x /= 5;

        err_x = Math.round(err_x);
        err_y = Math.round(err_y);

        document.cookie = "err_y=" + err_y;
        document.cookie = "err_x=" + err_x;
    }

    if (img_idx.charAt(0) == 'c') {
        console.log("calibrate idx: " + getCookie("calibrate_idx"));
        document.cookie = "calibrate_idx=" + (parseInt(getCookie("calibrate_idx")) + 1);
    } else {
        console.log("training idx: " + getCookie("training_idx"));
        document.cookie = "training_idx=" + (parseInt(getCookie("training_idx")) + 1);
    }

    document.cookie = "motion=" + document.getElementById("motion").value;
    document.cookie = "players=" + document.getElementById("players").value;
    document.cookie = "skipImage=false";
    window.location.href = "server.php";
}

function noPlayer()
{
    document.getElementById('noplayer').blur();

    if (disableMotion)
        return;

    zoom();

    while (DoF_idx != DoF_max)
        obscure();

    var element = document.getElementById('players');
    element.value = "0";

    commit();
}

// Source: http://www.w3schools.com/js/js_cookies.asp
function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }

    return "";
}

function getPosition(event)
{
    if (DoF_idx == DoF_max)
        return;

    // Source: http://miloq.blogspot.no/2011/05/coordinates-mouse-click-canvas.html
    var x = new Number();
    var y = new Number();
    var canvas = document.getElementById("canvas");

    if (event.x != undefined && event.y != undefined)
    {
        x = event.x;
        y = event.y + document.body.scrollTop;
    }
    else // Firefox method to get the position
    {
        x = event.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        y = event.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }

    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;

    mark(y, x);
}
