<?php
session_start();

require('phpconfig.php');
require('phpmodules/utilities.php');

if (isset($_POST['feed_text']) && isset($_SESSION['session_id'])) {
    if (is_numeric($_SESSION['session_id'])) {
        $entryNo = 0;
        $query = DB::query("SELECT MAX(entryNo) AS max FROM feedback WHERE sessionID = %i", $_SESSION['session_id']);

        if (is_numeric($query[0]['max'])) {
            $entryNo = $query[0]['max'] + 1;
        }

        DB::insert('feedback',
                   array('sessionID' => $_SESSION['session_id'],
                         'entryNo' => $entryNo,
                         'comment' => $_POST['feed_text'])
            );
    }
}

if (isset($_POST['feed_credit']) && isset($_SESSION['session_id'])) {
    if (is_numeric($_SESSION['session_id'])) {
        if (isset($_POST['feed_name']) && strlen($_POST['feed_name']) > 0) {
            DB::update('crowdworker',
                       array('realname' => $_POST['feed_name'], 'credit' =>  DB::sqlEval('TRUE')),
                       'sessionID = %i', $_SESSION['session_id']);
        } else {
            DB::update('crowdworker',
                       array('realname' => DB::sqlEval('username'), 'credit' => DB::sqlEval('TRUE')),
                       'sessionID = %i', $_SESSION['session_id']);
        }
    }
}

echo "<body>".PHP_EOL
."<div align=\"center\">".PHP_EOL
."<h1>baardew Master Training Data</h1>".PHP_EOL
."<p>Thank you for your feedback.</p>".PHP_EOL;

if ($multiSessions && moreFramesAvailable()) {
    echo '<p>If you like, you can <a href="resetsession.php">Start a New Session</a>.</p>'.PHP_EOL.'<br/>'.PHP_EOL;
}

echo "</div>".PHP_EOL
."</body>";

?>