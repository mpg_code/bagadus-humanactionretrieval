# Bagadus - Human Action Retrieval

This is the source code developed for the masters thesis _Human Action Retrieval in the sport analytic system BAGADUS_. It is designed to operate with the data/video from http://home.ifi.uio.no/paalh/dataset/alfheim/.

## Installation

Required libraries, install in this order:

- gcc / g++:
Recommended version 4.8.4 or newer

- Cmake:
Version 2.8 or newer

- Boost:
Latest version

- OpenMP:
Latest version

- FFmpeg:
Install with this configuration:
```
./configure --enable-shared --enable-static --enable-pic
```

- CUDA:
Version 6.5 or newer. Installation from nVidia. Newer version can work, but requires change in CMake Files.

If you get compilation error of missing CUDA, such as `/usr/bin/ld: cannot find -lcufft` and/or `/usr/bin/ld: cannot find -lcublas` then add: `-L/usr/local/cuda-6.5/lib64 -L/usr/local/cuda-6.5/lib64/lib` to the opencv.pc (pkg-config file).

- OpenCV:
Excact version 2.4.10. Version 3 has a slightly different API and will therefore not work.

Compilation of OpenCV (get 2.4.10 from opencv.org) is done with the following arguments (granted the CUDA installation is in /usr/local/cuda-6.5):
```
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-6.5 -D CUDA_SDK_ROOT_DIR=/usr/local/cuda-6.5 -D WITH_TBB=ON -D WITH_V4L=ON -D BUILD_EXAMPLES=ON -D WITH_CUDA=ON -D WITH_CUBLAS=ON -D WITH_CUFFT=ON -D WITH_EIGEN=ON -D CUDA_ARCH_BIN="30 35 50" -D CUDA_ARCH_PTX="30 50" -D BUILD_SHARED_LIBS=OFF -D CMAKE_CXX_FLAGS=-fPIC -D WITH_QT=ON -D WITH_VTK=OFF -D WITH_GTK=OFF -D WITH_OPENGL=OFF ..
```

## Programs Structure

```
Folders and Programs:
    TrainingDataGenerator/
        DataGenerator - Generate SQL, images amd optical flow to have ground truth set by www for use as trained data
        www           - Site for entering ground truth data
    MotionClassifier/
        MotionClassifier - Reads trained data and sequence and classify it
    Utility/
        ResultChecker        - Tool to both visualize and validate the results obtained from www and MotionClassifier
        CoordinateTranslator - Very small program to convert www relative joint positions to absolute full-frame positions or merge results
    shared/
        CMake.modules/  - Package finding definitions for cmake
        configurations/ - Files for configuring homography morphing (see below for format)
        lib/            - Include and Source files used in some or all projects put into a library
```
Every project contains its own README file that describes each program in more depth. Additionally, they are designed to be compatible with different workflows, with the primary workflow illustrated here (consisting of every step from obtaining video sequences to having reprojected skeletons):

![Primary Workflow](https://bytebucket.org/mpg_code/bagadus-humanactionretrieval/raw/f9608c3d782e6420a9b601b7803f5368b92538c1/Workflow_SkeletonEstimation.png)

Program projects with a test/ folder have a separate makefile to compile and run small sample code that uses the main classes and include files. Unfortunately, no TDD as you really cannot verify OpenCV functions.

## Data Files
The data format used in this thesis consists of png images, csv files (Comma-Separated Values), SQL files containing SQL queries and regular txt files for additional program control. This is done in order to have consistent inputs and outputs across all the different programs, but also because the original data used in this thesis comes from CSV files. The only file not being either an image or a CSV file is the serialized file created by the _DataGenerator, which is a binary packed vector field of optical flow vectors.

Video sources are available here: http://home.ifi.uio.no/paalh/dataset/alfheim/

### ZXY Tracker Data
Tracker data are ZXY data exported from a recorded game. See video source above, which also comes with ZXY data.

### Video Listings
Some programs requires the videos from the video source above listed in a text file. To create this text file, do the following in the directory containing the video clips (you might need to remove the last line containing `cliplist.txt`):
```
ls > cliplist.txt
```

### Homography Configuration
Homography mapping files are described in shared/README.md

### Database Files
All the data files used (except for ZXY tracker data) uses CSV files containing all the data entries found in the _www_ project (see TrainingDataGenerator/www/sql/setup.sql for definition).